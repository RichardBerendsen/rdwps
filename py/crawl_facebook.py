#!/usr/bin/python

from BeautifulSoup import BeautifulSoup
import urllib # for quote function
import urllib2 # for urlopen
import urlparse # to split the url, and urlencode only path part.

import get_queries
import sys
import time
import oauth2 as oauth
import simplejson as json
from pprint import pprint
from util import normalize
from get_queries import get_queries
from store_urls import store_urls
import random
import time
import os
import re
import MySQLdb

def _parse_search_results(page):
    res = []
    soup = BeautifulSoup(page)
    #f = open("page", "w")
    #f.write(soup.prettify())
    #f.close()

    # The url is in the href of an <a> element matching those criteria.
    # Strangely it is the last part of the href right after two consecutive stars.

    #ts = soup.findAll(lambda t: t.name=="a" and t.parent.name=="li" and t.parent.name=="ul" and t["class"]=="fn url beacon" and t.has_key("href"))
    ts = soup.findAll(lambda t: t.name=="a" and t.has_key("class") and t.has_key("href"))
    ts = [t for t in ts if t["class"] == "UIImageBlock_Image UIImageBlock_ENT_Image" ]
    return [ t['href'] for t in ts ]

def _connect():
	return MySQLdb.connect (host = "qassir",
						   user = "bogomil",
						   passwd = "wieowie",
						   db = "people",
                           cursorclass = MySQLdb.cursors.DictCursor)

def _query_already_processed(query):
    conn = _connect()
    cursor = conn.cursor()
    cursor.execute("select count(*) as count from search_results where \
FirstName = %s and LastName = %s and type = 'facebook'", (query['FirstName'], query['LastName']))
    row = cursor.fetchone()
    if row['count'] > 0:
        return True
    cursor.close()
    conn.close()
    return False

def _main():
    queries = get_queries()
    for query in queries:
        filepath = "../../exp/facebook_search_result_per_query/%s.html" % query['idx']
        if _query_already_processed(query):
            continue
        try:
            f = open(filepath, "r")
        except IOError, e:
            sys.stderr.write('IOError while trying to open\n%s\nfor reading\n' % filepath)
            sys.exit(2)
        else:
            urls = _parse_search_results(f.read())
            n_urls=[normalize(u) for u in urls]
            store_urls(query['FirstName'], query['LastName'], 'facebook', urls, n_urls)

if __name__ == "__main__":
    _main()
