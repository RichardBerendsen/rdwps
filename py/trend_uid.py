# wieowie trends.

import MySQLdb
import matplotlib
matplotlib.use('Agg') # make matplotlib use the AGG backend, for storing png's.
import matplotlib.pyplot as plt
import matplotlib.dates as mpldates
import numpy as np
import random
import os
import urllib
import datetime as dt
import sys

# assume connection, fetch trend
def _fetch_trend(conn, first_name, last_name):
    cursor = conn.cursor()
    cursor.execute("call trend_uid(%s, %s)", (first_name, last_name))
    dates, counts = zip(*cursor.fetchall())
    cursor.close()
    return dates, counts

# create phrase for annotation interface
def _phrase(first_name, last_name):
    # unquote
    first_name = first_name.replace('"', '')
    last_name = last_name.replace('"', '')
    # concat and return
    return '"%s %s"' % (first_name, last_name)

def _dict_val_or_zero(mydict, key):
    if key in mydict:
        return mydict[key]
    else:
        return 0

# return a list with len(dates) with zeros or values in dates_counts_dict
# dates is 'yyyy-mm-dd', dates_counts_dict must be that also
def _expand_dict(dates, dates_counts_dict):
    return [_dict_val_or_zero(dates_counts_dict, i) for i in dates]

def _fetch_news_trend(first_name, last_name):
    mydict = {}
    phrase = urllib.quote(_phrase(first_name, last_name))
    myurl = """http://zookst9.science.uva.nl:8182/\
?query=%s""" % phrase
    datestr_count = urllib.urlopen(myurl)
    # the webserver is too simple, so the only thing that will not
    # hang is retrieve the lines one by one.
    while True:
        line = datestr_count.readline()
        if line == '': # eof
            break
        line = line.strip()
        if line == '': # empty line, assume only the last line is empty
            break

        words = line.split()
        mydict[dt.date(year = int(words[0][0:4]),
                          month = int(words[0][4:6]),
                          day = int(words[0][6:8]))] = int(words[1])
    datestr_count.close()
    return mydict


# assume list of dates, list of counts, generate plot, store in path
# do not overwrite existing ones.
def _store_trend(first_name, last_name, dates, counts, date, path):

    # create path, ensuring that it does not exist.
    if os.path.exists(path):
        sys.stderr.write('Error: filename exists\n')
        sys.exit(2)

    # Geert Wilders counts, plot this as reference query.
    geert_counts = (70L, 57L, 46L, 48L, 66L, 80L, 87L, 85L, 104L, 86L, 90L, 91L, 104L, 88L, 58L,
        67L, 67L, 48L, 63L, 65L, 73L, 67L, 71L, 68L, 73L, 71L, 77L, 46L, 46L, 47L, 83L,
        67L, 77L, 95L, 101L, 88L, 60L, 44L, 37L, 45L, 45L, 45L, 53L, 38L, 53L, 32L,
        48L, 69L, 45L, 56L, 44L, 47L, 64L, 65L, 61L, 68L, 75L, 81L, 77L, 67L, 74L, 67L,
        75L, 64L, 75L, 93L, 54L, 56L, 78L, 47L, 69L, 64L, 58L, 52L, 61L, 66L, 69L, 62L,
        67L, 88L, 65L, 61L, 78L, 66L, 74L, 74L, 87L, 65L, 92L, 91L, 74L, 84L, 73L, 85L,
        82L, 92L, 115L, 117L, 109L, 102L, 108L, 103L, 89L, 84L, 76L, 77L, 70L, 78L,
        121L, 85L, 103L, 58L, 34L, 41L, 56L, 58L, 68L, 72L, 45L, 55L, 50L, 47L)

    # similarly for the news counts.
    geert_news_counts = [20, 35, 57, 28, 21, 38, 72, 35, 40, 14, 46, 18, 36, 15, 7, 3, 10, 3, 11, 20,
        16, 8, 27, 20, 12, 6, 19, 49, 28, 65, 36, 72, 33, 63, 67, 36, 41, 16, 8, 6,
        4, 36, 10, 21, 34, 18, 11, 24, 24, 7, 22, 45, 8, 13, 20, 45, 33, 16, 6, 30,
        7, 5, 7, 6, 16, 5, 5, 5, 12, 23, 17, 14, 17, 5, 4, 29, 39, 11, 34, 10, 1, 7,
        9, 6, 8, 12, 22, 3, 7, 2, 6, 6, 7, 3, 5, 5, 8, 3, 2, 7, 4, 0, 5, 9, 3, 10, 5,
        5, 0, 3, 3, 7, 6, 2, 4, 2, 0, 9, 7, 4, 9, 10]

    # for the news subplot underneath the search volume plot, we
    # need another time series, for now we'll use the geert_counts here.
    news_counts = _expand_dict(dates,
            _fetch_news_trend(first_name, last_name))

    # do some plotting
    plt.clf() # clear current figure
    fig = plt.figure()
    ax = fig.add_subplot(211) # two rows, one col, first plot.
#    ax.plot(dates, geert_counts, '--', color='lightgrey', label = 'Geert Wilders') # a line plot
    ax.plot(dates, counts, '-', color='blue', label = '%s %s' % (first_name, last_name)) # a line plot
    # the current search is plotted as a vertical line, with vlines,
#    ax.vlines(date, 0, max(counts + geert_counts), color='red', linestyle='solid', label='_nolegend_')
    ax.vlines(date, 0, max(counts), color='red', linestyle='solid', label='_nolegend_')
    ax.xaxis.set_major_locator(mpldates.WeekdayLocator(byweekday=mpldates.MO, interval = 2)) # every second monday
    ax.xaxis.set_minor_locator(mpldates.WeekdayLocator(byweekday=mpldates.MO)) # every monday
    ax.xaxis.set_major_formatter(mpldates.DateFormatter('%b %d, %a')) # strftime style date formatting
    ax.legend()
    plt.title('Unique daily browser cookies per day')
    # second axes for the news subplot
    ax2 = fig.add_subplot(212, sharex=ax) # share the x axis of the first subplot.
#    ax2.plot(dates, geert_news_counts, '--', color='lightgrey', label = 'Geert Wilders news volume') # a line plot
    ax2.plot(dates, news_counts, '-', color='blue', label = '%s' % _phrase(first_name, last_name))
#    ax2.vlines(date, 0, max(news_counts + geert_news_counts), color='red', linestyle='solid', label='_nolegend_')
    ax2.vlines(date, 0, max(news_counts), color='red', linestyle='solid', label='_nolegend_')
    ax2.legend()
    fig.autofmt_xdate()
    plt.savefig(path)

    return path

# this is the only exported function of this module right now.
def trend(conn, path, first_name, last_name, date):
    dates, counts = _fetch_trend(conn, first_name, last_name)
    return _store_trend(first_name, last_name, dates, counts, date, path)


# for testing, this module can also be executed by python.
def _main():
    # connect to the db.
    conn = MySQLdb.connect (host = "qassir",
                           user = "bogomil",
                           passwd = "wieowie",
                           db = "people")

    # issue query
    first_name = 'Suze'
    last_name = '"Van Rozelaar"'
    dates, counts = _fetch_trend(conn, first_name, last_name)

    # close connection
    conn.close ()

    # plot query
    print _store_trend(first_name, last_name, dates, counts,
            dt.date(2010, 12, 3), './trends/suze.png')



if __name__ == '__main__':
    _main()
