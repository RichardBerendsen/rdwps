#!/usr/local/bin/bash

# Duplicates the structure with stemmed versions.
# Accepts absolute paths.

htmls_dir=$1
anchor_hrefs_dir=$2
script_dir_base="$( cd "$( dirname "$0" )" && cd .. && pwd )"
lang=$3

if [ "$lang" == "" ]
then
  lang="dutch"
fi

if [ "$#" -lt "2" ]
then
    echo "Error, please provide an absolute htmls directory, an absolute empty directory."
    exit 2
fi
if [ ! -d $htmls_dir ]
then
    echo "Error, htmls dir path is not a directory"
    exit 2
fi

if [ ! -d $anchor_hrefs_dir ]
then
    echo "Error, mirror path is not a directory"
    exit 2
fi

if [ "$(ls -A $anchor_hrefs_dir)" ]
then
    echo "Error, mirror path is not empty"
    exit "2"
fi

echo $htmls_dir
echo $anchor_hrefs_dir
echo $script_dir_base

process_url() {
  dir=$1
  newdir=$2
  url=$3

  # remove the extension
  url_noext=${url%\.*}
  
  #touch "$dir"/"$url"
  hrefs=(`python "$script_dir_base"/py/extract_hrefs.py "$dir"/"$url"`)
  for h in ${hrefs[@]}
  do
    echo $h >> "$newdir"/"$url_noext"
  done
}

process_qry_dir() {
    echo "$1"
    subdir=$htmls_dir"/""$1"
    newdir=$anchor_hrefs_dir"/""$1"

    mkdir $newdir
    cd $subdir
    for u in $(ls -1 | grep '^[0-9]')
    do
      process_url $subdir $newdir $u
    done
    cd ..
}

for qry in $(ls "${htmls_dir}")
do
  process_qry_dir $qry
done



