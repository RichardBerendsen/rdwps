#!/usr/bin/python

from pyPdf import PdfFileReader
import os
import sys
import re
import util
from datetime import datetime

def write_run_conf(base_dir,path,feature_vector_method, occurrence_limit, metric, stopword_file, similarity_threshold, term_window, logged_tfs):
  stopwords = get_stopwords(stopword_file)
  description="";
  now = datetime.now()
  description += "\nTime: " + now.__str__() + "\n"
  description += "Base dir is: " + base_dir + "\n"
  description += "Feature vector method: " + feature_vector_method + "\n"
  description += "Metric: " + metric + "\n"
  description += "Logged tfs: " + str(logged_tfs) + "\n"
  description += "Similarity threshold: " + str(similarity_threshold) + "\n"
  description += "Occurrence limit: " + str(occurrence_limit) + "\n"
  description += "Term window: " + str(term_window) + "\n"
  description += "Stopwords: "
  for s in stopwords:
    description +=  "  " + s
  f=open(path + "/run.conf", "w")
  f.write(description)
  f.close() 


def get_stopwords(file):
  if file == "":
    return []
  else:
    return [r.rstrip() for r in open(file, "r").readlines()]

def get_collection_docs_as_strings(directory):
  collection_as_strings = {}
  abs_path = os.path.abspath(directory)
  queries = os.listdir(directory)
  for q in queries:
    docs_as_strings = get_query_docs_as_strings(directory + "/" + q)
    for d in docs_as_strings:
      collection_as_strings[d] = docs_as_strings[d]
  return collection_as_strings

# Relies that the directory is the query in fomat
# FirstName_LastName
# the keys in the output are of the form query_name|urlid
def get_query_docs_as_strings(directory):
  docs_as_strings = {}
  abs_path = os.path.abspath(directory)
  query = os.path.basename(abs_path)
  files = os.listdir(directory)
  for f in files:
    if f.isdigit():
      file_path = abs_path + "/" + f
      try:
         PdfFileReader(file(file_path, "r"))
         print >> sys.stderr, ("Ignoring pdf " + f)
         docs_as_strings[f] = ""
      except Exception, e:
         docs_as_strings[query + "|" + f] = open(file_path, "r").read()
  return docs_as_strings

# The keys in the output are of the form query_name|urlid.
# The values are lists of urls.
def get_query_docs_as_urls(directory):
  docs_as_urls = {}
  abs_path = os.path.abspath(directory)
  query = os.path.basename(abs_path)
  files = os.listdir(directory)
  for f in files:
    if f.isdigit():
      file_path = abs_path + "/" + f
      docs_as_urls[query + "|" + f] = [l.strip() for l in open(file_path, "r").readlines()]
  return docs_as_urls


def _print_sim_matrix(sim_matrix, labels, stream):
  n = len(sim_matrix)
  if n != len(labels):
    raise Exception("Similarity matrix and labels have different dimensions.")
  for i in range(0, n):
    stream.write("    " + str(labels[i]))
  for i in range(0, n):
    stream.write("\n")
    stream.write(str(labels[i]))
    for j in range(0, n):
      stream.write("  " + str(sim_matrix[i][j]))

def _print_docs(feature_vectors, features, stream):
  for f in sorted(features):
    stream.write("     " + f)
  for k in sorted(feature_vectors.keys()):
    stream.write("\n")
    stream.write(k)
    for f in sorted(features):
      stream.write("   " + str(feature_vectors[k][f]))

def serialize_feature_matrix(out_file, feature_vectors, terms):
  output = open(out_file, "w")
  _print_docs(feature_vectors, terms, output)

def serialize_similarity_matrix(out_file, sim_matrix, labels):
  output = open(out_file, "w")
  _print_sim_matrix(sim_matrix, labels, output)

def serialize_clustering(out_file,labelled_clustering):
  output = open(out_file, "w")
  #pprint.pprint(labelled_clustering, output)
  print >> output, labelled_clustering

def deserialize_feature_matrix(feature_matrix_file):
  feature_matrix = {}
  features = []
  file = open(feature_matrix_file,"r")
  lines = file.readlines()
  features = re.split("\s+", lines[0].strip())
  for line in lines[1:]:
    toks = re.split("\s+", line.strip())
    docid = toks[0]
    dochash = {}
    for i in range(1, len(toks)):
      dochash[features[i-1]]= toks[i]
    feature_matrix[docid] = dochash
  file.close()
  return feature_matrix, features

def deserialize_similarity_matrix(sim_matrix_file):
  file = open(sim_matrix_file, "r")
  lines = file.readlines()
  labels = re.split("\s+", lines[0].strip())
  matrix=[]
  if len(lines) != len(labels) + 1:
    print len(lines), len(labels)
    raise Exception("Malformed similarity matrix in file: " + sim_matrix_file)
  for n in range (1, len(lines)):
    row_n = [float(i) for i in re.split("\s+", lines[n].strip())[1:]]
    if len(row_n) != len(labels):
      raise Exception("Malformed similarity matrix in file: " + sim_matrix_file)
    matrix.append(row_n)
  file.close()
  return matrix, labels

def deserialize_clustering_to_top_level_only(clustering_file):
  file = open(clustering_file, "r")
  lines = file.readlines()
  if len(lines) != 1:
    raise Exception("Malformed clustering file: " + clustering_file)
  clustering_flat = util.string_array_extract_first_level(lines[0])
  file.close()
  return clustering_flat

if __name__=="__main__":
  feature_matrix, features = deserialize_feature_matrix(sys.argv[1])
  serialize_feature_matrix(sys.argv[1] + ".test", feature_matrix, features)
  sim_matrix, labels = deserialize_similarity_matrix(sys.argv[2])
  serialize_similarity_matrix(sys.argv[2] + ".test", sim_matrix, labels)
  print labels
  #print sim_matrix[93]
  #print labels[93]
  #clustering = deserialize_clustering_to_top_level_only(sys.argv[1])
  #serialize_clustering(sys.argv[3] + ".test", clustering)
  #print clustering
