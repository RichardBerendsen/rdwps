"""read queries from db, store file 'auto_increment_id.png' with search history trend in ./trends/"""

import MySQLdb
import MySQLdb.cursors
import trend_uid
import get_queries
import datetime


def _connect():
	return MySQLdb.connect(host = "qassir",
                           user = "bogomil",
                           passwd = "wieowie",
                           db = "people")


def _main():

    queries = get_queries.get_queries()

    conn = _connect()

    for query in queries:
        path = 'trends/%s.png' % query['idx']
        print path
        trend_uid.trend(conn, path,
                query['FirstName'], query['LastName'], datetime.date(2011, 02, 28))

	# close connection
    conn.close ()


if __name__ == '__main__':
    _main()
