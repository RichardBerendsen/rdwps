#!/usr/bin/python
import tornado.httpserver
import tornado.ioloop
import tornado.web
import sys


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello, world")


if __name__ == "__main__":

    if len(sys.argv) < 3:
      print "Usage: web_server.py port static_path"
      sys.exit(2)

    port=int(sys.argv[1])
    static_path=sys.argv[2]
    print "port: %s" % port
    print "static path: %s" % static_path

    settings = {"static_path" : static_path}

    application = tornado.web.Application([
      (r"/", MainHandler),
    ], **settings)
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(port)

    tornado.ioloop.IOLoop.instance().start()
