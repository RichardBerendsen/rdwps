import MySQLdb
import MySQLdb.cursors
from get_queries import get_queries
import sys

def _connect():
	return MySQLdb.connect (host = "qassir",
						   user = "bogomil",
						   passwd = "wieowie",
						   db = "people",
                           cursorclass = MySQLdb.cursors.DictCursor)


def _update_search_results(query):
    conn = _connect()
    cursor = conn.cursor()
    cursor.execute("update search_results as s set query_id = %s where s.firstname = %s and s.lastname = %s", (query['idx'], query['FirstName'], query['LastName']))
    cursor.close()
    conn.close()

if __name__ == "__main__":

    queries = get_queries()
    for q in queries:
        _update_search_results(q)

