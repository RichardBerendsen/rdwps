"""
store a new wieowie run in the db that is merely a merge of several previous wieowie runs

Usage:
    %s run_id run_id [run_id ...]
"""

import sys
import db


if len(sys.argv) < 3:
    print __doc__ % sys.argv[0]
    sys.exit(2)

run_ids = [int(i) for i in sys.argv[1:]]

def get_union_of_queries_in_runs(conn, run_ids):
    """
    retrieve queries for which any of the runs have clusters
    """
    sql_run_ids = ','.join(["%s" % run_id for run_id in run_ids])
    sql_select = "select distinct query_id from run_cluster where run_id in (%s)" % sql_run_ids
    return db.select(sql_select, conn=conn)

def get_cluster_ids(conn, run_id, query_id):
    """
    get ids of stored clusters for this run for this query
    """
    sql = "select id from run_cluster where run_id = %s and query_id = %s;"
    params = (run_id, query_id)
    return db.select(sql, params=params, conn=conn)

def insert_doc_ids(conn, new_run_id, query_id, old_cluster_id):
    """
    finds all doc_ids that belonged to the cluster with `cluster_id`,
    and inserts a new cluster with run_id `new_run_id`, and stores all
    found doc_ids in run_clustering with the new cluster_id
    """
    # insert new cluster
    sql_insert_cluster = "insert into run_cluster (run_id, query_id) values (%s, %s)"
    params_insert_cluster = (new_run_id, query_id)
    new_cluster_id = db.insert_select_last_insert_id(sql_insert_cluster, params_insert_cluster)

    # insert doc_ids corresponding to old_cluster into new cluster with one neat sql query:
    sql_insert_doc_ids = "insert into run_clustering (cluster_id, normalized_url_id) select %s as cluster_id, normalized_url_id from run_clustering where cluster_id = %s";
    params_insert_doc_ids = (new_cluster_id, old_cluster_id)
    db.insert(sql_insert_doc_ids, params=params_insert_doc_ids, conn=conn)

# store run and retrieve last stored run.
merged_run_name = "merged_run_of_run_ids_%s" % ('_'.join(["%s" % run_id for run_id in run_ids]),)
sql_insert_run = "insert into run (name, description) values (%s, %s)"
params_insert_run = (merged_run_name, "")
new_run_id = db.insert_select_last_insert_id(sql_insert_run, params_insert_run)

conn=db.connect()

# store clusters
for run_id in run_ids:
    for query in get_union_of_queries_in_runs(conn, run_ids):
        for cluster in get_cluster_ids(conn, run_id, query['query_id']):
            insert_doc_ids(conn, new_run_id, query['query_id'], cluster['id'])

conn.close()
