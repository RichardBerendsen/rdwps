#!/bin/bash
#
# Assumes the script resides alongside the other preprocessing scripts.

print_usage() {
    cat 1>&2 <<EOF
Usage: 
$1 nonsliced_matrices_dir sliced_matrices_dir output_dir_clustering_from_cliced output_dir_clustering_all run_name sim_threshold_web sim_threshold_social social_weight

EOF
}

if [ "$#" -ne "8" ] || [ ! -d "$1" ] || [ ! -d "$2" ] || [ ! -d "$3" ] || \
   [ ! -d "$4" ] || [ "$(ls -A "$3")" ] || [ "$(ls -A "$3")" ]

then
    print_usage $0
    exit 2
fi

matrices_dir_nonsliced=$(echo "$1" | sed 's+/*$++')
matrices_dir_sliced=$(echo "$2" | sed 's+/*$++')
result_dir_web=$(echo "$3" | sed 's+/*$++')
result_dir_all=$(echo "$4" | sed 's+/*$++')
run_name=$5
sim_thresh_web=$6
sim_thresh_social=$7
social_weight=$8
script_dir="$( cd "$( dirname "$0" )" && pwd )"
base_script_dir="$( cd "$( dirname "$0" )" && cd .. && pwd )"

echo "Clustering ..."
echo $sim_thresh_social

python $base_script_dir/py/cluster.py --similarity-threshold=$sim_thresh_web $matrices_dir_sliced $result_dir_web --dry-run

echo "$base_script_dir/bash/assimilate.sh $matrices_dir_nonsliced $result_dir_web $result_dir_all $run_name $sim_thresh_social $social_weight" 

bash $base_script_dir/bash/assimilate.sh $matrices_dir_nonsliced $result_dir_web $result_dir_all $run_name $sim_thresh_social $social_weight 

