"""
Extracts and tokenizes links from an html page.
Outputs a list of tokens.
The tokens include the top level domain and the elements of the path. If the last element of the path ends in .ext the .ext is ignored.
The query is ignored.
Usage:
  python program input_file
"""

import sys
import os
import re

infile = sys.argv[1]
f = open(infile, "r")

for l in f:
 l = l.strip()
 m = re.search(r'https?://([^\/"\']*)["\/\']', l)
 if m :
   domain = m.group(1)
   subdomains = re.split("\.", domain)
   if len(subdomains) < 2:
     toplevel_domain = subdomains[0]
   else:
     toplevel_domain = subdomains[-2]
   print toplevel_domain
   m1 = re.search(r'https?://(?:[^\/"\']*)\/([^?"\']+?)["?\']', l)
   if m1:
     path = m1.group(1)
     path_toks = re.split(r'/', path)
     for t in path_toks:
       if len(t) < 1:
         continue
       toks1 = re.split("\.", t)
       if len(toks1) > 1:
         for t1 in toks1[:-1]:
           print t1
       else:
         for t1 in toks1:
           print t1
     #print path
     #print l

