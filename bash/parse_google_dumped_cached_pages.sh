#!/usr/local/bin/bash

if [ "$#" -ne 1 ] || [ ! -d "$1" ]
then
    echo "Please specify the directory with the google cached pages"
    exit 2
fi
dirname="$1"

cwd="$(pwd)"
script_path="$cwd/$(dirname $0)"

for file in $(find $dirname -name '*.txt')
do
    google_cached_url_id="$(basename "$file" ".txt")"
    python "$script_path/../py/parse_w3m_output_of_google_cached_result.py" "$google_cached_url_id" < "$file"
done
