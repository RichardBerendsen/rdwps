"""given a click, either accept it or reject it
How to get the click?"""


import MySQLdb
import MySQLdb.cursors
import datetime as dt
import operator


# we sum dwellingtime on each URL over all sessions by a user, because a later
# session could contradict an earlier session.
# we select only users that have clicks on at least two types (disambigusers)
def get_users(conn, first_name, last_name):
	cursor = conn.cursor()
	cursor.execute("""select s.UserID \
from Searches_sessions as s, \
Clickouts_sessions as c \
where s.FirstName = %s and s.LastName = %s and s.SearchID = c.SearchID \
group by s.UserID having count(distinct c.type) >= 2""",
			(first_name, last_name))
	users = cursor.fetchall()
	cursor.close()
	return users


def _is_more_google_results(URL):
	pass

def _is_more_yahoo_results(URL):
	pass

def _is_more_bing_results(URL):
	pass

def _is_more_hyves_profiles(URL):
	pass

def _is_more_facebook_profiles(URL):
	pass

def _is_more_linkedin_profiles(URL):
	pass

def _is_more_twitter_profiles(URL):
	pass

def _is_more_myspace_profiles(URL):
	pass

def _is_more_

# a kind of placeholder function.
def _is_not_useful(URL):
	return false

# click is a dict with a click record, contains URL, type (tld), etc.
# returns true if the click is not a result document, or an add, but
# a 'more results' click, such as 'more google results', 'more yahoo results', etc.
def _is_more_results_click(click):
	pass



def _main():
	type_to_class_and_handler = {
			'hyves':('social', _is_hyves_profile),
			'facebook':('social', _is_facebook_profile),
			'schoolbank':('social', _is_schoolbank_profile),
			'linkedin':('social', _is_linkedin_profile),
			'google':('search', _is_google_search_result),
			'yahoo':('search', _is_yahoo_search_result),
			'bing':('search', _is_bing_search_result),
			'twitter':('social', _is_twitter_profile),
			'images':('media', _is_not_useful),
			'myspace',('social', _is_myspace_profile),
			'tags':('extracts', _is_not_useful),
			'docs':('media', _is_not_useful),
			'facts':('extracts', _is_not_useful),
			'googleblogs':('search', _is_not_useful),
			'detelefoongids':('extracts', _is_not_useful),
			'flickr':('social', _is_not_useful),
			'wieowie':('ads_news_nu_populair', _is_not_useful),
			'related':('extracts', _is_not_useful),
			'phonenumbers':('extracts', _is_not_useful),
			'xing':('social', _is_not_useful),
			'hi5':('social', _is_not_useful),
			'amazon':('search', _is_not_useful),
			'netlog':('social', _is_not_useful),
			'emailaddresses':('extracts', _is_not_useful),
			'yahooprofiles':('social', _is_not_useful),
			'meetup':('social', _is_not_useful),
			'googlenews':('search', _is_not_useful),
			'youtube':('media', _is_not_useful),
			'plaxo':('social', _is_not_useful),
			'ning':('social', _is_not_useful),
			'naymz':('social', _is_not_useful),
			'googleprofiles':('social', _is_not_useful),
			'typepad':('social', _is_not_useful),
			'friendster':('social', _is_not_useful),
			'blogger':('social', _is_not_useful),
			'cuil':('search', _is_not_useful),
			'lastfm':('social', _is_not_useful),
			'jigsaw':('social', _is_not_useful),
			'foursquare':('social', _is_not_useful),
			'tvblik':('misc', _is_not_useful),
			'wikipedia':('wikipedia', _is_not_useful),
			'vermist':('missing', _is_not_useful),
			'gezocht':('wanted', _is_not_useful),
			'friendfeed':('social', _is_not_useful),
			'vimeo':('media', _is_not_useful),
			'posterous':('social', _is_not_useful),
			'profiles':('wieowie-profiles', _is_not_useful),
			'vinden':('search' _is_not_useful),
	}

	# connect to the db.
	conn = MySQLdb.connect (host = "qassir",
						   user = "bogomil",
						   passwd = "wieowie",
						   db = "people",
						   cursorclass = MySQLdb.cursors.DictCursor)


	# select queries such that:
	# 1) there are clickouts on at least h hyves profiles (for estimating ambiguity)
	# 2) there are at least s searches such that:
	# 2a) the search has at least c clickouts on different URLs.
	# Now for each clickout: the clickout has to be useful in order to be counted. (or should
	# I do this only at a later stage?) A faster approach, programming wise,
	# and computation time wise would be to just take the numbers a bit higher
	# than the minimum, and then only for the queries that you select you would
	# compute the usefulness. so what we do, is iterate over all queries and
	# compute this? It hardly seems feasible, It may be worthwhile to do the
	# hyves computation in sql alone. So just add to the Queries table the number of different
	# hyves accounts clicked on for this query.

	# close connection
	conn.close ()


####### main code
if __name__ == '__main__':
	_main()
