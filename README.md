Result Disambiguation in Web People Search: Code
================================================

This is code that was used for the ECIR 2012 paper on Result Disambiguation in
Web People Search by Berendsen, Kovachev, Nastou, De Rijke, and Weerkamp.

It is largely undocumented as of now, but may contain some useful scripts for
people wanting to get started with the data that was released with the paper,
see http://ilps.science.uva.nl/resources/ecir2012rdwps/

This repository is only a snapshot of scripts. It is not maintained. It does
not even contain all scripts used for the paper. There is a repository that
contains all code, as well as experimental data and latex code for the paper.
There are plans to develop some more generally useful scripts from that
repository and add some documentation to them. At that point, this repository
will become obsolete and will be removed.

Evaluation
----------
The data collection in our paper differs slightly from that used in the WePS
campaigns. In the WePS campaigns, results were obtained from the Yahoo! search
engine. In our dataset, results were obtained from several search engines and
then merged. This is reflected in the XML format of our ground truth files.
The WEPS-1 evaluation script cannot be used with our ground truth for that
reason. We used the script in `py/weps_eval.py` instead. You can use it or
adapt it if you want.

### Evaluation with the WePS-1 evaluation script
To get the evaluation scores for the WePS-1 test queries in
/scratch1/rberend/weps2007_data_1.1/etc I ran the following command from this
directory:

$ java -jar java/wepsEvaluation.jar
/scratch1/rberend/weps2007_data_1.1/test/truth_files/official_annotation/
/scratch1/rberend/exp/weps2007/runs/
/scratch1/rberend/exp/weps2007/results/first_exp_weps/ -ALLMEASURES -AllInOne
-OneInOne -Combined

To get the evaluation scores for the WePS-2 test queries in
/scratch/bb/weps2/weps-2/data/test/gold_standard/ I ran the following command
from this directory, 
NB: first I had to change all the filenames to .clust.xml instead of just .xml
and uppercase the person names
or else the weps-1 scorer would give an exception:

$ java -jar java/wepsEvaluation.jar \
/scratch1/rberend/weps-2/data/test/gold_standard/ \
/scratch1/rberend/exp/weps-2/runs/ \
/scratch1/rberend/exp/weps-2/results/first_weps2_experiment \
-ALLMEASURES -AllInOne -OneInOne -Combined
