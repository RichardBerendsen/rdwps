"""
get topic stats from topic_stats, and output latex table.
"""

import db
import sys

sql = "select query_id, topic, discarded_urls - discarded_profile_urls as discarded_web_docs, total_urls - profile_urls as web_docs, profile_urls, discarded_profile_urls from topic_stats order by total_urls - discarded_urls desc;"

res = db.select(sql)

cols = {}

print "query_id\ttopic\tdiscarded_web_docs\tweb_docs\tprofile_urls\tdiscarded_profile_urls"
for rec in res:
    print "%(query_id)s\t%(topic)s\t%(discarded_web_docs)s\t%(web_docs)s\t%(profile_urls)s\t%(discarded_profile_urls)s" % rec

