#!/bin/bash
#
# Assumes the script resides alongside the other preprocessing scripts.
# First argument a weps crawls directory.
# Second argument an empty results directory.

print_usage() {
    cat 1>&2 <<EOF
Usage:
$1 similarity_matrices_dir output_dir run_name sim_threshold_for_web sim_threshold_for_social social_weight

output_dir will contain:
  - a dir with sliced matrices containing rows and coulms for only the non-social pages
  - a dir with clustering based on the sliced matrices
  - a dir with the final clusters
EOF
}

if [ "$#" -ne "6" ] || [ ! -d "$1" ] || [ ! -d "$2" ] || [ "$(ls -A "$2")" ]
then
    print_usage $0
    exit 2
fi

matrices_dir=$(echo "$1" | sed 's+/*$++')
output_dir=$(echo "$2" | sed 's+/*$++')
run_name=$3
sim_thresh_web=$4
sim_thresh_social=$5
soc_weight=$6
sliced_dir=$output_dir/"sliced"
cluster_sliced_dir=$output_dir/"cluster_sliced"
cluster_all_dir=$output_dir/"cluster_all"
script_dir="$( cd "$( dirname "$0" )" && pwd )"
base_script_dir="$( cd "$( dirname "$0" )" && cd .. && pwd )"


mkdir $sliced_dir
mkdir $cluster_sliced_dir
mkdir $cluster_all_dir

$base_script_dir/bash/slice_matrices.sh $matrices_dir $sliced_dir

#$1 nonsliced_matrices_dir sliced_matrices_dir output_dir_clustering_from_cliced output_dir_clustering_all run_name sim_threshold_web sim_threshold_social social_weight

$base_script_dir/bash/cluster_assimilate.sh $matrices_dir $sliced_dir $cluster_sliced_dir $cluster_all_dir $run_name $sim_thresh_web $sim_thresh_social $soc_weight

description="An assimilate-social-on-top-of-web type of experiment. similarity_matrices_dir=$matrices_dir sim_threshold_web=$sim_threshold_web sim_threshold_social=$sim_threshold_social social_weight=$soc_weight "

echo $description > $output_dir/run.conf

$base_script_dir/py/store_clustering_in_db.py $cluster_all_dir $run_name $description
