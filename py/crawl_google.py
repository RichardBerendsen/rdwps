#!/usr/bin/python

from BeautifulSoup import BeautifulSoup
import urllib # for quote function
import urllib2 # for urlopen
import urlparse # to split the url, and urlencode only path part.
import get_queries
import sys
import time
from pprint import pprint
from util import normalize
from get_queries import get_queries
from store_urls import store_urls
import random
import time
import os
import re
import MySQLdb
import sys

def _connect():
	return MySQLdb.connect (host = "qassir",
						   user = "bogomil",
						   passwd = "wieowie",
						   db = "people",
                           cursorclass = MySQLdb.cursors.DictCursor)

def _parse_search_results(page):
    soup = BeautifulSoup(page)
    ts = soup.findAll(lambda t: t.name=="a" and t.has_key("class") and\
        t.has_key("href") and t.parent.name=="h3" and t.parent.parent.name == "li")
    urls = [t['href'] for t in ts ]
    return urls

def _query_already_processed(query):
    conn = _connect()
    cursor = conn.cursor()
    cursor.execute("select count(*) as count from search_results where \
FirstName = %s and LastName = %s and type = 'google'", (query['FirstName'], query['LastName']))
    row = cursor.fetchone()
    if row['count'] > 0:
        return True
    cursor.close()
    conn.close()
    return False

def _main():
    queries = get_queries()
    for query in queries:
        filepath = "../../exp/google_search_results_per_query/%s.html" % query['idx']
        if _query_already_processed(query):
            continue
        try:
            f = open(filepath, "r")
        except IOError, e:
            sys.stderr.write('IOError while trying to open\n%s\nfor reading\n' % filepath)
            sys.exit(2)
        else:
            urls = _parse_search_results(f.read())
            n_urls=[normalize(u) for u in urls]
            store_urls(query['FirstName'], query['LastName'], 'google', urls, n_urls)

if __name__ == "__main__":
	_main()
