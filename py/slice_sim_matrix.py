#!/usr/bin/python

import serializer
import extractor
import sys

def get_index_of_social(labels):
  for i in range(0, len(labels)):
    if extractor._infer_type(labels[i]) != "none":
      return i
  return -1

def delete_id(sim_matrix, k, labels):
  #print labels
  #print k
  #print sim_matrix
  n = len(sim_matrix)
  for i in range(k, n-1):
    sim_matrix[i] = sim_matrix[i+1]
  sim_matrix = sim_matrix[:-1]
  for i in range(0, n-1):
    row = sim_matrix[i]
    for j in range(k, len(row) - 1):
      row[j] = row[j+1]
    sim_matrix[i] = row[:-1]
  for i in range(k, n-1):
    labels[i] = labels[i+1]
  labels = labels[:-1]
  #print sim_matrix
  #print labels
  return sim_matrix, labels

def slice(sim_matrix, labels):
  while True:
    social_index = get_index_of_social(labels)
    if social_index > -1:
      sim_matrix, labels = delete_id(sim_matrix, social_index, labels)
    else:
      break
  return sim_matrix, labels

if __name__=="__main__":
  usage = "Usage: script in_file out_file"
  matrix, labels = serializer.deserialize_similarity_matrix(sys.argv[1])
  sliced, labels = slice(matrix, labels)
  serializer.serialize_similarity_matrix(sys.argv[2], sliced, labels)
