#!/usr/bin/python

import MySQLdb
import MySQLdb.cursors
import sys

def _connect():
	return MySQLdb.connect (host = "qassir",
						   user = "bogomil",
						   passwd = "wieowie",
						   db = "people",
                           cursorclass = MySQLdb.cursors.DictCursor)


def _insert_translated_url(conn, url, translated_url):
    cursor = conn.cursor()
    cursor.execute("insert into google_cached_urls (URL, translated_URL) values (%s, %s)", (url, translated_url))
    cursor.close()

def _update_translated_url(conn, url, translated_url):
    cursor = conn.cursor()
    cursor.execute("update google_cached_urls set translated_URL = %s where URL = %s", (translated_url, url))
    cursor.close()

def _store_translated_url(conn, url, translated_url):
    # first check if url has not been stored already
    cursor = conn.cursor()
    cursor.execute("select * from google_cached_urls where URL = %s", (url,))
    res = cursor.fetchone()
    cursor.close()
    if res is None:
        # no result found apparently, we can insert.
        _insert_translated_url(conn, url, translated_url)
    else:
        # rule already stored, but if we have a better one, go ahead
        if translated_url != '' and res['translated_URL'] == '':
            # we can update
            _update_translated_url(conn, url, translated_url)


def _main():
    pairs = sys.stdin.readlines()
    conn = _connect()
    for pair in pairs:
        pair = pair.strip()
        urls = pair.split()
        if len(urls) == 1:
            urls += ['']
        _store_translated_url(conn, urls[0], urls[1])
    conn.close()


if __name__ == "__main__":
    _main()
