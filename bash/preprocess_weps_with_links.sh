#!/usr/local/bin/bash

# Assumes the script resides alongside the other preprocessing scripts.
# First argument a weps crawls directory.
# Second argument an empty results directory.
# 

print_usage() {
    cat 1>&2 <<EOF
Usage: 
$1 weps_2_crawls_dir empty_dir which_weps

where which_weps must be 1 (weps-1-test), 2 (weps-2) or 3 (weps-1-train)
EOF
}

if [ "$#" -ne "3" ] || [ ! -d "$1" ] || [ ! -d "$2" ] || [ "$(ls -A "$2")" ]
then
    print_usage $0
    exit 2
fi

if [ "$3" -ne "1" ] && [ "$3" -ne "2" ] && [ "$3" -ne "3" ]
then
    print_usage $0
    exit 2
fi

crawls_dir=$(echo "$1" | sed 's+/*$++')
result_dir=$(echo "$2" | sed 's+/*$++')
script_dir="$(dirname "$0")"

mkdir $result_dir/noscript_stripped
mkdir $result_dir/links_with_html_extension
mkdir $result_dir/w3mmed
mkdir $result_dir/w3mmed_with_links
mkdir $result_dir/stemmed
mkdir $result_dir/stemmed_with_links
mkdir $result_dir/matrices_from_stemmed
mkdir $result_dir/matrices_from_stemmed_with_links

echo "Removing noscript tags from html pages.. so that w3m won't follow redirects in noscript tags" 1>&2
$script_dir/strip_noscript_tags.sh $crawls_dir $result_dir/noscript_stripped

echo "Make symbolic links with .html extensions.. so that w3m will not just dump the source." 1>&2
$script_dir/make_symbolic_links_to_crawled_pages_weps.sh $result_dir/noscript_stripped $result_dir/links_with_html_extension

echo "Passing documents through w3m.." 1>&2
$script_dir/w3m_dump_htmls.sh $result_dir/links_with_html_extension $result_dir/w3mmed

echo "Passing documents through w3m with links.." 1>&2
$script_dir/w3m_dump_and_parse_links.sh $result_dir/links_with_html_extension $result_dir/w3mmed_with_links

echo "Tokenizing with english tokenizer and stemming.. " 1>&2
$script_dir/stem_htmls_after_w3m.sh $result_dir/w3mmed $result_dir/stemmed english

echo "Tokenizing with english tokenizer and stemming with links.. " 1>&2
$script_dir/stem_htmls_after_w3m.sh $result_dir/w3mmed_with_links $result_dir/stemmed_with_links english

echo "Creating matrices from stemmed documents" 1>&2
$script_dir/../py/create_matrices.py \
"--input-dir=$result_dir/stemmed" \
"--output-dir=$result_dir/matrices_from_stemmed" \
"--occurrence-limit=5" \
"--logged-tfs" \
"--run-name=weps_${which_weps}_glob_log_tf_occ5_from_stemmed" \
"--weps-run=$which_weps" \
"--feature-vector-creator=tf_idf_vectors_global"

echo "Creating matrices from stemmed documents with links" 1>&2
$script_dir/../py/create_matrices.py \
"--input-dir=$result_dir/stemmed_with_links" \
"--output-dir=$result_dir/matrices_from_stemmed_with_links" \
"--occurrence-limit=5" \
"--logged-tfs" \
"--run-name=weps_${which_weps}_glob_log_tf_occ5_from_normalized" \
"--weps-run=$which_weps" \
"--feature-vector-creator=tf_idf_vectors_global"
