#!/usr/bin/python

import sys
import connect


def _get_search_results(conn):
    cursor = conn.cursor()
    cursor.execute("select firstname, lastname, query_id, id from search_results where name_found_in_crowbarred_doc_2 = 'yes' group by query_id, id order by query_id, id")
    srch_results = cursor.fetchall()
    cursor.close()
    return srch_results


if __name__ == "__main__":

    conn = connect.connect()
    search_results = _get_search_results(conn)
    for s in search_results:
      fname = s['firstname']
      lname = s['lastname']
      chopped_lname = lname.replace("\"", "")
      chopped_lname = chopped_lname.replace(" ", "_")
      print str(s['query_id']) + "_" + fname + "_" + chopped_lname + ";" + str(s['id'])
    conn.close()

