#!/usr/local/bin/bash

# walk over stored crawls from weps,
# and mirror that directory structure with only the needed symbolic links to the actual pages

if [ "$#" -lt "2" ]
then
    echo "Error, please provide crawls directory and an empty directory and optionally an extension to be appended to links. (e.g. .html)"
    exit 2
fi

# links relative to self path
crawls_path=$1
mirror_path=$2
extension=$3
self_path=$(dirname "$0")


if [ ! -d $crawls_path ]
then
    echo "Error, crawls dir path is not a directory"
    exit 2
fi

if [ ! -d $mirror_path ]
then
    echo "Error, mirror path is not a directory"
    exit 2
fi

if [ "$(ls -A $mirror_path)" ]
then
    echo "Error, mirror path is not empty"
    exit "2"
fi

for query_id in $(ls "${crawls_path}")
do
    # create query directory in mirror_path if it is not there 
    if [ ! -e "${mirror_path}/${query_id}" ]
    then
        mkdir "${mirror_path}/${query_id}"
    fi

    for doc_id in $(ls "${crawls_path}/${query_id}")
    do
        doc_path="${crawls_path}/${query_id}/${doc_id}"
        # create symbolic link if it is not already there and if the target exists
        if [ ! -e "${mirror_path}/${query_id}/${doc_id}${extension}" ] \
            && [ -e "${crawls_path}/${query_id}/${doc_id}" ]
        then
            ln -s \
            "${crawls_path}/${query_id}/${doc_id}" \
                "${mirror_path}/${query_id}/${doc_id}${extension}"
        fi
    done
done
