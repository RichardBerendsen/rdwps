"""
Usage: %s annotator annot_dir

Where annot_dir is a directory with in it query directories, and in each query
directory an annotation, a clustering: This is the directory structure that we
assume for each query directory:

>discard 
>>discard_name_not_found
>>>37792
>>>...
>>discard_too_private
>>>239
>keep
>>girl_with_pearl_earring
>>>2398
>>>823
>>>...
>>paardrijden_hockey_harderwijk
>>>309
>duplicates
>>set_1
>>>2398
>>>823

Why this script: 
Thomas introduced a somewhat different format for storing stuff in the db.
This format also allows for storing duplicate sets of duplicate documents. But
now I do have to write a new script of course :-)
"""

import sys
import os
import db
import re
import util

def _store_discarded_prop_cluster(conn, annotator, query, cluster_dir, discard = 'no'):
    try:
        docs = os.listdir(cluster_dir)
    except OSError, e:
        print >> sys.stderr, e
        print >> sys.stderr, "Ignoring exception: we just ignore files that are no directories here, skipping"
    else:
        sql = "update cluster set discard = %s where query_id = %s and author = %s and name = %s"
        cluster_name = os.path.basename(cluster_dir)
        params = (discard, query['idx'], annotator, cluster_name)
        db.update(sql, params, conn)

def _store_discarded_prop_clusters(conn, annotator, query, cluster_dirs, discard = 'no'):
    for cluster_dir in cluster_dirs:
        if os.path.basename(cluster_dir) == "__uniques": # a special cluster directory which holds documents that could not be clustered
            continue
        else:
            _store_discarded_prop_cluster(conn, annotator, query, cluster_dir, discard)

def _store_discarded_prop(conn, annotator, query_dir):
    base = os.path.basename(query_dir)
    print >> sys.stderr, base
    query = util.get_query_from_dirname(conn, base)
    if query is None:
        print >> sys.stderr, "Query for dir `%s` not found in the database, skipping" % query_dir
        return

    # store discarded
    try:
        discard_dirs = os.listdir("%s/discard" % query_dir)
    except OSError, e:
        print >> sys.stderr, e
        sys.exit(2)
    else:
        print >> sys.stderr, "Storing discard clusters"
        _store_discarded_prop_clusters(conn, annotator, query, [query_dir + "/discard/" + discard_dir for discard_dir in discard_dirs], 'yes')

def check_annotator(conn, annotator):
    cursor = conn.cursor()
    cursor.execute("show columns from cluster like 'author'")
    res = cursor.fetchone()
    cursor.close()
    type = res['Type']
    prog = re.compile('enum\((.*)\)')
    match = prog.search(type)
    if match is None:
        print >> sys.stderr, "enum column parsed incorrectly"
        sys.exit(3)
    enum_values = match.group(1)
    annotators = enum_values.split(',')
    return "'%s'" % annotator in annotators

def _main(args):

    if len(args) != 3:
        print >> sys.stderr, __doc__ % args[0]
        return 2

    annotator = args[1]
    annot_dir = args[2].rstrip('/')

    try:
        query_dirs = os.listdir(annot_dir)
    except OSError, e:
        print >> sys.stderr, e
        return 2
    else:
        conn = db.connect()
        if not check_annotator(conn, annotator):
            print >> sys.stderr, "Annotator %s not known in the db, please insert it first" % annotator
            return 2
        for query_dir in query_dirs:
            print >> sys.stderr, "Processing query dir %s" % query_dir
            _store_discarded_prop(conn, annotator, annot_dir + "/" + query_dir)
        conn.close()

if __name__ == "__main__":
    sys.exit(_main(sys.argv))
