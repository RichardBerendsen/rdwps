import MySQLdb
import MySQLdb.cursors
import db

def _get_queries(conn, query_id_from, query_id_to):
    cursor = conn.cursor()
    cursor.execute("select * from selected_queries where idx >= %s and idx <= %s", (query_id_from, query_id_to))
    queries = cursor.fetchall()
    cursor.close()
    return queries

def get_queries(query_id_from = 3, query_id_to = 45):
    conn = db.connect()
    queries = _get_queries(conn, query_id_from, query_id_to)
    conn.close()
    return queries
