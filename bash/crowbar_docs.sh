#!/bin/bash

# read URLs from stdin

# idea, crowbar docs, to allow Ajax code to run.  crowbar only docs though that
# have been proven to be 'in_weps_corpus': contain the name, are of the right
# mimetype.  then store these, and then wget the page requisites for these
# pages. This way we will have a dataset with images and everything, completely
# offline, however small it will be useful.

# format of input lines:
# query_id ; normalized_url_id ; time_out; url


if [ "$#" -ne 1 ]
then
	echo "Error, please provide crawls directory to write html files to"
    exit 2
fi
mypath=$1
if [ ! -d "$mypath" ]
then
    echo "Error, crawls dir path is not a directory"
    exit 2
fi
if [ "$(ls -A "$mypath")" ]
then
    echo "Notice: crawls dir is not empty"
fi

# let's go to the root path where we will store our crawl hierarchy
cwd="$(pwd)"
self_path="$cwd/$(dirname "$0")"
cd "${mypath}"
abs_path=$(pwd)
unset mypath

while read line;
do
    query_id=$(echo "$line" | cut -f 1 -d ';')
    normalized_url_id=$(echo "$line" | cut -f 2 -d ';')
    timeout=$(echo "$line" | cut -f 3 -d ';')
    url=$(echo "$line" | cut -f 4 -d ';')


    # create query directory if it is not there 
    if [ ! -e "${abs_path}/${query_id}" ]
    then
        mkdir "${abs_path}/${query_id}"
    fi

    cd "${abs_path}/${query_id}"

	if [ -e "${normalized_url_id}.html" ]
	then
		echo "Notice: skipping ${normalized_url_id}.html, it already exists."
		continue
	fi

    # let's sleep a bit, according to the timeout in our input
	sleep "$timeout"

    # assumes:
    # XULRUNNER is installed
    # Crowbar (http://simile.mit.edu/wiki/Crowbar) is installed
    # Crowbar is running (and listening on 127.0.0.1:10000)
    # Run crowbar (on MacOSX) with the command:
    # $> /Applications/Crowbar.app/Contents/MacOS/xulrunner
    "${self_path}/timeout.sh" -t 120 curl -s --data "url=$url&delay=10000" "http://127.0.0.1:10000" > "${normalized_url_id}.html"

    # let's go back to the root path (in absolute form)
    cd "${abs_path}"
done
