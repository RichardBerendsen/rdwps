#!/usr/bin/perl -w
$|=1;
use strict;
use warnings;

my($out_dir,@in_dirs)=@ARGV;

my $total_name_file_pairs=0;
my $no_top_terms=5;

my $max_neighbors=1;
my $idf_param='global';
#my $idf_param='local';
my $stemming=0;
#my $stemming=1;
my $min_sim_global=0.1;
#my $min_sim_global=0.2;
#my $min_sim_global=0.09;
my $add_hypernyms=0;

my $max_name_dist=100;

my $point_point_weight=0.98;
my $point_centroid_weight=(1-$point_point_weight)/2;

my $max_centroid_terms=200;
my $hypernym_level=2;

#my $strategy_global='centroid-hypernym';
#my $strategy_global='complete';
#my $strategy_global='centroid';
#my $strategy_global='single-to-centroid';
my $strategy_global='single';

#my $dir='/homes/christof/scripts/weps';
#unshift (@INC, "$dir");
#require 'cluster_distance.pl';
#require 'centroid.pl';
#require 'cosine.pl';
#require 'word_normalization.pl';


#my $idf_counts_file='/import/lvt/christof/corpora/weps/ldc_afp.word_counts.txt';
#my $idf_counts_file='/import/lvt/christof/corpora/weps/background-corpus/crawls/html-sample-w3m.counts';
my $idf_counts_file='/import/lvt/christof/corpora/weps/background-corpus/wikipedia_df.txt';

my $lib='/home/christof/scripts/lib';
unshift (@INC, "$lib");
require 'stop-words.pl';
require 'wordnet_pm.pl' if($add_hypernyms);

my %stop_words;

&load_stopwords_strict('/home/christof/scripts/dowser/lib/stop_short.wrd', \%stop_words);

require 'porter_stemmer.pl';

my %idf_ref;
my $total_docs_idf_ref;
if(0) {
open(F,"<$idf_counts_file")||die("can't open file $idf_counts_file: $!\n");
my $line=<F>;
chomp($line);
($_,$total_docs_idf_ref)=split(/[\t\s]+/,$line);
while(defined($line=<F>)) {
    chomp($line);
    my($word,$count)=split(/[\t\s]+/,$line);
    $word=&normalize_word($word);
    if($word!~/^[\s\t]*$/) {
	$word=&porter_stem($word);
	$idf_ref{$word}++;
    }
}
close(F);
}



my %name_parts;
my %total_files_per_name;
my %term_name_occ;
my %term_occ;
my %name_file_term;
my %exp_name_file_term;
my %exp_term_name_occ;
my %stem2unstemmed;

my %hypernym_name_occ;
my %hypernym_occ;

my %doc_weight;
my %idf;
my %idf_name;
my %sim_matrix;
my %cluster;
my %centroids;
my %centroid_weights;

my %inactive_cluster;

my %tried_hypernym;
my %hypernym_cache;

for(my $d=0; $d<@in_dirs; $d++) {
    my $dir=$in_dirs[$d];
    opendir(D,"$dir");
    while(defined(my $name_dir=readdir(D))) {
	opendir(N,"$dir/$name_dir");
	my $name=$name_dir;
	
	my @tokens=split(/\_/,$name);
	for(my $i=0; $i<@tokens; $i++) {
#	$tokens[$i]=~tr/A-Z/a-z/;
	    $name_parts{$name}{$tokens[$i]}=1;
	}
	
	
#	next if($name!~/^(Louis|Miranda|Stacey)/);
#	next if($name!~/^Abby/);
	
	while(defined(my $file=readdir(N))) {
        my $file_id;
	    my $type;
	    if($file=~/^([0-9]+)\.(txt)/) {
		$file_id=$1;
		$type=$2;
		$total_name_file_pairs++;
		$total_files_per_name{$name}++;
	    } elsif($file=~/^([0-9]+)\.(links)/) {
		$file_id=$1;
		$type=$2;
	    } else {
		next;
	    }
	    
	    my $name_file_pair="$name:$file_id";
	    
	    print STDERR "Processing $dir/$name_dir/$file\n";
	    open(F,"<$dir/$name_dir/$file");
	    
	    if($type eq 'txt') {
		my @tokens;
		while(defined(my $line=<F>)) {
		    chomp($line);
		    $line=~s/\[[^ ]*\]/ /g;

		    $line=&naive_tokenize($line);

		    my @line_tokens=split(/[\s\t]+/,$line);
		    push(@tokens,@line_tokens);

		}
		
		my %name_occurrences;
		for(my $i=0; $i<@tokens; $i++) {
		    if(defined($name_parts{$name}{$tokens[$i]})) {
			$name_occurrences{$i}=1;
		    }
		}

		for(my $i=0; $i<@tokens; $i++) {
			
#			next if(defined($name_parts{$name}{$tokens[$i]}));

		    next if(defined($name_occurrences{$i}));

		    my $closest_name_dist=100000;
		    foreach my $pos (keys %name_occurrences) {
			if(abs($pos-$i)<=$closest_name_dist) {
			    $closest_name_dist=abs($pos-$i);
			}
		    }

#		    next if($closest_name_dist!=100000 && $closest_name_dist>$max_name_dist);

#			print STDERR "normalize_word($tokens[$i])\n";;
		    $tokens[$i]=&normalize_word($tokens[$i]);
		    next if($tokens[$i]=~/^[\s\t]*$/);
#			print STDERR "normalized=$tokens[$i]\n";
		    if($add_hypernyms) {
#			    print STDERR "find_hypernyms($tokens[$i],1)\n";
			
			if(!exists($tried_hypernym{$tokens[$i]})) {
			    my $hypernyms=&find_hypernyms($tokens[$i],$hypernym_level);
			    $tried_hypernym{$tokens[$i]}=1;
			    foreach my $hyper_word (keys %$hypernyms) {
				foreach my $hyper_word_part (split(/ /,$hyper_word)) {
				    $hyper_word_part=&normalize_word($hyper_word_part);
#				print STDERR "$tokens[$i] => $hyper_word\n";
				    $hyper_word_part=&porter_stem($hyper_word_part) if($stemming);
				    $exp_name_file_term{$name}{$file_id}{$hyper_word_part}=1;
				    $exp_term_name_occ{$name}{$hyper_word_part}{$file_id}=1;
				    $term_occ{$hyper_word_part}{$name_file_pair}=1;
				    $hypernym_cache{$tokens[$i]}{$hyper_word_part}=1;
				    
				    $hypernym_name_occ{$name}{$hyper_word_part}{$file_id}=1;
				    $hypernym_occ{$hyper_word_part}{$name_file_pair}=1;
				    
				}
			    }
			} elsif(defined($hypernym_cache{$tokens[$i]})) {
			    foreach my $hyper_word_part (keys %{ $hypernym_cache{$tokens[$i]} }) {
				$exp_name_file_term{$name}{$file_id}{$hyper_word_part}=1;
				$exp_term_name_occ{$name}{$hyper_word_part}{$file_id}=1;
				$term_occ{$hyper_word_part}{$name_file_pair}=1;
				
				$hypernym_name_occ{$name}{$hyper_word_part}{$file_id}=1;
				$hypernym_occ{$hyper_word_part}{$name_file_pair}=1;
			    }				
			}
		    }
		    
		    if($stemming) {
			my $unstemmed=$tokens[$i];
			$tokens[$i]=&porter_stem($tokens[$i]);
#			    $tokens[$i]=~s/^(.......).+$/$1/;
			$stem2unstemmed{$tokens[$i]}{$unstemmed}=1;
		    }
		    
		    next if($tokens[$i]=~/^[\s\t]*$/);
		    $name_file_term{$name}{$file_id}{$tokens[$i]}++;
		    $term_name_occ{$name}{$tokens[$i]}{$file_id}++;
		    $term_occ{$tokens[$i]}{$name_file_pair}=1;
		}
		if(!exists($name_file_term{$name}{$file_id})) {
#		    print STDERR "name_file_term{$name}{$file_id}{NIL}=1\n";
		    $name_file_term{$name}{$file_id}{'NIL'}=1;
		    $term_name_occ{$name}{'NIL'}{$file_id}=1;
		    $term_occ{'NIL'}{$name_file_pair}=1;
		}
	    }
	
	    
	    if(0 && $type eq 'links') {
		while(defined(my $line=<F>)) {
		    if($line=~/^([^\t]+)\t/) {
			my $anchor=$1;
			my @tokens=split(/ +/,$anchor);
			
			for(my $i=0; $i<@tokens; $i++) {
			    $tokens[$i]=normalize_word($tokens[$i]);
			    next if($tokens[$i]=~/^[\s\t]*$/);
			    $name_file_term{$name}{$file_id}{$tokens[$i]}++;
			    $term_name_occ{$name}{$tokens[$i]}{$file_id}++;
			    $term_occ{$tokens[$i]}{$name_file_pair}=1;
			}
		    }
		}
	    }
	}
	closedir(N);
    }
    closedir(D);	
}

if(0 && $add_hypernyms) {
    foreach my $name (keys %exp_name_file_term) {
	foreach my $file (keys %{ $exp_name_file_term{$name} }) {
	    foreach my $hyper_word (keys %{ $exp_name_file_term{$name}{$file} }) {
		if(!defined($name_file_term{$name}{$file}{$hyper_word})) {
#		    print STDERR "name_file_term{$name}{$file}{$hyper_word}=1\n";
		    $name_file_term{$name}{$file}{$hyper_word}=1;
		}
	    }
	}
    }

    foreach my $name (keys %exp_term_name_occ) {
	foreach my $hyper_word (keys %{ $exp_term_name_occ{$name} }) {
	    foreach my $file (keys %{ $exp_term_name_occ{$name}{$hyper_word} }) {
		if(!defined($term_name_occ{$name}{$hyper_word}{$file})) {
		    $term_name_occ{$name}{$hyper_word}{$file}=1;
		}
	    }
	}
    }    
}



if(1) {
    foreach my $name (sort (keys %name_file_term)) {
	foreach my $file (keys %{ $name_file_term{$name} }) {
	    my $avg_term_freq=0;
	    my $no_terms=0;
	    foreach my $term (keys %{ $name_file_term{$name}{$file} }) {
		$avg_term_freq+=$name_file_term{$name}{$file}{$term};
		$no_terms++;
	    }
	    if($no_terms>0) {
		$avg_term_freq/=$no_terms;
	    } else {
		$avg_term_freq=0;
	    }
	    if(1) {
		foreach my $term (keys %{ $name_file_term{$name}{$file} }) {
		    $name_file_term{$name}{$file}{$term}=(1+log($name_file_term{$name}{$file}{$term}))/(1+log($avg_term_freq*$name_file_term{$name}{$file}{$term}));
		}
	    }
	}
    }
}

#compute idf scores for each term independent of the names
foreach my $word (keys %term_occ) {
#    $idf_ref{$word}=0 if(!exists($idf_ref{$word}));
#    $idf{$word}=log(($total_name_file_pairs+$total_docs_idf_ref)/((keys %{ $term_occ{$word} })+$idf_ref{$word}));

    if(defined($idf_ref{$word}) && $idf_ref{$word}>0) {
    $idf{$word}=log(($total_name_file_pairs+$total_docs_idf_ref)/((keys %{ $term_occ{$word} })+$idf_ref{$word}));
} else {
    $idf{$word}=log($total_name_file_pairs/(keys %{ $term_occ{$word} }));
}

}

if($add_hypernyms) {
    foreach my $word (keys %hypernym_occ) {
	if(!exists($idf{$word})) {
	    $idf{$word}=log($total_name_file_pairs/(keys %{ $hypernym_occ{$word} }));
	}
    }
}


#compute idf scores for each term with respect to each name
foreach my $name (keys %term_name_occ) {
    foreach my $word (keys %{ $term_name_occ{$name} }) {
	$idf_name{$name}{$word}=log($total_files_per_name{$name}/(keys %{ $term_name_occ{$name}{$word} }));
    }
}

#compute the doc weight for each file (later used for length normalization)
&compute_doc_weights($idf_param);


my %sim_name;
#compute all similarities (cosine) between all individual documents (wrt a name)
print STDERR "Building similarity matrix ... \n";
foreach my $name (sort (keys %name_file_term)) {
#    next if($name!~/^Al/);
    print STDERR "$name\n";
    my @sorted_ids=(sort (keys %{ $name_file_term{$name} }));
    for(my $i=0; $i<@sorted_ids-1; $i++) {
	my $file1=$sorted_ids[$i];
	for(my $j=$i+1; $j<@sorted_ids; $j++) {
	    my $file2=$sorted_ids[$j];
	    my $sim=sim_cosine($idf_param,$name,$file1,$file2);

	    $sim_matrix{$name}{$file1}{$file2}=1-$sim;
	    $sim_matrix{$name}{$file2}{$file1}=1-$sim;

#	    $sim_name{$name}{"$file1 $file2"}=$sim;
#	    print STDERR "sim($name,$file1,$file2)=$sim\n";
	}
    }
}
print STDERR "done.\n";

foreach my $name (sort (keys %name_file_term)) {
#    next if($name!~/^Al/);
    print STDERR "clustering $name\n";
    my $strategy=$strategy_global;
#    my $strategy='single';
    my $min_sim=$min_sim_global;

    print STDERR "strategy=$strategy\n";

#    my %cluster;
    undef %cluster;
    undef %centroids;
    undef %centroid_weights;
    my %point_centroid_matrix;
    undef %point_centroid_matrix;

    #initialize all clusters (as one doc per cluster)
    my $id=-1; 
    foreach my $file (keys %{ $name_file_term{$name} }) {
	$id++;
	$cluster{$id}{$file}=1;
	&compute_centroid($id,$name,$idf_param,\%centroids,\%centroid_weights);
	$point_centroid_matrix{$id}{$file}=0;

#	print STDERR "cluster{$id}{$file}=$cluster{$id}{$file}\n";
    }

    my $change=1;
    my $no_iterations=0;
    my $min_iterations=20;
    while($change) {	
	my @cluster_ids=(keys %cluster);

	if(@cluster_ids==1) {
	    print STDERR "iteration=$no_iterations\n";
	    last;
	}

	if(0) {
	    print STDERR "$name |||";
	    foreach my $c (keys %cluster) {
		my $cluster_content=join(",",(sort (keys %{ $cluster{$c} })));
		print  STDERR " $c=\{$cluster_content\}";
	    }
	    print STDERR "\n";
	}

	my $min_dist=10**10;
	my $dist_threshold=0.0;
	my $min_i;
	my $min_j;
	$change=0;
	for(my $i=0; $i<@cluster_ids-1; $i++) {
	    for(my $j=$i+1; $j<@cluster_ids; $j++) {
		my $d=cluster_dist($strategy,$cluster_ids[$i],$cluster_ids[$j],$name,\%point_centroid_matrix);       	
#		print STDERR "d=$d i=$cluster_ids[$i] j=$cluster_ids[$j]\n";
		if($d<=$min_dist) {
		    $min_dist=$d;
		    $min_i=$cluster_ids[$i];
		    $min_j=$cluster_ids[$j];
		}

	    }
	}

#	if($min_dist<1-0.1 || $no_iterations<$min_iterations) {
#	print STDERR "iteration=$no_iterations\n";
	if($min_dist<=1-$min_sim) {
	    $change=1;
	}

	$id++;
	my $cluster_i_string;
	my $cluster_j_string;
	foreach my $file (keys %{ $cluster{$min_i} }) {
	    $cluster{$id}{$file}=$min_i;
#	    $inactive_cluster{$min_i}{$file}=$cluster{$min_i}{$file};
	    $cluster_i_string=join(" ",(keys %{ $cluster{$min_i} }));
	}

	foreach my $file (keys %{ $cluster{$min_j} }) {
	    $cluster{$id}{$file}=$min_j;
#	    $inactive_cluster{$min_j}{$file}=$cluster{$min_j}{$file};
	    $cluster_j_string=join(" ",(keys %{ $cluster{$min_j} }));
	}

#	print STDERR "compute_centroid($id,$name,...)\n";

	&compute_centroid($id,$name,$idf_param,\%centroids,\%centroid_weights);
	&fill_point_centroid_matrix($id,$name,$idf_param,\%point_centroid_matrix);

#	print STDERR "$name id=$id d=$min_dist ||| id=$min_i $cluster_i_string ||| id=$min_j $cluster_j_string\n";
	delete($cluster{$min_i});
	delete($cluster{$min_j});
	delete($centroids{$min_i});
	delete($centroids{$min_j});
	delete($point_centroid_matrix{$min_i});
	delete($point_centroid_matrix{$min_j});

	$no_iterations++;

	if(0 && $strategy eq 'complete' && ($change==0 || $no_iterations>25)) {
	    $strategy='single';
	    print STDERR "switched to \'$strategy\' after iteration $no_iterations.\n";
	    $change=1;
	    $min_sim=0.1;
	    next;
	}

	if($no_iterations>200 || $change==0) {
	    print STDERR "iteration=$no_iterations\n";
	    last;
	}

    }

    my $filename=$name . '.xml';
    $filename=~s/ +/\_/g;

    open(RES,">$out_dir/$filename");
    print RES "<clustering>\n";
    my $entity_id=0;
    foreach my $c (keys %cluster) {
	print RES "\t<entity id=\"$entity_id\">\n";
	foreach my $cluster_doc (sort (keys %{ $cluster{$c} })) {
	    $cluster_doc=~s/^[0]+([0-9]+)$/$1/;
	    print RES "\t\t<doc rank=\"$cluster_doc\" \/>\n";
	}
	print RES "\t</entity>\n";
	$entity_id++;
    }
    print RES "<\/clustering>\n";
    close(RES);

}



my %sim_contribution;
foreach my $name (sort (keys %sim_name)) {
    print "<name id=\"$name\">\n";
    foreach my $pair (sort {-1*($sim_name{$name}{$a}<=>$sim_name{$name}{$b})} (keys %{ $sim_name{$name} })) {
	print "$name\t$pair\t$sim_name{$name}{$pair}\n";
	my $c=1;
	foreach my $term (sort {-1*($sim_contribution{$name}{$pair}{$a}<=>$sim_contribution{$name}{$pair}{$b})} (keys %{ $sim_contribution{$name}{$pair} })) {
	    print "   $term=$sim_contribution{$name}{$pair}{$term}";
	    $c++;
	    last if($c>$no_top_terms);
	}
	print "\n";
    }
    print "<\/name>\n";
}



sub sim_cosine {
    my ($idf_param,$name,$file1,$file2)=@_;
    my $pair="$file1 $file2";

    my $sum=0;
    if($idf_param eq 'local') {
	foreach my $term (keys %{ $name_file_term{$name}{$file1} }) {
	    if(defined($name_file_term{$name}{$file2}{$term})) {
		$sum+=$name_file_term{$name}{$file1}{$term}*$idf_name{$name}{$term}**2*$name_file_term{$name}{$file2}{$term};
		$sim_contribution{$name}{$pair}{$term}=$name_file_term{$name}{$file1}{$term}*$idf_name{$name}{$term}**2*$name_file_term{$name}{$file2}{$term};	       
	    }
	}
    } elsif ($idf_param eq 'global') {
	foreach my $term (keys %{ $name_file_term{$name}{$file1} }) {
	    if(defined($name_file_term{$name}{$file2}{$term})) {
		$sum+=$name_file_term{$name}{$file1}{$term}*$idf{$term}**2*$name_file_term{$name}{$file2}{$term};
		$sim_contribution{$name}{$pair}{$term}=$name_file_term{$name}{$file1}{$term}*$idf{$term}**2*$name_file_term{$name}{$file2}{$term};	       
	    }
	}
    }
    $sum/=($doc_weight{$name}{$file1}*$doc_weight{$name}{$file2});

    return $sum;
}


sub compute_doc_weights {
    my($idf_param)=@_;

    if($idf_param eq 'local') {
	foreach my $name (keys %name_file_term) {
	    foreach my $file (keys %{ $name_file_term{$name} }) {
		$doc_weight{$name}{$file}=0;
		foreach my $term (keys %{ $name_file_term{$name}{$file} }) {
		    $doc_weight{$name}{$file}+=($name_file_term{$name}{$file}{$term}*$idf_name{$name}{$term})**2;
		}
		$doc_weight{$name}{$file}=sqrt($doc_weight{$name}{$file});
	    }
	}
    } elsif($idf_param eq 'global') {
	foreach my $name (keys %name_file_term) {
	    foreach my $file (keys %{ $name_file_term{$name} }) {
		$doc_weight{$name}{$file}=0;
		foreach my $term (keys %{ $name_file_term{$name}{$file} }) {
		    $doc_weight{$name}{$file}+=($name_file_term{$name}{$file}{$term}*$idf{$term})**2;
		}

#		print STDERR "doc_weight{$name}{$file}=", sqrt($doc_weight{$name}{$file}), "\n";
		$doc_weight{$name}{$file}=sqrt($doc_weight{$name}{$file});
	    }
	}
    }
}

sub cluster_dist {
    my($strategy,$i,$j,$name,$point_centroid_matrix)=@_;

    if($strategy eq 'avg') {
	my $avg_dist=0;
	my $no_comparisons=0;
	foreach my $w1 (keys %{ $cluster{$i} }) {
	    foreach my $w2 (keys %{ $cluster{$j} }) {
#	    print STDERR "dist{$w1}{$w2}=$dist{$w1}{$w2}\n";
		if(defined($sim_matrix{$name}{$w1}{$w2})) {
		    $avg_dist+=$sim_matrix{$name}{$w1}{$w2};
		} else {
		    $avg_dist+=10000;
		}
		$no_comparisons++;
	    }
	}
	$avg_dist/=$no_comparisons;
	return $avg_dist;
    } elsif($strategy eq 'complete') {
	my $max_dist=-1;
	foreach my $w1 (keys %{ $cluster{$i} }) {
	    foreach my $w2 (keys %{ $cluster{$j} }) {

#		print STDERR "sim_matrix{$name}{$w1}{$w2}=$sim_matrix{$name}{$w1}{$w2}\n";
		if(defined($sim_matrix{$name}{$w1}{$w2}) && $sim_matrix{$name}{$w1}{$w2}>$max_dist) {
		    $max_dist=$sim_matrix{$name}{$w1}{$w2};
#		} else {
#		    print STDERR "not defined: sim_matrix{$name}{$w1}{$w2}\n";
#		    $max_dist=10000;
		}
	    }
	}
	$max_dist=10000 if($max_dist==-1);
#	print STDERR "max_dist=$max_dist\n";
	return $max_dist;
    } elsif($strategy eq 'single') {
	my $min_dist=1;
	foreach my $w1 (keys %{ $cluster{$i} }) {
	    foreach my $w2 (keys %{ $cluster{$j} }) {
#		print STDERR "sim_matrix{$name}{$w1}{$w2}=$sim_matrix{$name}{$w1}{$w2}\n";
		if(defined($sim_matrix{$name}{$w1}{$w2}) && $sim_matrix{$name}{$w1}{$w2}<=$min_dist) {
		    $min_dist=$sim_matrix{$name}{$w1}{$w2};
		}
	    }
	}
	return $min_dist;
    } elsif($strategy eq 'single-to-centroid-inner') {
	my $min_dist=1;
	foreach my $w1 (keys %{ $cluster{$i} }) {	    
	    foreach my $w2 (keys %{ $cluster{$j} }) {
		if(defined(my $d=$sim_matrix{$name}{$w1}{$w2})) {
		    $d*=$point_point_weight;
		    $d+=$point_centroid_weight*$$point_centroid_matrix{$i}{$w1}
		        +$point_centroid_weight*$$point_centroid_matrix{$j}{$w2};
		    $min_dist=$d if($d<=$min_dist);
		}
	    }
	}
	return $min_dist;
    } elsif($strategy eq 'single-to-centroid') {
	my $min_dist=1;
	my $min_w1;
	my $min_w2;
	foreach my $w1 (keys %{ $cluster{$i} }) {	    
	    foreach my $w2 (keys %{ $cluster{$j} }) {
		if(defined($sim_matrix{$name}{$w1}{$w2}) && $sim_matrix{$name}{$w1}{$w2}<=$min_dist) {
		    $min_dist=$sim_matrix{$name}{$w1}{$w2};
		    $min_w1=$w1;
		    $min_w2=$w2;
		}
	    }
	}
	$min_dist*=$point_point_weight;
	$min_dist+=$point_centroid_weight*$$point_centroid_matrix{$i}{$min_w1}
	           +$point_centroid_weight*$$point_centroid_matrix{$j}{$min_w2};
	return $min_dist;
     } elsif($strategy eq 'centroid' || $strategy eq 'centroid-hypernym') {

	my $min_dist=1;
	my $d=(1-&sim_cosine_centroid($idf_param,$name,$i,$j));
	if($d<=$min_dist) {
	    $min_dist=$d;
	}

#	print STDERR "centroid-dist($i,$j)=$min_dist\n";
	return $min_dist;
    } elsif($strategy eq 'avg_nn') {
	my %dist_neighborhood;
	my $neighbor_id=0;
	foreach my $w1 (keys %{ $cluster{$i} }) {
	    foreach my $w2 (keys %{ $cluster{$j} }) {
		if(defined($sim_matrix{$name}{$w1}{$w2})) {
		    my $d=$sim_matrix{$name}{$w1}{$w2};
		    $dist_neighborhood{$neighbor_id}[0]=$d;
		    $dist_neighborhood{$neighbor_id}[1]=$w1;
		    $dist_neighborhood{$neighbor_id}[2]=$w2;
		    $neighbor_id++;
		}
	    }
	}

	my $no_neighbors=0;
	my %neighbors;
	foreach my $neighbor_id (sort {$dist_neighborhood{$a}[0]<=>$dist_neighborhood{$b}[0]} (keys %dist_neighborhood)) {
	    my $w1=$dist_neighborhood{$neighbor_id}[1];
	    my $w2=$dist_neighborhood{$neighbor_id}[2];
	    $neighbors{1}{$w1}=1 if(!exists($neighbors{1}) || (keys %{ $neighbors{1} })<$max_neighbors);
	    $neighbors{2}{$w2}=1 if(!exists($neighbors{2}) || (keys %{ $neighbors{2} })<$max_neighbors);
	}

	my $avg_neighbor_dist=0;
	my $no_neighbor_links=0;
	foreach my $w1 (keys %{ $neighbors{1} }) {
	    foreach my $w2 (keys %{ $neighbors{2} }) {
		if(defined($sim_matrix{$name}{$w1}{$w2})) {
#		    print STDERR "neighbor_dist: sim_matrix{$name}{$w1}{$w2}=$sim_matrix{$name}{$w1}{$w2}\n";
		    $avg_neighbor_dist+=$sim_matrix{$name}{$w1}{$w2};
		    $no_neighbor_links++;
		}
	    }
	}
	if($no_neighbor_links>0) {
	    $avg_neighbor_dist/=$no_neighbor_links;
	} else {
	    $avg_neighbor_dist=1;
	}

	return $avg_neighbor_dist;
    }
}


sub compute_centroid {
    my($i,$name,$idf_param,$centroid_pointer,$centroid_weight_pointer,$point_centroid_matrix)=@_;

    my $no_docs=0;
    foreach my $file (keys %{ $cluster{$i} }) {
	foreach my $term (keys %{ $name_file_term{$name}{$file} }) {
#	    print STDERR "cluster{$i}{$file}{$term}=$name_file_term{$name}{$file}{$term}\n";
	    $$centroid_pointer{$i}{$term}+=$name_file_term{$name}{$file}{$term};
	}
    }
    foreach my $term (keys %{ $$centroid_pointer{$i} }) {
	$$centroid_pointer{$i}{$term}/=(keys %{ $cluster{$i} });
    }

    #limit centroid to top $max_centroid_terms terms
    my $term_counter=0;
    foreach my $term (sort { -1*($$centroid_pointer{$i}{$a}*$idf{$a}<=>$$centroid_pointer{$i}{$b}*$idf{$b} )} (keys %{ $$centroid_pointer{$i} })) {
	if($term_counter>$max_centroid_terms) {
	    delete($$centroid_pointer{$i}{$term});
	}
	$term_counter++;
    }

    # add hypernyms to centroid
    if($strategy_global eq 'centroid-hypernym') {
	my $term_counter=0;
	foreach my $term (sort { -1*($$centroid_pointer{$i}{$a}*$idf{$a}<=>$$centroid_pointer{$i}{$b}*$idf{$b} )} (keys %{ $$centroid_pointer{$i} })) {
	    last if($term_counter>=100);
	    $term_counter++;
	    foreach my $unstemmed (keys %{ $stem2unstemmed{$term} }) {
		foreach my $hypernym (keys %{ $hypernym_cache{$unstemmed} }) {
#		    print STDERR "add hypernyms: $term => $hypernym\n";
		    $$centroid_pointer{$i}{$hypernym}=1;
		}
	    }
	}
    }
	
	
    #compute the document weight (for length normalization) for centroid:
    $$centroid_weight_pointer{$i}=0;
    if($idf_param eq 'local') {
	foreach my $term (keys %{ $$centroid_pointer{$i} }) {
	    $$centroid_weight_pointer{$i}+=($$centroid_pointer{$i}{$term}*$idf_name{$name}{$term})**2;
	}
    } elsif ($idf_param eq 'global') {
	foreach my $term (keys %{ $$centroid_pointer{$i} }) {
	    $$centroid_weight_pointer{$i}+=($$centroid_pointer{$i}{$term}*$idf{$term})**2;
	}
    }
    $$centroid_weight_pointer{$i}=sqrt($$centroid_weight_pointer{$i});    
}

sub fill_point_centroid_matrix {
    my($centroid_id,$name,$idf_param,$point_centroid_matrix)=@_;
    foreach my $file (keys %{ $cluster{$centroid_id} }) {       
	$$point_centroid_matrix{$centroid_id}{$file}=(1-&sim_cosine_point_centroid($idf_param,$name,$file,$centroid_id));
    }
}



sub sim_cosine_centroid {
    my($idf_param,$name,$centroid_i,$centroid_j)=@_;
    my $sum=0;


    if($idf_param eq 'local') {
    } elsif ($idf_param eq 'global') {
	foreach my $term (keys %{ $centroids{$centroid_i} }) {
	    if(defined($centroids{$centroid_j}{$term})) {
		$sum+=$centroids{$centroid_i}{$term}*$centroids{$centroid_j}{$term}*$idf{$term}**2;
#		print STDERR "sum+=centroids{$centroid_i}{$term}=$centroids{$centroid_i}{$term} + centroids{$centroid_j}{$term}=$centroids{$centroid_j}{$term}\n";
	    }
	}
    }
    $sum/=($centroid_weights{$centroid_i}*$centroid_weights{$centroid_j});
    return $sum;
}

sub sim_cosine_point_centroid {
    my($idf_param,$name,$file,$centroid_id)=@_;
    my $sum=0;
    if($idf_param eq 'local') {
    } elsif ($idf_param eq 'global') {
	foreach my $term (keys %{ $name_file_term{$name}{$file} }) {
	    if(defined($centroids{$centroid_id}{$term})) {
		$sum+=$name_file_term{$name}{$file}{$term}*$idf{$term}**2*$centroids{$centroid_id}{$term};
	    }
	}
    }
    $sum/=($doc_weight{$name}{$file}*$centroid_weights{$centroid_id});
    return $sum;
}


sub naive_tokenize {
    my($line)=@_;
    $line=~s/([\,\(\)\!\:\"\'\[\]\{\}\+\?\>\<\_\#\|\=])/ $1 /g;
    $line=~s/\.\.\./ ... /g;
    $line=~s/([^ ])\. /$1 . /g;
    $line=~s/([^ ])\.$/$1 ./g;
    $line=~s/ +/ /g;
    $line=~s/^[\s\t]*(.*?)[\s\t]$/$1/;
    return $line;
}

sub normalize_word {
    my($word);

    ($word)=@_;

    $word=~tr/A-Z/a-z/;
    $word=~s/[\.\,\:\;\'|'\+\-\!\?\(\)\[\]\/\\\|\{\}\_\*\&\@\!\`\"\<\>\s\t\#]//g;
    $word=~s/[0-9]//g;

    if(!defined($stop_words{$word})) {
	return $word;
    } else {
	return "";
    }

#    if(!defined($stop_words{$word})) {
#	if($stemming) {
#	    $word=porter_stem($word);
#	}
#	return $word;
#    } else {
#	return "";
#    }
}

