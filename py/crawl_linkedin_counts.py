#!/usr/bin/python
"Usage: script in_file out_file [lang]"

from BeautifulSoup import BeautifulSoup
import urllib # for quote function
import urllib2 # for urlopen
import urlparse # to split the url, and urlencode only path part.
import sys
import re
import time

def _get_page(url):
    return urllib2.urlopen(url).read()

def query_url(line, lang):
  toks = re.split("\s", line)
  first_name=toks[0]
  last_name = "+".join(toks[1:])
  if lang == "nl":
    return "http://nl.linkedin.com/pub/dir/" \
          + first_name + "/" \
          + last_name
  else:
    return "http://www.linkedin.com/pub/dir/" \
        + first_name + "/" \
        + last_name

def _parse_search_results(page):
    soup = BeautifulSoup(page)
    ts = soup.findAll(lambda t: t.name=="li" and t.parent.name=="ul"  and t.parent.has_key("class") and t.parent["class"]=="result-summary same-name-dir")
    #print ts
    if not ts:
      return 1
    else:
      elem = ts[0].contents[0].strip()
      toks = re.split("\s", elem)
      res = toks[2]
      res = re.sub(",", "", res)
      #print toks
      return res

def process_line(line, lang):
  line = line.strip()
  q_url = query_url(line, lang)
  print q_url
  res = 0
  try:
    page = _get_page(q_url)
    res = _parse_search_results(page)
  except Exception, e:
    pass
  print res
  #line = q_url
  line = line + ";"
  line = line + str(res)
  line = line + "\n"
  return line

def _main():
  in_file = sys.argv[1]
  out_file = sys.argv[2]
  lang = "en"
  if len(sys.argv) > 3 and sys.argv[3] == "nl":
    lang = "nl"
  
  fin = open(in_file, "r")
  lines = fin.readlines()
  fin.close()
  result_lines = []
  result_lines.append(lines[0])
  for i in range(1,len(lines)):
    time.sleep(5)
    l = process_line(lines[i], lang)
    result_lines.append(l)
  fout = open(out_file, "w")
  for l in result_lines:
    fout.write(l)
  fout.close()
if __name__ == "__main__":
    _main()
