import MySQLdb
import MySQLdb.cursors
import hashlib

def mysql_password(str):
    """
    Hash string twice with SHA1 and return uppercase hex digest,
    prepended with an asterix.

    This function is identical to the MySQL PASSWORD() function.
    """
    pass1 = hashlib.sha1(str).digest()
    pass2 = hashlib.sha1(pass1).hexdigest()
    return "*" + pass2.upper()

def connect():
	return MySQLdb.connect (host = "qassir.science.uva.nl",
                           user = "bogomil",
                           passwd = "wieowie",
                           db = "people",
                           cursorclass = MySQLdb.cursors.DictCursor)

def select(sql, params = None, conn = None):
    use_own_connection = conn is None
    if use_own_connection:
        conn = connect()
    cursor = conn.cursor()
    if params is None:
        cursor.execute(sql)
    else:
        cursor.execute(sql, params)
    res = cursor.fetchall()
    cursor.close()
    if use_own_connection:
        conn.close()
    return res

def select_unique(sql, params = None, conn = None):
    use_own_connection = conn is None
    if use_own_connection:
        conn = connect()
    cursor = conn.cursor()
    if params is None:
        cursor.execute(sql)
    else:
        cursor.execute(sql, params)
    res = cursor.fetchall()
    assert len(res) == 1, "number of rows returned does not equal one"
    return res[0]

def update(sql, params = None, conn = None):
    use_own_connection = conn is None
    if use_own_connection:
        conn = connect()
    cursor = conn.cursor()
    if params is None:
        cursor.execute(sql)
    else:
        cursor.execute(sql, params)
    cursor.close()
    if use_own_connection:
        conn.close()

# identical to update, really
def insert(sql, params = None, conn = None):
    update(sql, params, conn)

# assuming you are inserting into a table with an auto_increment id.
def insert_select_last_insert_id(sql, params = None, conn = None):
    use_own_connection = conn is None
    if use_own_connection:
        conn = connect()
    cursor = conn.cursor()
    if params is None:
        cursor.execute(sql)
    else:
        cursor.execute(sql, params)
    cursor.execute("select last_insert_id() as id")
    id = cursor.fetchone()['id']
    cursor.close()
    if use_own_connection:
        conn.close()
    return id
