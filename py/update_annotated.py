#!/usr/bin/python
"""
Updates annotated column in table selected_queries
"""

import db
import sys

sql_yes = "update selected_queries set annotated = 'yes' where idx in (select distinct query_id from cluster)"
sql_no = "update selected_queries set annotated = 'no' where idx not in (select distinct query_id from cluster)"

db.update(sql_yes)
db.update(sql_no)
