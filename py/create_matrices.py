#!/usr/bin/python

# First argument - directory with files
# Second argument - file with stopwords - each word on a separate line
# Third argument - occurrence limit, e.g. how many times at least a word has to occur in the corpus to be considered
# Expects a flat directory where each file has been been put through w3m already. Each file name should be normalized url id.
# For now pdf files are ignored - e.g. represented by a row of zeroes.
# Sample output where the names of the docs are 1, 2 and 3:
#      term4   term3   term2   term1
#1 0.0   0.2   0.9   0.2
#3 0.75   0.25   0.375   0.25
#2 0.0   0.2   0.0   0.8

import re
import os
import sys
import math
import metrics
import hac
import pprint
import getopt
import datetime
import MySQLdb
import store_run
import store_run_weps
import serializer
import vector_utils
import feature_vector_creators


def create_matrices(base_dir, target_dir, feature_vector_method, stopword_file, occurrence_limit, metric, store_experiment="false", experiment_name="", similarity_threshold=0.1, db_storer=store_run, term_window=0, stemmer_used="", logged_tfs=False, which_weps=0):
  if os.listdir(target_dir):
    raise Exception("Target directory not empty: " + target_dir)
  abs_base = os.path.abspath(base_dir)
  abs_targ = os.path.abspath(target_dir)
  queries = os.listdir(abs_base)
  run_id = 0
  serializer.write_run_conf(base_dir,abs_targ,feature_vector_method, occurrence_limit, metric, stopword_file, similarity_threshold, term_window, logged_tfs)
  if store_experiment == "true":
    run_id = db_storer.store_run_in_db(base_dir, feature_vector_method, occurrence_limit, metric, stopword_file, similarity_threshold, term_window, logged_tfs, experiment_name, which_weps)
  for q in queries:
    source_dir_for_feature_vectors= abs_base + "/" + q
    target_dir_for_matrices= abs_targ + "/" + q
    os.mkdir(target_dir_for_matrices)
    print >> sys.stderr, q
    print >> sys.stderr, "Generating feature vectors.."
    #tf_idf_vectors, terms = generate_tf_idf_matrix(source_dir_for_matrix, stopword_file, occurrence_limit)
    toexec="tf_idf_vectors, terms = feature_vector_creators."+str(feature_vector_method)+"(base_dir, source_dir_for_feature_vectors, stopword_file, occurrence_limit, term_window, stemmer_used, logged_tfs)"
    #toexec="tf_idf_vectors, terms = feature_vector_creators.tf_idf_vectors_global(base_dir, source_dir_for_feature_vectors, stopword_file, occurrence_limit)"
    #print toexec
    exec(toexec)
    print >> sys.stderr, "Serializing feature vectors.."
    serializer.serialize_feature_matrix(target_dir_for_matrices + "/matrix", tf_idf_vectors, terms)
    print >> sys.stderr, "Generating similarity matrix.."
    exec("metric_function=metrics." + metric)
    similarity_matrix, labels = vector_utils.get_similarity_matrix(tf_idf_vectors,metric_function)
    print >> sys.stderr, "Serializing similarity matrix.."
    serializer.serialize_similarity_matrix(target_dir_for_matrices + "/similarity_matrix", similarity_matrix, labels)
    #labels = [int(x) for x in labels]
    # print >> sys.stderr, "Clusterging.."
    # clustering, labelled_clustering = hac.cluster_single_link_maximum(similarity_matrix, labels, similarity_threshold)
    # print >> sys.stderr, "Number of clusters: ", len(clustering)
    # print >> sys.stderr, "Serializing clustering.."
    # serializer.serialize_clustering(target_dir_for_matrices + "/clustering", labelled_clustering)
    # if store_experiment == "true":
    #   query_id = db_storer.get_query_id(q)
    #   db_storer.store_clustering_in_db(labelled_clustering, run_id, query_id )


if __name__ == "__main__":
  usage="""Usage:
     create_matrices
        --input-dir=<input-dir>
        --output-dir=<output-dir>
       [--stopword-file=<stopword-file>]
       [--occurrence-limit=<occurance_limit>]
       [--metric=<metric_to_use_for_similarity_matrices>]
       [--logged-tfs]
       [--store-run=true|false]
       [--run-name=<name_of_the_experiment>]
       [--weps-run=1|2]
       [--similarity-threshold=<similarity_threshold>]
       [--feature-vector-creator=<feature_vector_creator>]
       [--term-window=<term_window_around_name>]
       [--stemmer-used=<path_to_stemmer_used>]"""

  db_storer = store_run
  logged_tfs = False
  which_weps="0"
  try:
    optlist, args = getopt.getopt(sys.argv[1:],'', ['input-dir=', 'output-dir=', 'stopword-file=', 'occurrence-limit=', 'metric=', 'logged-tfs','store-run=', 'run-name=', 'similarity-threshold=', 'weps-run=', 'feature-vector-creator=', 'term-window=', 'stemmer-used='])
  except getopt.GetoptError, err:
    print usage
    print err
    sys.exit(2)
  for o, a in  optlist:
    if o == "--input-dir":
      source_dir = os.path.abspath(a)
    elif o == "--output-dir":
      target_dir = a
    elif o == "--stopword-file":
      stopword_file = a
    elif o == "--occurrence-limit":
      occurrence_limit = a
    elif o == "--metric":
      metric = a
    elif o =="--logged-tfs":
      logged_tfs=True
    elif o == "--store-run":
      store_experiment = a
    elif o == "--run-name":
      experiment_name = a
    elif o == "--similarity-threshold":
      similarity_threshold = a
    elif o == "--weps-run":
      db_storer = store_run_weps
      which_weps=a
    elif o == "--feature-vector-creator":
      feature_vector_method = a
    elif o == "--term-window":
      term_window = a
    elif o == "--stemmer-used":
      stemmer_used = a

  if ('source_dir' not in locals() or 'target_dir' not in locals()):
    print usage
    sys.exit(2)
  if ('store_experiment' in locals() and 'experiment_name' not in locals()):
    print "Please specify a name for the experiment."
    print store_experiment
    sys.exit(2)

  stopword_file = "" if 'stopword_file' not in locals() else stopword_file
  occurrence_limit = 5 if 'occurrence_limit' not in locals() else int(occurrence_limit)
  metric = "cosine_hashes" if 'metric' not in locals() else metric
  store_experiment = "true" if 'store_experiment'in locals() and store_experiment=="true" else "false"
  experiment_name = "" if 'experiment_name' not in locals() else experiment_name
  similarity_threshold = 0.1 if 'similarity_threshold' not in locals() else float(similarity_threshold)
  feature_vector_method = "tf_idf_vectors_local" if 'feature_vector_method' not in locals() else feature_vector_method
  term_window = 0 if 'term_window' not in locals() else int(term_window)
  stemmer_used = "" if 'stemmer_used' not in locals() else stemmer_used

  create_matrices(source_dir, target_dir, feature_vector_method, stopword_file, occurrence_limit, metric, store_experiment, experiment_name, similarity_threshold, db_storer, term_window, stemmer_used, logged_tfs, which_weps)
  #tf_idf_vectors, terms = get_matrix(directory, stopword_file, int(occurence_limit))
  #print tf_idf_vectors[tf_idf_vectors.keys()[0]]
  #_print_docs(tf_idf_vectors, terms)
