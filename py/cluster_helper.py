import sys
import db
import os
import serializer

# expand a set with (x,y) tuples to a binary matrix M (x * y) 
def expand_sim_set(sim):
    docs = set([])
    for doc_id1, doc_id2 in sim:
        docs.add(doc_id1)
        docs.add(doc_id2)
    docs_with_idx = {}
    labels = ['']*len(docs)
    for i, doc_id in enumerate(docs):
        docs_with_idx[doc_id] = i
        labels[i] = str(doc_id)
    sim_matrix = [[0]*len(docs) for i in range(len(docs))]
    for i in range(len(docs)):
        sim_matrix[i][i] = 1
    for doc_id1, doc_id2 in sim:
        sim_matrix[docs_with_idx[doc_id1]][docs_with_idx[doc_id2]] = 1
        sim_matrix[docs_with_idx[doc_id2]][docs_with_idx[doc_id1]] = 1
    return labels, sim_matrix

def store_run_and_get_run_id(name, description, out_dir, which_weps=None, conn=None):
    """
    Stores run in database, creates a file run.conf in out_dir, and in it
    the description of this run (parameters settings) are written, as well as
    the value of which_weps (1 = weps-1-test, 2 = weps-2, 3 = weps-1-train)
    """
    if which_weps:
        sql = "insert into run_weps (name, description, which_weps) values (%s, %s, %s)"
        params = (name, description, which_weps)
    else:
        sql = "insert into run (name, description) values (%s, %s)"
        params = (name, description)
    run_id = db.insert_select_last_insert_id(sql, params, conn)
    f = open("%s/run.conf" % out_dir, "w")
    f.write("""
    run_id: %s
    name: %s
    which_weps: %s
    description: %s
    """ % (run_id, name, which_weps, description))
    f.close()
    return run_id

def write_clustering_to_disk(labelled_clustering, out_dir, query_dir):
    os.mkdir("%s/%s" % (out_dir, query_dir))
    serializer.serialize_clustering("%s/%s/clustering" % (out_dir, query_dir), labelled_clustering)
