import os
import sys

# increment dict values
def plus_is(dict, key, add):
    if key in dict:
        dict[key] += add
    else:
        dict[key] = add

def ls_dir_or_exit(dir):
    try:
        ls = set(os.listdir(dir))
    except OSError, e:
        print >> sys.stderr, e
        sys.exit(2)
    else:
        return ls

def read_file_or_empty_string(path):
    try:
        f = open(path)
    except IOError, e:
        return ""
    else:
        return f.read()
