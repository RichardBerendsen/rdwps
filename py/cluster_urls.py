"""cluster_urls.py clusters URLs returned by a people search engine. 
It uses the peoplesearch mysql database, and stores its results in there."""

import MySQLdb
import MySQLdb.cursors
import datetime as dt
import operator

# we compute the clusters on a per query basis for now.
# funny: query instances where an additional keyword was used are taken into
# account and treated as the same query!. (but it will be far less ambiguous)
def get_queries(conn):
	cursor = conn.cursor()
	cursor.execute("""select * from Queries where FirstName != "" and LastName != "" and \
	countUserId < 30 and countUserId > 15 and disambigusers > 7""")
	queries = cursor.fetchall()
	cursor.close()
	return queries

# we sum dwellingtime on each URL over all sessions by a user, because a later
# session could contradict an earlier session.
# we select only users that have clicks on at least two types (disambigusers)
def get_users(conn, first_name, last_name):
	cursor = conn.cursor()
	cursor.execute("""select s.UserID \
from Searches_sessions as s, \
Clickouts_sessions as c \
where s.FirstName = %s and s.LastName = %s and s.SearchID = c.SearchID \
group by s.UserID having count(distinct c.type) >= 2""",
			(first_name, last_name))
	users = cursor.fetchall()
	cursor.close()
	return users

# we iterate over a list of actions (clicks or searches) to compute dwelling times for each URL.
def get_actions(conn, first_name, last_name, user_id):
	cursor = conn.cursor()
	cursor.execute("""(select Time, "search" as type, "" as url, "" as platform \
from Searches_sessions \
where UserID = %s and FirstName = %s and LastName = %s) \
union \
(select c.Time, "click", c.URL, c.type \
from Searches_sessions as s, Clickouts_sessions as c \
where \
s.UserID = %s and s.SearchID = c.SearchID \
and s.FirstName = %s and s.LastName = %s) \
order by Time asc""", (user_id, first_name, last_name, user_id, first_name, last_name));
	actions = cursor.fetchall()
	cursor.close()
	return actions

def next_action_or_none(actions, idx):
	try:
		return actions[idx]
	except:
		pass # python will return None

def next_click_or_none(actions, idx):
	try:
		while actions[idx]['type'] != "click":
			return next_click_or_none(actions, idx + 1)
		return actions[idx]
	except:
		pass # python will return None

def store_dwelling_time(dict, url, platform, time):
	if dict.has_key(platform):
		if dict[platform].has_key(url):
			dict[platform][url] += time
		else:
			dict[platform][url] = time
	else:
		dict[platform] = {url:time}

# and compute the dwelling times for each URL
def get_dwelling_times(actions):
	dwelling_times = {}
	idx = 0
	while True:
		next_click = next_click_or_none(actions, idx)
		if next_click is None:
			break
		next_next_action = next_action_or_none(actions, idx + 1)
		dwelling_time = 0
		if next_next_action is None:
			store_dwelling_time(dwelling_times, next_click['url'],
					next_click['platform'], 60)
		else:
			dwelling_time = (next_next_action['Time'] - next_click['Time']).seconds
			store_dwelling_time(dwelling_times, next_click['url'],
					next_click['platform'], dwelling_time)
		idx = idx + 1
	return dwelling_times

def max_dict_by_value(type):
	sorted_type = sorted(type.iteritems(), key=operator.itemgetter(1))
	return sorted_type[-1]

# compute 'clear winners' for each type
# a clear win is if the ratio dwelling time on one URL over total dwelling time
# on URLs of the same type is bigger than
def get_clear_winners(dwelling_times, ratio_threshold):
	winners_with_ratio = {}
	for platform_key, platform_dict in dwelling_times.iteritems():
		max_url, max_dwelling_time = max_dict_by_value(platform_dict)
		ratio = (max_dwelling_time * 1.0) / sum(platform_dict.values())
		if ratio > ratio_threshold:
			winners_with_ratio[platform_key] = (max_url, ratio)
	return winners_with_ratio

	# return clear_winners
	pass

def print_clear_winners(clear_winners):
	for url, ratio in clear_winners.values():
		print url
	print

# store clear winners
def store_clear_winners(conn, clear_winners, first_name, last_name):
	pass

def _main():
	# connect to the db.
	conn = MySQLdb.connect (host = "qassir",
						   user = "bogomil",
						   passwd = "wieowie",
						   db = "peoplesearch",
						   cursorclass = MySQLdb.cursors.DictCursor)

	queries = get_queries(conn)

	for query in queries:
		users = get_users(conn, query['FirstName'], query['LastName'])

		for user in users:
			actions = get_actions(conn, query['FirstName'], query['LastName'], user['UserID'])

			dwelling_times = get_dwelling_times(actions)

			winners_with_ratio = get_clear_winners(dwelling_times, 0.7)

			print_clear_winners(winners_with_ratio)

			#store_clear_winners(conn, clear_winners, query['FirstName'], query['LastName'])

	# close connection
	conn.close ()


####### main code
if __name__ == '__main__':
	_main()
