"""
Usage: %s annotator annot_dir

Where annot_dir is a directory with in it query directories, and in each query
directory an annotation, a clustering: This is the directory structure that we
assume for each query directory:

>discard 
>>discard_name_not_found
>>>37792
>>>...
>>discard_too_private
>>>239
>keep
>>girl_with_pearl_earring
>>>2398
>>>823
>>>...
>>paardrijden_hockey_harderwijk
>>>309
>duplicates
>>set_1
>>>2398
>>>823

Why this script: 
Thomas introduced a somewhat different format for storing stuff in the db.
This format also allows for storing duplicate sets of duplicate documents. But
now I do have to write a new script of course :-)
"""

import sys
import os
import db
import re
import util

def _store_doc_in_cluster(conn, cluster_id, doc):
    prog = re.compile(r'^\d+$')
    match = prog.search(doc)
    if match is not None:
        doc_id = match.group(0)
        cursor = conn.cursor()
        sql = "insert into clustering (normalized_url_id, cluster_id) values (%s, %s)"
        print >> sys.stderr, sql % (doc_id, cluster_id)
        cursor.execute(sql, (doc_id, cluster_id))
        cursor.close()

def _store_cluster(conn, annotator, query, cluster_dir, discard = 'no'):
    cursor = conn.cursor()
    sql = "insert into cluster (name, discard, author, query_id) values (%s, %s, %s, %s)"
    print >> sys.stderr, sql % (cluster_dir, discard, annotator, query['idx'])
    cursor.execute(sql, (cluster_dir, discard, annotator, query['idx']))
    cursor.execute("select last_insert_id() as id")
    res = cursor.fetchone()
    cursor.close()
    return res['id']

def _store_clustering(conn, annotator, query, cluster_dir, discard = 'no'):
    try:
        docs = os.listdir(cluster_dir)
    except OSError, e:
        print >> sys.stderr, e
        print >> sys.stderr, "Ignoring exception: we just ignore files that are no directories here, skipping"
    else:
        cluster_name = os.path.basename(cluster_dir)
        cluster_id = _store_cluster(conn, annotator, query, cluster_name, discard)
        for doc in docs:
            _store_doc_in_cluster(conn, cluster_id, doc)

def _store_uniques(conn, annotator, query, cluster_dir):
    try:
        docs = os.listdir(cluster_dir)
    except OSError, e:
        print >> sys.stderr, e
        sys.exit(2)
    else:
        for doc in docs:
            prog = re.compile(r'^\d+$')
            match = prog.search(doc)
            if match is not None:
                doc_id = match.group(0)
                cluster_id = _store_cluster(conn, annotator, query, doc_id)
                _store_doc_in_cluster(conn, cluster_id, doc)

def _store_clusters(conn, annotator, query, cluster_dirs, discard = 'no'):
    for cluster_dir in cluster_dirs:
        if os.path.basename(cluster_dir) == "uniques": # a special cluster directory which holds documents that could not be clustered
            _store_uniques(conn, annotator, query, cluster_dir)
        else:
            _store_clustering(conn, annotator, query, cluster_dir, discard)

def _store_doc_in_duplicate_set(conn, duplicate_id, doc):
    prog = re.compile(r'^\d+$')
    match = prog.search(doc)
    if match is not None:
        doc_id = match.group(0)
        cursor = conn.cursor()
        sql = "insert into duplicate_set_membership (normalized_url_id, duplicate_set_id) values (%s, %s)"
        print >> sys.stderr, sql % (doc_id, duplicate_id)
        cursor.execute(sql, (doc_id, duplicate_id))
        cursor.close()

def _store_duplicate_set(conn, annotator, query, duplicate_dir):
    cursor = conn.cursor()
    sql = "insert into duplicate_set (name, author, query_id) values (%s, %s, %s)"
    print >> sys.stderr, sql % (duplicate_dir, annotator, query['idx'])
    cursor.execute(sql, (duplicate_dir, annotator, query['idx']))
    cursor.execute("select last_insert_id() as id")
    res = cursor.fetchone()
    cursor.close()
    return res['id']

def _store_duplicates_in_set(conn, annotator, query, duplicates_dir):
    try:
        docs = os.listdir(duplicates_dir)
    except OSError, e:
        print >> sys.stderr, e
        print >> sys.stderr, "Ignoring exception: we just ignore files that are no directories here, skipping"
    else:
        duplicate_set_name = os.path.basename(duplicates_dir)
        duplicate_id = _store_duplicate_set(conn, annotator, query, duplicate_set_name)
        for doc in docs:
            _store_doc_in_duplicate_set(conn, duplicate_id, doc)

def _store_duplicates(conn, annotator, query, duplicates_dirs):
    for duplicates_dir in duplicates_dirs:
        _store_duplicates_in_set(conn, annotator, query, duplicates_dir)

def _store_annot_in_db(conn, annotator, query_dir):
    base = os.path.basename(query_dir)
    print >> sys.stderr, base
    query = util.get_query_from_dirname(conn, base)
    if query is None:
        print >> sys.stderr, "Query for dir `%s` not found in the database, skipping" % query_dir
        return

    # store discarded
    try:
        discard_dirs = os.listdir("%s/discard" % query_dir)
    except OSError, e:
        print >> sys.stderr, e
        sys.exit(2)
    else:
        print >> sys.stderr, "Storing discard clusters"
        _store_clusters(conn, annotator, query, [query_dir + "/discard/" + discard_dir for discard_dir in discard_dirs], 'yes')

    # store keep
    try:
        keep_dirs = os.listdir("%s/keep" % query_dir)
    except OSError, e:
        print >> sys.stderr, e
        sys.exit(2)
    else:
        print >> sys.stderr, "Storing keep clusters"
        _store_clusters(conn, annotator, query, [query_dir + "/keep/" + keep_dir for keep_dir in keep_dirs])

    # store duplicates
    try:
        duplicates_dirs = os.listdir("%s/duplicates" % query_dir)
    except OSError, e:
        print >> sys.stderr, 'Error opening duplicates directory, not storing duplicates'
    else:
        print >> sys.stderr, "Storing duplicate sets"
        _store_duplicates(conn, annotator, query, [query_dir + "/duplicates/" + duplicate_dir for duplicate_dir in duplicates_dirs])

def check_annotator(conn, annotator):
    cursor = conn.cursor()
    cursor.execute("show columns from cluster like 'author'")
    res = cursor.fetchone()
    cursor.close()
    type = res['Type']
    prog = re.compile('enum\((.*)\)')
    match = prog.search(type)
    if match is None:
        print >> sys.stderr, "enum column parsed incorrectly"
        sys.exit(3)
    enum_values = match.group(1)
    annotators = enum_values.split(',')
    return "'%s'" % annotator in annotators

def annotator_did_query(conn, annotator, query_dir):
    base = os.path.basename(query_dir)
    query = util.get_query_from_dirname(conn, base)
    sql = "select count(*) as n from cluster where author = %s and query_id = %s"
    params = (annotator, query['idx'])
    nclusters = int(db.select_unique(sql, params, conn)['n'])
    if nclusters:
        return True
    else:
        return False

def _main(args):

    if len(args) != 3:
        print >> sys.stderr, __doc__ % args[0]
        return 2

    annotator = args[1]
    annot_dir = args[2].rstrip('/')

    try:
        query_dirs = os.listdir(annot_dir)
    except OSError, e:
        print >> sys.stderr, e
        return 2
    else:
        conn = db.connect()
        if not check_annotator(conn, annotator):
            print >> sys.stderr, "Annotator %s not known in the db, please insert it first" % annotator
            return 2
        for query_dir in query_dirs:
            print >> sys.stderr, "Processing query dir %s" % query_dir
            if annotator_did_query(conn, annotator, query_dir):
                print >> sys.stderr, "Annotator did already annotate this query, skipping"
                continue
            else:
                _store_annot_in_db(conn, annotator, annot_dir + "/" + query_dir)
        conn.close()

if __name__ == "__main__":
    sys.exit(_main(sys.argv))
