#!/usr/bin/python
"""
Output:
username
profile_picture_url
list of frends separated by #
"""
import sys
import os
import re
from BeautifulSoup import BeautifulSoup
import markup_util

def get_profilename(soup):
  name_div = soup.findAll(lambda x: x.name == "div" and x.has_key("class") and x['class'] == "screen-name" and x.parent.name == "h2" and x.parent.has_key("class") and x.parent['class'] == "thumb clearfix")
  if len(name_div) > 1:
    return name_div[0].__repr__()
  else:
    name_loc_div = soup.findAll(lambda x: x.name == "div" and x.has_key("class") and x['class'] == "screen-name-and-location" and x.parent.parent.name == "div" and x.parent.parent.has_key("class") and x.parent.parent['class'] == "profile-info clearfix")
    #text = markup_util.remove_markup(name_loc_div[0].__repr__(), [])
    text = name_loc_div[0].__repr__()
    pfname = re.findall("@[^\s<]+", text)
    return pfname[0][1:]

def get_picture(soup):
  img_elem = soup.findAll(lambda x: x.name == "img" and x.has_key("src") and x.parent.parent.name == "h2" and x.parent.parent.has_key("class") and x.parent.parent['class'] == "thumb clearfix")
  if len(img_elem) > 0:
    path = img_elem[0]['src']
  else:
    img_elem = soup.findAll(lambda x: x.name == "img" and x.has_key("src") and x.parent.parent.name == "div" and x.parent.parent.has_key("class") and x.parent.parent['class'] == "profile-image-container")
    path = img_elem[0]['src']
    return path

def get_friends(soup):
  return [], []

def get_text(soup):
  bio_span = soup.findAll(lambda x: x.name == "span" and x.has_key("class") and x['class'] == "bio" and x.parent.parent.name == "ul" and x.parent.parent.has_key("class") and x.parent.parent['class'] == "about vcard entry-author")
  if len(bio_span) > 0:
    bio = unicode(bio_span[0].__repr__(), "utf-8")
    elems = re.split(u'\u2022', bio)
    text = " ".join(elems)
  else:
    bio_div = soup.findAll(lambda x: x.name == "div" and x.has_key("class") and x['class'] == "bio" and x.parent.name == "div" and x.parent.has_key("class") and x.parent.parent['class'] == "profile-info clearfix")
    bio = unicode(markup_util.remove_markup(bio_div[0].__repr__(), []), "utf-8")
    return bio
  

  # now try to get location
  adr_span = soup.findAll(lambda x: x.name == "span" and x.has_key("class") and x['class'] == "adr" and x.parent.parent.name == "ul" and x.parent.parent.has_key("class") and x.parent.parent['class'] == "about vcard entry-author")
  if len(adr_span) > 0:
    adr = adr_span[0].__repr__().strip()
    text = text + " " + adr
  return text

