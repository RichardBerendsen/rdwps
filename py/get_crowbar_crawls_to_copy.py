#!/usr/bin/python

import sys
import connect
import in_weps_corpus


if __name__ == "__main__":

    query_id_from = 3
    query_id_to = 45
    if len(sys.argv) != 3 and len(sys.argv) != 1:
        print >> sys.stderr, "Usage: %s [from to]" % sys.argv[0]
        print >> sys.stderr, "where from and to with from <= to is a query id range"
        sys.exit(2)
    if len(sys.argv) == 3:
        query_id_from = sys.argv[1]
        query_id_to = sys.argv[2]

    conn = connect.connect()
    search_results = in_weps_corpus.get_crowbar_docs_per_query(conn, query_id_from, query_id_to)
    for s in search_results:
      fname = s['firstname']
      lname = s['lastname']
      chopped_lname = lname.replace("\"", "")
      chopped_lname = chopped_lname.replace(" ", "_")
      print str(s['query_id']) + "_" + fname + "_" + chopped_lname + ";" + str(s['id'])
    conn.close()
