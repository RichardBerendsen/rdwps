"""write runs stored in the database to xml

Usage: python write_runs.py empty_dir [run1 run2 ...]

Will store one dir for each run in empty_dir. Inside each run dir, will store xml file for each query in WePS-2 format.

run1 run2 .. are names of runs as stored in the database. If you don't specify any, will store all as xml.

optional arguments: 
    --query_id_from X, __query_id_to Y
"""

import sys
import os
import db
from optparse import OptionParser
import get_queries

def _query_xml_filename(firstname, lastname):
    firstname = firstname.replace('"', '').strip()
    lastname = lastname.replace('"', '').strip()
    words = firstname.split() + lastname.split()
    query_string = '_'.join(words)
    filename = query_string + ".clust.xml"
    return filename

def _get_entities(conn, run, query):
    cursor = conn.cursor()
    cursor.execute("select * from run_cluster where run_id = %s and query_id = %s order by id asc", (run['id'], query['idx']))
    res = cursor.fetchall()
    cursor.close()
    return res

def _get_normalized_url_ids(conn, query, cluster):
    """
    return normalized url ids that appear in the run for this cluster.
    """
    sql = """
    select distinct s.id from search_results as s, run_clustering as r 
    where s.query_id = %s and in_weps_corpus = 'yes' and s.id = r.normalized_url_id
    and r.cluster_id = %s;
    """ # no need to check for platform here.
    params = (query['idx'], cluster['id'])
    return db.select(sql, params, conn)

def _get_xml_doc(doc):
    xml = "\t\t<doc rank=\"%s\"/>\n" % doc['id']
    return xml

def _get_xml_entity(conn, query, cluster):
    docs = [_get_xml_doc(doc) for doc in _get_normalized_url_ids(conn, query, cluster)]
    xml = ""
    if docs != []:
        xml += "\t<entity id =\"%s\">\n" % cluster['id']
        xml += "".join(docs)
        xml += "\t</entity>\n"
    return xml

def _get_xml_entities(conn, run, query):
    entities = [_get_xml_entity(conn, query, entity) for entity in _get_entities(conn, run, query)]
    xml = "".join(entities)
    return xml

def _get_xml_uniques(conn, options, run, query):
    """Since we may not store uniques (unclustered documents) in the database, \
    we retrieve them here:
    """
    # here we do check that uniques are drawn only from the correct set: 
    # both, web, or social media.
    platforms = {
            'SERs' : "'google','yahoo','bing'",
            'Profiles' : "'hyves','facebook','linkedin','twitter','myspace'",
            'both' : "'hyves','facebook','linkedin','twitter','myspace','google','yahoo','bing'"
    }
    sql_create = "create temporary table if not exists clustered_docs (id int);"
    sql_insert = "insert into clustered_docs (id) select distinct r2.normalized_url_id from run_cluster as r1 inner join run_clustering as r2 on r1.id = r2.cluster_id where r1.run_id = %s and r1.query_id = %s;"
    params_insert = (run['id'], query['idx'])
    sql_select = """select distinct s.id from search_results as s left outer join clustered_docs as r on s.id = r.id where s.query_id = %s and in_weps_corpus = 'yes' and s.type in (%s) and r.id is null;""" % ("%s", platforms[options.dataset])
    params_select = (query['idx'],)
    sql_drop = "drop table clustered_docs"
    db.insert(sql_create, conn=conn)
    db.insert(sql_insert, params=params_insert, conn=conn)
    res = db.select(sql_select, params_select, conn=conn)
    db.insert(sql_drop, conn=conn)
    xml = ""
    for rec in res:
        xml += "\t<entity id = \"doc_%s\">\n" % (rec['id'],)
        xml += "\t\t<doc rank=\"%s\"/>\n" % rec['id']
        xml += "\t</entity>\n"
    return xml

def _write_clustering(conn, options, run, query, run_dir):
    xml_entities = _get_xml_entities(conn, run, query)
    xml_uniques = _get_xml_uniques(conn, options, run, query)
    if not (xml_entities == "" and xml_uniques == ""):
        xml = "<clustering>\n" + xml_entities + xml_uniques + "</clustering>\n"

        basename = _query_xml_filename(query['FirstName'], query['LastName'])
        f = open("%s/%s" % (run_dir, basename), 'w')
        f.write(xml)
        f.close()
    else:
        msg = "Error: can't find entities or singleton docs for query %s"
        raise RuntimeError(msg)

def _get_runs(conn, runs):
    if len(runs): # user supplied run names, let's select only these from the db.
        str_runs = "(" + ",".join(["'" + run + "'" for run in runs]) + ")" # vulnerable to sql injection, but just take care with your arguments.
        sql = "select * from run where id in %s" % str_runs
    else: # just match all runs.
        sql = "select * from run"
    cursor = conn.cursor()
    cursor.execute(sql)
    res = cursor.fetchall()
    cursor.close()
    return res

def _is_empty_dir(dir):
    try:
        ls = os.listdir(dir)
    except OSError:
        return False
    else:
        return ls == []

def main(argv):
    parser = OptionParser()
    parser.add_option("-f", "--query_id_from", dest="query_id_from", action="store", type="int", default=3)
    parser.add_option("-t", "--query_id_to", dest="query_id_to", action="store", type="int", default=45)
    parser.add_option("-s", "--dataset", dest="dataset", action="store", type="choice", choices=["SERs", "Profiles", "both"], default="both")
    options, args = parser.parse_args()

    if len(args) < 1 or not _is_empty_dir(args[0]):
        print >> sys.stderr, __doc__
        return 2
    dir = args[0].rstrip('/')

    conn = db.connect()
    for run in _get_runs(conn, args[1:]):
        run_dir = "%s/%s_%s" % (dir, run['id'], run['name'])
        try:
            os.mkdir(run_dir)
        except OSError, e:
            print >> sys.stderr, e
            return 3
        else:
            for q in get_queries.get_queries(options.query_id_from, options.query_id_to):
                if q['annotated'] == 'yes':
                    _write_clustering(conn, options, run, q, run_dir)
    conn.close()
    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
