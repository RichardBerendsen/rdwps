#!/bin/bash

# Format script indir outdir truthfiledir
# Where indir is a directory X such that every subdirectory is a name
# and the files in each such subdirectory have ranks as names and documents as content.
#
# The truthfiledir contains a directory with the gold standard files as xmls.
#

indir=$1
outdir=$2
annotation_dir=$3
names=(`ls $indir`)
for n in ${names[@]}
do 
  #./comg-hier-clustering.pl $outdir $indir/$n
  ./weps-cluster.pl $outdir $indir/$n
  mv $outdir/cluster.xml $outdir/$n.xml 
  echo $n
done
#./weps_eval_bcubed.pl $outdir/ truth_files/official_annotation/ > $outdir/results.txt
./weps_eval_bcubed.pl $outdir/ $annotation_dir > $outdir/results.txt

