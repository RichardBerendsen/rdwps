"""
situation: we have a directory structure created by ../bash/run_exp.sh

in there there are a number of experiments:

    runs/
        31_expname/
            query_1
            query_2
            ...
        32_expname/
            query_1
            query_2
            ...
        ...
    results/
        31_expname.eval
        32_expname.eval

    and then in 31_expname.eval we have

                BEP BER .... F_measure
    query_1
    query_2

and our goal is to find:

    for each query,
        for all thresholds,
            the F-measure.

Therefore, we:

    list all run dirs in runs/
    for each run:
        find the corresponding threshold
        find corresponding .eval file in results/
        for each query in .eval file
            store (threshold, query_id, f-measure)

    print a file to stdout in the format:
    query   threshold   b-cubed-pres    b-cubed-recall  f-measure

    with that output in R we should be able to produce nice plots
    (so we print also a header then)
"""

import idiom
import sys
import db
import re


def get_similarity_threshold(run_id):
    sql = "select description from run_weps where id = %s"
    rec =  db.select_unique(sql, (run_id,))
    re_threshold = re.compile(r'similarity_threshold : ([0-9]*\.[0-9]+)', re.MULTILINE)
    match = re_threshold.search(rec['description'])
    assert match is not None, "no similarity threshold found in description!"
    return float(match.group(1))

if len(sys.argv) != 2:
    print >> sys.stderr, "please specify an experiment dir (as created by run_exp.sh) "
    exit(2)

exp_dir = sys.argv[1].rstrip('/')
runs_dir = "%s/runs" % (exp_dir,)
results_dir = "%s/results" % (exp_dir,)

ls_runs_dir = idiom.ls_dir_or_exit(runs_dir)

# grmpf, .svn entry in each dir must be ignored.
ls_runs_dir = [f for f in ls_runs_dir if not f.endswith('.svn')]

query_tau_bp_br_fm = []

for run in ls_runs_dir:
    run_id = run.split('_')[0]
    tau = get_similarity_threshold(run_id)
    f = open("%s/%s.eval" % (results_dir, run))
    lines = f.readlines()
    for line in lines[1:-1]: # these contain queries (first line is header, last line is average)
        fields = line.strip().split()
        name = fields[0]
        bp = fields[1]
        br = fields[2]
        f_measure = fields[3]
        query_tau_bp_br_fm.append((name, tau, bp, br, f_measure))
    f.close()

print """query   tau bp  br  fm"""
for tup in query_tau_bp_br_fm:
    print "\t".join("%s" % i for i in tup)
