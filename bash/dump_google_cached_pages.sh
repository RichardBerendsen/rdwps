#!/usr/local/bin/bash

# read URLs from stdin


if [ "$#" -ne 1 ] || [ ! -d "$1" ] || [ "$(ls -A "$1")" ]
then
    echo "Please specify an empty directory for storing the google cached pages"
    exit 2
fi
dirname="$1"
old_IFS=$IFS
IFS="$(echo -e '\t')"
while read id url;
do
    w3m -dump -no-cookie -T text/html $url > "$dirname/$id.txt"
    sleep 180;
done
IFS=$old_IFS
