#!/bin/bash
#
# Assumes the script resides alongside the other preprocessing scripts.


print_usage() {
    cat 1>&2 <<EOF
Usage:
$1 similarity_matrices_dir clusterings_from_sliced_dir output_dir run_name sim_threshold_for_social social_weight

EOF
}

if [ "$#" -ne "6" ] || [ ! -d "$1" ] || [ ! -d "$2" ] || [ ! -d "$3" ] || [ "$(ls -A "$3")" ]
then
    print_usage $0
    exit 2
fi

matrices_dir=$(echo "$1" | sed 's+/*$++')
clustering_dir=$(echo "$2" | sed 's+/*$++')
result_dir=$(echo "$3" | sed 's+/*$++')
run_name=$4
sim_thresh_social=$5
soc_weight=$6
script_dir="$( cd "$( dirname "$0" )" && pwd )"
base_script_dir="$( cd "$( dirname "$0" )" && cd .. && pwd )"

echo "Assimilating.."

process_qry() {
  qry=$1
  echo $qry
  mkdir $result_dir/$qry
  $base_script_dir/py/add_social_media_to_clusters.py $clustering_dir/$qry/clustering $matrices_dir/$qry/similarity_matrix $result_dir/$qry/clustering $sim_thresh_social $soc_weight
}

cd $matrices_dir
qs=(`ls -d */`)
for q in ${qs[@]}
do
  process_qry $q
done
