#!/usr/local/bin/bash

# Duplicates the structure of a directory with links.
# Assumes absolute paths.

links_dir=$1
new_dir=$2

if [ "$#" -ne "2" ]
then
    echo "Error, please provide an absolute links directory and an absolute empty directory"
    exit 2
fi
if [ ! -d $links_dir ]
then
    echo "Error, crawls dir path is not a directory"
    exit 2
fi

if [ ! -d $new_dir ]
then
    echo "Error, mirror path is not a directory"
    exit 2
fi

if [ "$(ls -A $new_dir)" ]
then
    echo "Error, mirror path is not empty"
    exit "2"
fi

process_url() {
  dir=$1
  newdir=$2
  url=$3  
  w3m -dump -no-cookie "$url" > "$newdir""/""${url%.html}"
}

process_qry_dir() {
    subdir=$links_dir"/""$1"
    newdir=$new_dir"/""$1"
    
    mkdir $newdir
    cd $subdir    
    for u in $(ls)
    do
      process_url $subdir $newdir $u
    done
    cd ..
} 

for qry in $(ls "${links_dir}")
do
  process_qry_dir $qry
done
  
