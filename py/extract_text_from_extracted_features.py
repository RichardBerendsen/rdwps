"""
extract the useful text from the output of the social media platform content extractors.

Usage:
    %s < features_file > text_file  (this will go through the preprocessing script)
"""

import sys
import os

features = {
        'friend name' : lambda x: x,
        'friend title' : lambda x: x,
        'name' : lambda x: x,
        'pic' : lambda x: os.path.basename(x),
        'platform' : lambda x: "",
        'text' : lambda x: x,
        'url' : lambda x: ""}

while True:

    line = sys.stdin.readline()

    if line == '\n': # empty line
        continue

    if line == '': #eof
        break

    line = line.strip()

    parts = line.split(':')
    if parts[0] in features and len(parts) > 1:
        text = features[parts[0]](':'.join(parts[1:]))
        if text != "":
            print text
