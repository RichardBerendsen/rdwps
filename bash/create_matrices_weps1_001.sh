#!/usr/local/bin/bash

# Assumes the script resides alongside the other preprocessing scripts.
# First argument a weps crawls directory.
# Second argument an empty results directory.
# 

print_usage() {
    cat 1>&2 <<EOF
Usage: 
$1 weps_crawls_dir empty_dir which_weps

where which_weps must be 1 (weps-1-test), 2 (weps-2) or 3 (weps-1-train)
EOF
}

if [ "$#" -ne "3" ] || [ ! -d "$1" ] || [ ! -d "$2" ] || [ "$(ls -A "$2")" ]
then
    print_usage $0
    exit 2
fi

if [ "$3" -ne "1" ] && [ "$3" -ne "2" ] && [ "$3" -ne "3" ]
then
    print_usage $0
    exit 2
fi

crawls_dir=$(echo "$1" | sed 's+/*$++')
result_dir=$(echo "$2" | sed 's+/*$++')
script_dir="$(dirname "$0")"

mkdir $result_dir/matrices_from_stemmed
mkdir $result_dir/matrices_from_normalized

echo "Creating matrices from stemmed documents" 1>&2
$script_dir/../py/create_matrices.py \
"--input-dir=$crawls_dir/stemmed" \
"--output-dir=$result_dir/matrices_from_stemmed" \
"--occurrence-limit=5" \
"--logged-tfs" \
"--run-name=weps_${which_weps}_glob_log_tf_occ5_from_stemmed" \
"--weps-run=$which_weps" \
"--feature-vector-creator=tf_idf_vectors_global"

echo "Creating matrices from stemmed documents" 1>&2
$script_dir/../py/create_matrices.py \
"--input-dir=$crawls_dir/normalized_words" \
"--output-dir=$result_dir/matrices_from_normalized" \
"--occurrence-limit=5" \
"--logged-tfs" \
"--run-name=weps_${which_weps}_glob_log_tf_occ5_from_normalized" \
"--weps-run=$which_weps" \
"--feature-vector-creator=tf_idf_vectors_global"
