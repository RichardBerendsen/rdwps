#!/usr/bin/python
"""
No profilename and friends for linkedin.

Output:
username
profile_picture_url
list of frends separated by #
"""
import sys
import os
import re
from BeautifulSoup import BeautifulSoup
import markup_util


linkedin_boilerplate_phrases=["see all...", "see less...", "see all", "see less"]

def get_profilename(soup):
  return ""

def get_picture(soup):
  img = soup.findAll(lambda x: x.name == "img" and x.has_key("src") and x.parent.has_key("id") and x.parent['id'] == "profile-picture")
  if len(img) > 0:
    return img[0]['src']
  else:
    return ""

def get_friends(soup):
  return [], []

def get_text(soup):
  elems = soup.findAll(lambda x: (x.name == "dd" and x.has_key("class") and (x['class'] == "summary-past" or x['class'] == "summary-present"))\
or (x.name == "span" and x.has_key("class") and (x['class'] == "locality" or x['class'] == "industry"))\
or (x.name == "dd" and x.has_key("class") and (x['class'] == "locality" or x['class'] == "industry"))\
or (x.name == "div" and x.has_key("class") and x['class'] == "postitle"))
  text = ""
  for elem in elems:
    txt = elem.findAll(text=True)
    for t in txt:
      text += " " + markup_util.decode_utf_ref(t.strip())
  for p in linkedin_boilerplate_phrases:
    text = re.sub(p, " ", text)
  return text
