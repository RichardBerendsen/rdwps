#!/usr/bin/python
"""
get query_dir;normalized_url_id pairs to copy for src/bash/cp_crawls.sh

Usage: %s [options]
"""

import sys
import db
from optparse import OptionParser

def _get_search_results(options):
    sql = {
            "SERs" : "select firstname, lastname, query_id, id from search_results where in_weps_corpus = 'yes' and type in ('google', 'yahoo', 'bing') group by query_id, id order by query_id, id",
            "Profiles" : "select firstname, lastname, query_id, id from search_results where in_weps_corpus = 'yes' and type in ('hyves', 'facebook', 'linkedin', 'twitter', 'myspace') group by query_id, id order by query_id, id",
            "both" : "select firstname, lastname, query_id, id from search_results where in_weps_corpus = 'yes' group by query_id, id order by query_id, id"}
    return db.select(sql[options.dataset])

def main(argv):
    parser = OptionParser(usage=__doc__ % argv[0])
    parser.add_option("-s", "--dataset", dest="dataset", action="store", type="choice", choices=["SERs", "Profiles", "both"], default="both")
    options, args = parser.parse_args()
    search_results = _get_search_results(options)
    for s in search_results:
        fname = s['firstname']
        lname = s['lastname']
        chopped_lname = lname.replace("\"", "")
        chopped_lname = chopped_lname.replace(" ", "_")
        print str(s['query_id']) + "_" + fname + "_" + chopped_lname + ";" + str(s['id'])

if __name__ == "__main__":
    sys.exit(main(sys.argv))


