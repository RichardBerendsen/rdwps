"""
Run a repeated HAC experiment

Usage: %s sim_matrix_dir partial_clustering_dir out_dir

where out_dir is an empty directory where you would like to store the clusterings.

NB: this script is not yet suitable to do a repeated HAC experiment on any of the WePS datasets.
To accomodate that, at least store_weps should also be important, and some other minor changes
would have to be made as well. See cluster_with_text.py, which can be used to cluster the weps
datasets.
"""

import sys
import os
import serializer
import hac
import util
import db
import idiom
import cluster_helper
import store_run
import store_run_weps
from optparse import OptionParser

def expand_partial_clustering(partial_clustering, labels):
    """
    partial clustering is a list of lists of doc_ids we return a clustering
    where ids in 'labels' that are clustered in 'clustering' are clustered, and
    the remaining ids in 'labels' are added as singletons
    """
    # now remove the clustered stuff in partial_clustering if it's not present in labels.
    # if singleton clusters remain that's ok, but empty lists should be removed from the
    # partial clustering.
    # note that we assume that partial_clustering is at most one level deep
    labelset = set(labels)
    new_partial_clustering = []
    for cluster in partial_clustering:
        new_cluster = [id for id in cluster if id in labelset]
        if new_cluster:
            new_partial_clustering.append(new_cluster)

    ids_in_new_partial_clustering= set([])
    for cluster in new_partial_clustering:
        for id in cluster:
            ids_in_new_partial_clustering.add(id)

    # now we make a partial clustering of just the ids in 'labels', and all the ids in labels
    expanded_partial_clustering = new_partial_clustering + [[i] for i in labels if not i in ids_in_new_partial_clustering]
    return expanded_partial_clustering

def labels2label_indexes(clustering, labels):
    """
    clustering is a list of lists of labels,
    labels contains labels,

    NB: we assume that labels and clustering correspond here.
    """
    labels2idx = {}
    for idx, value in enumerate(labels):
        labels2idx[value] = idx
    return [[labels2idx[label] for label in cluster] for cluster in clustering]


def cluster_with_text_from_partial(conf, matrices_dir, partial_clustering_dir, query_dir):
    sim_matrix, sim_matrix_labels = serializer.deserialize_similarity_matrix(\
            "%s/%s/similarity_matrix" % (matrices_dir, query_dir))
    partial_clustering = serializer.deserialize_clustering_to_top_level_only(\
            "%s/%s/clustering" % (partial_clustering_dir, query_dir))

    # now in the partial clustering not all ids present in the similarity
    # matrix may be there, and vice versa. We will scale up the partial
    # clustering to include everything that is in the similarity matrix, and
    # ignore what is in the partial clustering but not in the similarity
    # matrix. In other words: the similarity matrix sets the standard.
    expanded_partial_clustering = expand_partial_clustering(partial_clustering, sim_matrix_labels)

    # Now it's about time to do the clustering.
    return hac.cluster_from_partial(sim_matrix, sim_matrix_labels,
            conf['linkage_func'],
            conf['similarity_threshold'], labels2label_indexes(expanded_partial_clustering, sim_matrix_labels))

def cluster_with_text(conf, matrices_dir, query_dir):
    sim_matrix, labels = serializer.deserialize_similarity_matrix(\
            "%s/%s/similarity_matrix" % (matrices_dir, query_dir))
    return hac.cluster(sim_matrix, labels, conf['linkage_func'], \
            conf['similarity_threshold'])

def store_run_and_get_run_id(conn, options, conf, matrices_dir, partial_clustering_dir, out_dir):
    matrices_conf = idiom.read_file_or_empty_string("%s/run.conf" % matrices_dir)
    partial_clustering_conf = idiom.read_file_or_empty_string("%s/run.conf" % partial_clustering_dir)
    run_conf = """Text based clustering from similarity matrics and a partial clustering.

    Settings for obtaining the matrices:
    %s

    Settings for obtaining the partial clustering:
    %s

    Settings for the text based clustering:
    %s
    """ % (matrices_conf, partial_clustering_conf, '\n'.join(["%s : %s" % (key, value) for key, value in conf.iteritems()]))
    print >> sys.stderr, "Run description:\n%s" % (run_conf,)
    if not options.dry_run:
        return cluster_helper.store_run_and_get_run_id("cluster_with_text_from_partial_weps_%s" % (options.which_weps,), run_conf, out_dir, which_weps=options.which_weps, conn=conn)
    else:
        return 1


def main(args):

    progname = args[0]
    parser = OptionParser(usage = __doc__ % progname)
    parser.add_option("-d", "--dry-run", dest="dry_run", action="store_true", default=False,
            help = "do not store stuff in the db or on disk")
    parser.add_option("-f", "--query_id_from", dest="query_id_from", action="store", type="int", default=3)
    parser.add_option("-t", "--query_id_to", dest="query_id_to", action="store", type="int", default=122)
    parser.add_option("-s", "--similarity-threshold", dest="similarity_threshold", action="store", type="float", default=0.1)
    parser.add_option("-m", "--linkage-method", dest="linkage_method", action="store", type="string", default="single_link")
    parser.add_option("-w", "--which-weps", dest="which_weps", action="store", type="int", default="0", help="""
    if this is a weps run, specify which one. If this is not a weps run, don't specify, or set it to 0. 
    1: weps-1-test
    2: weps-2
    3: weps-1-train
    """)
    options, args = parser.parse_args()

    if len(args) != 3:
        print >> sys.stderr, parser.print_help()
        return 2

    matrices_dir = args[0].rstrip('/')
    partial_clustering_dir = args[1].rstrip('/')
    out_dir = args[2].rstrip('/')
    db_storer = store_run
    if options.which_weps:
        db_storer = store_run_weps

    linkage_method_to_linkage_func = {
            'single_link' : hac.cluster_sim_single_link_max,
            'centroid' : hac.cluster_sim_centroid_max
            }

    conf = {
            'linkage_method' : options.linkage_method,
            'linkage_func' : linkage_method_to_linkage_func[options.linkage_method],
            'similarity_threshold': options.similarity_threshold
            }

    # we look in the matrices dir. For each query there, we look if it exists in the
    # partial clustering dir. If so, we start from that partial clustering.
    # We also check that the query is in the database, this is necessary to write
    # the results of this experiment to the database

    # NB: in both directories, a run.conf file may be present, in which there will
    # be a description of parameter settings used to obtain the matrices or partial
    # clusterings in there.

    query_dirs_matrices = idiom.ls_dir_or_exit(matrices_dir)
    query_dirs_matrices.discard('run.conf')
    query_dirs_partial_clustering = idiom.ls_dir_or_exit(partial_clustering_dir)
    query_dirs_partial_clustering.discard('run.conf')
    out_dir_ls = idiom.ls_dir_or_exit(out_dir)
    if out_dir_ls:
        print >> sys.stderr, "Error: out_dir: %s is not empty" % out_dir
        return 2

    conn = db.connect()

    run_id = store_run_and_get_run_id(conn, options, conf, matrices_dir, partial_clustering_dir, out_dir)

    for query_dir in query_dirs_matrices:
        # check if it is in the db.
        if options.which_weps:
            query = util.get_query_from_dirname_weps(conn, query_dir)
        else:
            query = util.get_query_from_dirname(conn, query_dir)
        if not query:
            print >> sys.stderr, "Warning: Could not find query %s in the database" % query_dir
            continue
        elif query['idx'] < options.query_id_from or query['idx'] > options.query_id_to:
            continue
        if query_dir in query_dirs_partial_clustering:
            clustering, labelled_clustering = cluster_with_text_from_partial(conf, \
                    matrices_dir, partial_clustering_dir, query_dir)
        else:
            clustering, labelled_clustering = cluster_with_text(conf, \
                    matrices_dir, query_dir)
        print >> sys.stderr, "Writing clustering to disk, creating dir %s/%s" % (out_dir, query_dir)
        if not options.dry_run:
            db_storer.store_clustering_in_db(labelled_clustering, run_id, query['idx'])
            cluster_helper.write_clustering_to_disk(labelled_clustering, out_dir, query_dir)

    conn.close()

if __name__ == '__main__':
    sys.exit(main(sys.argv))
