import sys
import re
import connect

def _store_url(conn, translated_url, id):
    cursor = conn.cursor()
    cursor.execute("update google_cached_urls set translated_URL = %s where id = %s", (translated_url, id))
    cursor.close()

if len(sys.argv) != 2:
    print >> sys.stderr, "Please specify google_cached_url_id"
    sys.exit(2)

google_cached_url_id = sys.argv[1]

page = sys.stdin.read().strip()

# now just find 'This is Google's cache of', if it's not there, print an empty line 
pat_url = re.compile(
        r'This is Google\'s cache of (http://.+)\.\s{1,2}It\sis\sa\ssnapshot\sof\sthe\spage\sas\sit\sappeared\son', re.DOTALL)
match_url = pat_url.search(page)
if match_url is not None:
    url =  match_url.group(1).rstrip()
    url = url.replace('\n', '')
    conn = connect.connect()
    _store_url(conn, url, google_cached_url_id)
    conn.close()

