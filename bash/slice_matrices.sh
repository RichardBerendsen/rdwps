#!/bin/bash
#
# Assumes the script resides alongside the other preprocessing scripts.
# First argument a matrices directory.
# Second argument an empty results directory.

print_usage() {
    cat 1>&2 <<EOF
Usage: 
$1 similarity_matrices_dir empty_dir

EOF
}

if [ "$#" -ne "2" ] || [ ! -d "$1" ] || [ ! -d "$2" ] || [ "$(ls -A "$2")" ]
then
    print_usage $0
    exit 2
fi


matrices_dir=$(echo "$1" | sed 's+/*$++')
result_dir=$(echo "$2" | sed 's+/*$++')
script_dir="$( cd "$( dirname "$0" )" && pwd )"
base_script_dir="$( cd "$( dirname "$0" )" && cd .. && pwd )"


process_qry() {
  qry=$1
  echo $qry
  mkdir $result_dir/$qry
  sleep 3; # this is to allow conn.close() statements to execute, otherwise db gets flooded
  $base_script_dir/py/slice_sim_matrix.py $matrices_dir/$qry/similarity_matrix $result_dir/$qry/similarity_matrix
}


cd $matrices_dir
qs=(`ls -d */`)
for q in ${qs[@]}
do
  process_qry $q
done


