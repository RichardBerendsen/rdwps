#!/bin/bash
#run this on a weps-1 data directory prior to running preprocess.sh

print_usage() {
    cat <<EOF
Usage: 
$1 weps1_web_pages_dir empty_dir
EOF
}

if [ $# -ne 2 ] || [ ! -d $1 ] || [ ! -d $2 ] || [ "$(ls -A $2)" ]
then
    print_usage $0
    exit 2
fi

# strip trailing slashes with sed.
web_pages_dir=$(echo "$1" | sed 's+/*$++')
echo $web_pages_dir
crawls_dir=$(echo "$2" | sed 's+/*$++')

for query_dir in $(ls $web_pages_dir)
do
    mkdir "$crawls_dir/$query_dir"
    for doc_dir in $(ls "$web_pages_dir/$query_dir/raw")
    do
        # grab first .html doc whatever its name (assume there is just one) 
        # and copy it to a file without the .html extension.
        find "$web_pages_dir/$query_dir/raw/$doc_dir" -name '*.html' -exec cp {} "$crawls_dir/$query_dir/$doc_dir.html" \;
    done
done
