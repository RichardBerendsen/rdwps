#!/usr/bin/python

import MySQLdb
import MySQLdb.cursors
import urllib
from pprint import pprint
from urlparse import urlparse
import re
import random

def _connect():
	return MySQLdb.connect (host = "qassir",
						   user = "bogomil",
						   passwd = "wieowie",
						   db = "people",
                           cursorclass = MySQLdb.cursors.DictCursor)
def _get_queries(conn):
    cursor = conn.cursor()
    cursor.execute("select * from selected_queries")
    queries = cursor.fetchall()
    cursor.close()
    return queries    
    
def _get_search_results(conn):
    cursor = conn.cursor()
    cursor.execute("select * from search_results")
    srch_results = cursor.fetchall()
    cursor.close()
    return srch_results

def _get_top_level_domain(url):
    parsed = urlparse(url)
    domain = ""
    if (url[0:4] == "http"):
      domain = parsed[1]
    elif (url[0:4] == "www."):
      domain = parsed[2]
    else:
      raise Exception("unexpected url format")    
    levels = re.split("\.", domain)
    return ".".join(levels[-2:])
      
def _get_all_bins(search_results, queries):
   all_bins = {}
   for q in queries:
     key = q['FirstName'] + "_" + q['LastName'] 
     all_bins[key] = []
   
   for s in search_results:
     key = s['firstname'] + "_" + s['lastname']
     top_domain = _get_top_level_domain(s['url'])
     if top_domain not in all_bins[key]:
       all_bins[key].append(top_domain)
   return all_bins
       
def _get_bins(FirstName, LastName, search_results):
   bins = {}
   for s in search_results:
      if s['firstname'] != FirstName or s['lastname'] != LastName:
        continue
      key = _get_top_level_domain(s['url'])
      if key in bins.keys():
        bins[key].append(s['auto_incr_id'])
      else:
        bins[key] = [s['auto_incr_id']]
   return bins

def _get_next_nonempty_bin(bins, last_bin):
    keys = sorted(bins.keys())
    for i in range(last_bin+1, len(keys)):
      if len(bins[keys[i]]) != 0:
        return i
        
    for i in range(0, last_bin+1):
      if len(bins[keys[i]]) != 0:
        return i
    return -1  
    
    
def _get_next(bins, last_key):    
    keys = sorted(bins.keys())
    next_nonempty = _get_next_nonempty_bin(bins, last_key)
    if next_nonempty == -1:
      return None, -1
    res = bins[keys[next_nonempty]][0]
    bins[keys[next_nonempty]] = bins[keys[next_nonempty]][1:]
    return res, next_nonempty
    
def _get_all(bins):
    waits = {}
    srt = sorted(bins.keys())
    for k in sorted(bins.keys()):
      waits[srt.index(k)] = -1
    res = []
    keys = sorted(bins.keys())
    last_key = -1
    while(True):
      next_res, key_used = _get_next(bins, last_key)
      if (next_res == None):
        return res
      else:
        if waits[key_used] == -1:
            waittime = 0
        elif waits[key_used] < 60:
            waittime = 50 - waits[key_used] + random.randrange(0,20)
            waittime = 0 if waittime < 0 else waittime
        else:
            waittime = 0
        for k in waits.keys():      
            waits[k] += waittime
        waits[key_used] = 0
        res.append((waittime, next_res))
      
      last_key = key_used      

   
if __name__ == "__main__":
    conn = _connect()
    queries = _get_queries(conn)
    search_results = _get_search_results(conn)
    count = 0
    for q in queries:
      fname = q['FirstName']
      lname = q['LastName']
      chopped_lname = lname.replace("\"", "")
      chopped_lname = chopped_lname.replace(" ", "_")
      bins = _get_bins(fname, lname, search_results)
      all_urls = _get_all(bins)
      
      for u in all_urls:
        waittme, url = u
        row = [s for s in search_results if s['auto_incr_id'] == url][0]
        print str(q['idx']) + "_" + fname + "_" + chopped_lname + ";" + str(row['id']) + ";" + str(waittme) + ";" + row['url']

    
    
    
    #all_bins = _get_all_bins(search_results, queries)
    

    
    #urls = [r['url'] for r in search_results if r['url'][0:4] != "http"]
    
    #pprint(bins.keys()[0])
    #pprint(sorted(bins[bins.keys()[0]]))
    