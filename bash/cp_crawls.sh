#!/usr/local/bin/bash

# read from stdin, format
# query_id ; normalized_url_id
# (idea: query-url ids that have the person name in the crowbarred page

if [ "$#" -ne 2 ]
then
    echo "Error, please provide crawls directory and an empty directory" 1>&2
    exit 2
fi
crawls_dir="$1"
mirror_dir="$2"
if [ ! -d "$crawls_dir" ] || [ ! -d "$mirror_dir" ]
then
    echo "Error, please provide crawls_dir directory and an empty directory" 1>&2
    echo "Error, one of the arguments is not a directory" 1>&2
    exit 2
fi
if [ "$(ls -A "$mirror_dir")" ]
then
    echo "Notice: mirror dir is not empty" 1>&2
fi

# convert the possibly relative paths to absolute paths
cwd="$(pwd)"
cd "$crawls_dir"
crawls_dir="$(pwd)"
cd "$cwd"
cd "$mirror_dir"
mirror_dir="$(pwd)"

while read line;
do
    query_id=$(echo "$line" | cut -f 1 -d ';')
    normalized_url_id=$(echo "$line" | cut -f 2 -d ';')

    # create query directory if it is not there 
    if [ ! -e "${mirror_dir}/${query_id}" ]
    then
        mkdir "${mirror_dir}/${query_id}"
    fi

    # check if normalized url id dir is not there already
    if [ -e "${mirror_dir}/${query_id}/${normalized_url_id}" ]
    then
        echo "Warning: skipping ${normalized_url_id} in query dir $query_id, it exists already" 1>&2
        continue
    fi

    # check if crawl is there
    if [ ! -d "${crawls_dir}/$query_id/$normalized_url_id" ]
    then
        echo "Warning, crawl for qeury $query_id and url $normalized_url_id does not exist, skipping" 1>&2
        continue
    fi

    # cp the stuff
    cp -a "$crawls_dir/$query_id/$normalized_url_id" "${mirror_dir}/${query_id}/${normalized_url_id}"

done
