#!/bin/bash

print_usage() {
    cat <<EOF
Usage:
$1 new_dir" 1>&2
EOF
}

if [ $# -ne 1 ] || [ -e "$1" ]
then
    print_usage $0
    exit 2
fi

mkdir "$1"
mkdir "$1/keep"
mkdir "$1/keep/uniques"
mkdir "$1/discard"
mkdir "$1/duplicates"
