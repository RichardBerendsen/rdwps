import in_weps_corpus
import sys
import connect

def store_in_weps_corpus(conn, rec):
	cursor = conn.cursor()
	sql = "update search_results set in_weps_corpus = 'yes' where query_id = %s and id = %s"
	cursor.execute(sql, (rec['query_id'], rec['id']))
	cursor.close()

def main(args):

	conn = connect.connect()

	crowbarred_pages_with_name = in_weps_corpus.get_crowbar_docs_per_query(conn)
	wgot_pages_with_name = in_weps_corpus.get_wget_docs_per_query(conn)

	for rec in crowbarred_pages_with_name + wgot_pages_with_name:
		store_in_weps_corpus(conn, rec)

	conn.close()

if __name__ == "__main__":
	sys.exit(main(sys.argv))
