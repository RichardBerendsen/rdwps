#!/usr/local/bin/bash

# Assumes absolute paths.
# Assumes the script resides alongside the other preprocessing scripts.
# First argument a crawls directory.
# Second argument an empty results directory.


crawls_dir=$1
result_dir=$2
script_dir="$( cd "$( dirname "$0" )" && pwd )"
base_script_dir="$( cd "$( dirname "$0" )" && cd .. && pwd )"

if [ "$#" -ne "2" ]
then
    echo "Error, please provide an absolute links directory and an absolute empty directory"
    exit 2
fi

if [ ${crawls_dir:0:1} != "/" ]
then
    echo "Error, please provide an absolute links directory and an absolute empty directory"
    exit 2
fi

if [ ${result_dir:0:1} != "/" ]
then
    echo "Error, please provide an absolute links directory and an absolute empty directory"
    exit 2
fi


if [ ! -d $crawls_dir ]
then
    echo "Error, crawls dir path is not a directory"
    exit 2
fi

if [ ! -d $result_dir ]
then
    echo "Error, mirror path is not a directory"
    exit 2
fi

if [ "$(ls -A $result_dir)" ]
then
    echo "Error, mirror path is not empty"
    exit "2"
fi

echo "Crawls dir is ""$crawls_dir"
echo "Result dir is ""$result_dir"
echo "Script dir is ""$script_dir"
echo "Base script dir is ""$base_script_dir"

mkdir $result_dir/links_to_crawls_no_extensions
mkdir $result_dir/links_to_crawls_with_extensions
mkdir $result_dir/crawls_copy
mkdir $result_dir/w3mmed_documents
mkdir $result_dir/stemmed_documents
mkdir $result_dir/matrices

echo "Copying crawls to temporary directory.."

cp -r $crawls_dir/* $result_dir/crawls_copy

echo "Creating symbolic links without extensions.."

cd $script_dir
./make_symbolic_links_to_crawled_pages.sh $result_dir/crawls_copy $result_dir/links_to_crawls_no_extensions

echo "Creating symbolic links with .html extensions.."

./make_symbolic_links_to_crawled_pages.sh $result_dir/crawls_copy $result_dir/links_to_crawls_with_extensions .html

echo "Removing noscript tags from html pages.."

python $base_script_dir/py/drop_noscript_tags.py $result_dir/links_to_crawls_no_extensions

echo "Passing documents through w3m.."

$script_dir/w3m_dump_htmls.sh $result_dir/links_to_crawls_with_extensions $result_dir/w3mmed_documents

echo "Stemming.."

$script_dir/stem_htmls_after_w3m.sh $result_dir/w3mmed_documents $result_dir/stemmed_documents

#echo "Creating matrices.."

#python $base_script_dir/py/create_matrices.py --input-dir=$result_dir/stemmed_documents --output-dir=$result_dir/matrices

