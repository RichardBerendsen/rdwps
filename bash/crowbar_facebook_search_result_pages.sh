#!/bin/bash

# read URLs from stdin

# assumes:
# XULRUNNER is installed
# Crowbar (http://simile.mit.edu/wiki/Crowbar) is installed
# Crowbar is running (and listening on 127.0.0.1:10000)
# Run crowbar (on MacOSX) with the command:
# $> /Applications/Crowbar.app/Contents/MacOS/xulrunner
while read line;
do
    query_id=$(echo $line | cut -f 1 -d ';')
    query_url=$(echo $line | cut -f 2 -d ';')
    curl -s --data "url=$query_url&delay=10000" "http://127.0.0.1:10000" > "../../exp/facebook_search_result_per_query/$query_id.html"
	n=$RANDOM
	let "n %= 10"
	sleep $((106 + $n))
done
