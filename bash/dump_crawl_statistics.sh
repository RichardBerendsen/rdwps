#!/usr/local/bin/bash
# run over a directory structure of documents, and report some statistics, such as:
# does document contain person name? filetype of document? something like that.

if [ "$#" -ne 1 ]
then
    echo "Usage: $(basename $0) dir"
    echo "Where dir is a symbolic link mirror of a crawl directory."
    exit 2
fi

doc_dir="$1"
cwd="$(pwd)"
self_dir="$cwd/$(dirname "$0")"
cd "$doc_dir"
doc_dir="$(pwd)"

cd "${doc_dir}"
for query_dir in $(ls)
do
    # these are query directories
    query_id="$(echo $(basename "$query_dir") | cut -f 1 -d '_')"

    cd "$query_dir"
    for doc in $(ls)
    do
        
        if [ -d "$doc" ]
        then
            continue
        fi

        # collect some properties, and store these in the database.

        doc_id="$(basename "$doc")"
        mime=$(file --mime --brief --dereference "$doc")
        # store the mime type.
        python "${self_dir}/../py/store_mime_type.py" "$doc_id" "$mime"

        # is the person name in there? store this only for html files
        if [ "$(expr match "$mime" "text/html")" == "9" ] || [ "$(expr match "$mime" "application/xhtml+xml")" == "21" ]
        then
            w3m -dump "$doc" | python "${self_dir}/../py/parse_w3m_person_name.py" "$query_id" "$doc_id" 
        fi

        # idem for pdf files
        if [ "$(expr match "$mime" "application/pdf")" == "15" ]
        then
            pdftotext -nopgbrk "$doc"
            python "${self_dir}/../py/parse_w3m_person_name.py" "$query_id" "$doc_id" < "${doc}.txt"
            rm "${doc}.txt"
        fi

    done
    cd ".."
    break
done

