# read from stdin an html document file and check whether or not the person name is in there.
import MySQLdb
import MySQLdb.cursors
import sys

def _connect():
	return MySQLdb.connect (host = "qassir",
						   user = "bogomil",
						   passwd = "wieowie",
						   db = "people",
                           cursorclass = MySQLdb.cursors.DictCursor)

def _store_mimetype(normalized_url_id, mimetype):
    conn = _connect()
    cursor = conn.cursor()
    cursor.execute("select count(*) as count from normalized_url as n, search_results as s where n.id = %s and n.id = s.id and s.query_id = 10" % (normalized_url_id,))
    if cursor.fetchone()['count'] == 0:
        print "%s: normalized_url_id \"%s\"is not in db for query 10" % (sys.argv[0], normalized_url_id)
    cursor.execute("update normalized_url set mimetype= %s where id = %s", (mimetype, normalized_url_id))
    res = cursor.fetchall()
    cursor.close()
    conn.close()
    return res

if __name__ == "__main__":

    if len(sys.argv) != 3:
        sys.stderr.write("Usage: %s doc_id mimetype\n" % (sys.argv[0],))
        sys.exit(2)

    doc_id = sys.argv[1].strip()
    mimetype = sys.argv[2].strip()

    _store_mimetype(doc_id, mimetype)
