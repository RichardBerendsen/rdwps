#!/usr/bin/python

import re
import sys
import math
import util

# global variables to keep collection wide information
docs_collection = {}
lengths_collection = {}
terms_collection = {}
tf_vectors_collection = {}


#def get_tf_idf_matrix(directory, stopwordfile, occurrence_limit):
def get_tf_idf_matrix(docs_as_strings, stopwords, occurrence_limit, term_window, stemmer_used, logged_tfs):
  docs, lengths = represent_collection_query(docs_as_strings, term_window, stemmer_used)
  print >> sys.stderr, "Converted docs."
  terms = get_collection_terms(docs.values(), stopwords, occurrence_limit)
  print >> sys.stderr, "Collected terms."
  tf_vectors = get_tf_vectors(docs, lengths, terms, logged_tfs)
  print >> sys.stderr, "Obtained tf vectors.", len(tf_vectors)
  idfs = get_idfs(terms, tf_vectors)
  print >> sys.stderr, "Obtained idfs.", len(idfs)
  tf_idf_vectors = get_tf_idf_vectors(tf_vectors, idfs, terms)
  print >> sys.stderr, "Obtained tf_idf vectors."
  return tf_idf_vectors, terms

def get_tf_idf_matrix_global(collection_as_strings, docs_as_strings, stopwords, occurrence_limit, term_window, stemmer_used, logged_tfs):
  global docs_collection
  global lengths_collection
  global terms_collection
  global tf_vectors_collection
  if len(docs_collection) == 0:
    docs_collection, lengths_collection = represent_collection_full(collection_as_strings, term_window, stemmer_used)
  docs_query, lengths_query = represent_collection_query(docs_as_strings, term_window, stemmer_used)
  #print "DEBUG", len(collection_as_strings), len(docs_collection), len(docs_as_strings)
  print >> sys.stderr, "Converted docs."
  terms_query = get_collection_terms(docs_query.values(), stopwords, occurrence_limit)
  print >> sys.stderr, "Collected terms query"
  if len(terms_collection) == 0:
    terms_collection = get_collection_terms(docs_collection.values(), stopwords, occurrence_limit)
  print >> sys.stderr, "Collected terms collection."
  tf_vectors_query = get_tf_vectors(docs_query, lengths_query, terms_query, logged_tfs)
  if len(tf_vectors_collection) == 0:
    tf_vectors_collection = get_tf_vectors(docs_collection, lengths_collection, terms_collection, logged_tfs)
  print >> sys.stderr, "Obtained tf vectors.", len(tf_vectors_query)
  #print "DEBUG!!",[t for t in terms_query if t not in terms_collection ], len(terms_query), len(terms_collection)
  idfs_query = get_idfs(terms_query, tf_vectors_collection)
  tf_idf_vectors_query = get_tf_idf_vectors(tf_vectors_query, idfs_query, terms_query)
  print >> sys.stderr, "Obtained tf_idf vectors.", len(tf_idf_vectors_query)
  return tf_idf_vectors_query, terms_query
"""
  Input: vectors as a hash of hashes, metric as a function
taking two lists of the same size and returing a real.
  Output: a list of lists X representing the matrix and a list of strings Y representing the documents.Y[i] is the name of document X[i].
"""
def get_similarity_matrix(vectors, metric):
  sim_matrix = []
  labels = sorted(vectors.keys())
  n = len(labels)
  for i in range (0, n):
    #print i
    row = []
    for j in range (0, i + 1):
      distance = 1.0 if i == j else metric(vectors[labels[i]], vectors[labels[j]])
      row.append(distance)
    row[i+1:]=[0 for k in range(i+1,n)]
    sim_matrix.append(row)
  # populate the part above the main diagonal by symmetry
  for i in range (0, len(labels)):
    for j in range (i+1,n):
      sim_matrix[i][j]=sim_matrix[j][i]
  return sim_matrix, labels

# gets the indexes of the positions in the array of tokens that
# correspond to the full name in the correct order
def _get_starting_indexes(toks, names):
  res = []
  for i in range(0, len(toks)):
    is_right_index = False
    for j in range(0, len(names)):
      if i+j >= len(toks) or toks[i+j] != names[j]:
       #print "breaking on", j, i, is_right_index
       is_right_index = False
       break
      is_right_index = True
    if is_right_index:
      res.append(i)
  return res

# applies the term window to the list of tokens
def _get_resulting_terms(toks, starting_indexes, names, term_window):
  if term_window == 0:
    return toks
  names_len = len(names)
  indexes_len = len(starting_indexes)
  toks_len = len(toks)
  indexes_result=[]
  res = []
  for i in range(0, indexes_len):
    for k in range(0, names_len):
      index_of_name_part_to_add = starting_indexes[i] + k
      if index_of_name_part_to_add not in indexes_result:
        #print "AAA", index_of_name_part_to_add
        indexes_result.append(index_of_name_part_to_add)
    for j in range(0, term_window):
      index_of_left_to_add = starting_indexes[i] - j -1
      index_of_right_to_add = starting_indexes[i] + names_len + j
      if index_of_left_to_add > 0 and index_of_left_to_add not in indexes_result:
        #print "BBB", index_of_left_to_add
        indexes_result.append(index_of_left_to_add)
      if index_of_right_to_add < toks_len and index_of_right_to_add not in indexes_result:
        #print "CCC", index_of_right_to_add
        indexes_result.append(index_of_right_to_add)
  #print indexes_result
  #print starting_indexes
  return [toks[j] for j in indexes_result]


# returns a hash repsresentation of the doc plus the length of the doc
def _represent_as_hash(doc, term_window, names):
  res = {}
  toks = re.split("\s+", doc.strip())
  toks = filter(lambda x: len(x) > 0, toks)
  name_indexes = _get_starting_indexes(toks, names)
  toks = _get_resulting_terms(toks, name_indexes,names, term_window)
  for t in toks:
    if t in res:
      res[t] += 1
    else:
      res[t] = 1
  return res, len(toks)

# input - hash of strings - keys are normalized urls, values are string representations of documents
# output - two hashes - the first one represents the collection as a hash of documents (document = a hash {term : frequency}), the second one is a hash with elements of the form {docid: length}

def represent_collection_full(docs, term_window, stemmer_used):
  res_hashes = {}
  res_lengths = {}
  for k in docs.keys():
    query=k[:k.index("|")]
    urlid=k[k.index("|")+1:]
    names_unstemmed=re.split("_", query)
    names = []
    for n in names_unstemmed:
      names.append(util.stem(stemmer_used, n))
    hash_repr, length = _represent_as_hash(docs[k], term_window, names)
    #res_hashes[urlid] = hash_repr
    #res_lengths[urlid] = length
    res_hashes[k] = hash_repr
    res_lengths[k] = length
  return res_hashes, res_lengths

def represent_collection_query(docs, term_window, stemmer_used):
  res_hashes = {}
  res_lengths = {}
  for k in docs.keys():
    query=k[:k.index("|")]
    urlid=k[k.index("|")+1:]
    names_unstemmed=re.split("_", query)
    names = []
    for n in names_unstemmed:
      names.append(util.stem(stemmer_used, n))
    hash_repr, length = _represent_as_hash(docs[k], term_window, names)
    res_hashes[urlid] = hash_repr
    res_lengths[urlid] = length
  return res_hashes, res_lengths

# docs - a list of lists of strings - each list of strings is a document
# stopwords - a list with stopwords

def get_collection_terms(docs, stopwords, limit):
  terms_hash = {}
  for d in docs:
    #print >> sys.stderr, d
    for t in d.keys():
      #if (t == "ebook"):
      #  print "DEBUG!!!", t
      if (t in terms_hash):
        terms_hash[t] += d[t]
      elif (t not in stopwords):
        terms_hash[t] = d[t]
  #print >> sys.stderr, terms_hash, limit
  #print >> sys.stderr, [t for t in terms_hash.keys() if terms_hash[t] >= limit]
  return [t for t in terms_hash.keys() if terms_hash[t] >= limit]

def _get_avg_freq(doc):
  occurring_terms = doc.keys()
  sum = 0
  for t in occurring_terms:
    sum += doc[t]
  return sum/(len(occurring_terms) + 0.0)

def _get_tf_vector_log(doc, length, terms):
  res = {}
  if length == 0:
    for t in terms:
      res[t] = 0.0
    return res
  avg_freq_logged = 1 + math.log(_get_avg_freq(doc))
  for t in terms:
    if t not in doc:
      res[t] = 0.0
    else:
      res[t] = (1 + math.log(doc[t])) / avg_freq_logged
  return res

def _get_tf_vector(doc, length, terms):
  res = {}
  if length == 0:
    for t in terms:
      res[t] = 0.0
    return res
  for t in terms:
    cnt = 0 if not t in doc else doc[t]
    res[t] = cnt/ (length + 0.0)
  return res

# docs - hashtable
# returns a hashtable

def get_tf_vectors(docs, lengths, terms, logged_tfs=False):
  res = {}
  for k in docs.keys():
    if logged_tfs:
      res[k] = _get_tf_vector_log(docs[k], lengths[k], terms)
    else:
      res[k] = _get_tf_vector(docs[k], lengths[k], terms)
  return res

# tf_vectore - a hash where each key is a urlid and each value
#is a tf vector, i.e. a hash containing the term occurances
# terms - all terms of the collection
# output - ahash where each key is a urlid and each value is a tfidf vector
def get_tf_idf_vectors(tf_vectors, idfs, terms):
  res = {}
  for k in tf_vectors.keys():
    tf_idf_vector = {}
    for t in terms:
      tf = tf_vectors[k][t]
      #idf = _get_idf(t, tf_vectors)
      tf_idf_vector[t] = tf*idfs[t]
    res[k] = tf_idf_vector
  return res

# returns a hash with term as key and idfs as vector
def get_idfs(terms, tf_vectors):
  res = {}
  for t in terms:
    res[t] = _get_idf(t, tf_vectors)
  return res

def _get_idf(term, tf_vectors):
  #print >> sys.stderr, term
  count = 0
  for v in tf_vectors.keys():
    #print "DEBUG", tf_vectors[v]['20answer']    
    if tf_vectors[v][term] > 0:
      count += 1
  #print >> sys.stderr, len(tf_vectors)/(count + 0.0)
  return math.log(len(tf_vectors)/(count + 0.0))

if __name__=="__main__":
  print _represent_as_hash("uu ii bb aa cc aa bb cc dd gg ee aa bb", 2, ["aa", "bb"])
