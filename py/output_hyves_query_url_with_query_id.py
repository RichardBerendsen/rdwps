#!/usr/bin/python

from get_queries import get_queries
import sys
import urllib


def _query_url(firstname, lastname):
    firstname = firstname.replace('"', '').strip()
    lastname = lastname.replace('"', '').strip()
    words = firstname.split() + lastname.split()
    query_string = '+'.join(words)
    url = "http://www.hyves.nl/search/hyver?searchterms=" \
        + query_string
    url = urllib.quote(url, safe='')
    return url


if __name__ == "__main__":
    queries = get_queries()
    for q in queries:
        sys.stdout.write('%s;%s\n' % (q['idx'], _query_url(q['FirstName'], q['LastName'])))
