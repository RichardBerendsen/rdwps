#!/usr/bin/python
"""
Usage:
    %s

Reads in a html document from stdin and serializes it to stdout with its
noscript tags stripped
"""

from BeautifulSoup import BeautifulSoup
import sys
import os


def main(args):

    html = sys.stdin.read()

    try:
        soup = BeautifulSoup(html)
    except Exception, e:
        print >> sys.stderr, e
        if html.find("noscript") > 0:
            print >> sys.stderr, "Warning: could not beautifulsoup, but there is a noscript tag inside"
        else:
            print >> sys.stderr, "Notice: could not beautifulsoup"
        return 3
    else:
        noscripts = soup.findAll("noscript")
        [el.extract() for el in noscripts]
        print soup.prettify()


if __name__ == "__main__":
    sys.exit(main(sys.argv))
