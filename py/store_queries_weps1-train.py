"""store the queries in the WePS1 train dataset"""

import sys
import os
import db


def main(args):

    f = open('../bash/queries_weps1_train')
    conn = db.connect()
    sql = "insert into selected_queries_weps (FirstName, LastName) values (%s, %s)"
    for line in f.readlines():
        words = line.strip().split(',')
        if len(words) == 2:
            db.insert(sql, (words[0].strip(), words[1].strip()))

    f.close()

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
