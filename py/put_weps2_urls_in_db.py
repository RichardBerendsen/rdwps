#!/usr/bin/python
"""
A script to be executed one time. It puts the weps2 urls in the db.
Usage: python script metadata_dir_weps2
"""

import db
import os
import sys
import util
from BeautifulSoup import BeautifulSoup

conn = db.connect()
cur = conn.cursor()
cur.execute("create table normalized_url_weps2(query_id int, rank int, normalized_url varchar(765))")

def process_file(f, path):
  print f
  query_id = str(util.get_query_from_dirname_weps(conn, f[:f.index(".")])['idx'])
  print query_id
  soup = BeautifulSoup(open(path + "/" + f, "r").read())
  docs = soup.findAll(lambda t: t.has_key("rank") and t.has_key("url") and t.name=="doc")
  for d in docs:
    rank = str(d["rank"])
    normalized_url = util.normalize(d["url"])
    cur.execute("insert into normalized_url_weps2(query_id, rank, normalized_url) values (" + query_id + ", " + rank + " ,\"" + normalized_url +"\")")


d = os.listdir(sys.argv[1])
path = os.path.abspath(sys.argv[1])
for f in d:
  process_file(f, path)



