
"""

  File contatining the implementations of the feature vector creators.
"""

import serializer
import vector_utils

collection_docs_global_var={}

def tf_idf_vectors_local(base_directory, query_directory, stopwordfile, occurrence_limit, term_window, stemmer_used, logged_tfs):
  docs_as_strings=serializer.get_query_docs_as_strings(query_directory)
  stopwords=serializer.get_stopwords(stopwordfile)
  return vector_utils.get_tf_idf_matrix(docs_as_strings, stopwords, occurrence_limit, term_window, stemmer_used, logged_tfs)

def tf_idf_vectors_global(base_directory, query_directory, stopwordfile, occurrence_limit, term_window, stemmer_used, logged_tfs):
  global collection_docs_global_var
  stopwords=serializer.get_stopwords(stopwordfile)
  docs_as_strings=serializer.get_query_docs_as_strings(query_directory)
  if len(collection_docs_global_var) >0 :
    print "RETURNING FROM GLOBALS"
    collection_docs_as_strings = collection_docs_global_var
  else:
    collection_docs_as_strings = serializer.get_collection_docs_as_strings(base_directory)
    collection_docs_global_var = collection_docs_as_strings
  return vector_utils.get_tf_idf_matrix_global(collection_docs_as_strings,docs_as_strings, stopwords, occurrence_limit, term_window, stemmer_used, logged_tfs)


