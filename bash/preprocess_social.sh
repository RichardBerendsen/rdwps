#!/usr/local/bin/bash

# First argument a crawls directory.
# Second argument an empty results directory.

crawls_dir=$1
result_dir=$2
script_dir="$( cd "$( dirname "$0" )" && pwd )"
base_script_dir="$( cd "$( dirname "$0" )" && cd .. && pwd )"

if [ "$#" -ne "2" ]
then
    echo "Error, please provide an absolute crawls directory and an absolute empty directory"
    exit 2
fi

if [ ${crawls_dir:0:1} != "/" ]
then
    echo "Error, please provide an absolute crawls directory and an absolute empty directory"
    exit 2
fi

if [ ${result_dir:0:1} != "/" ]
then
    echo "Error, please provide an absolute links directory and an absolute empty directory"
    exit 2
fi

if [ ! -d $crawls_dir ]
then
    echo "Error, crawls dir path is not a directory"
    exit 2
fi

if [ ! -d $result_dir ]
then
    echo "Error, mirror path is not a directory"
    exit 2
fi

if [ "$(ls -A $result_dir)" ]
then
    echo "Error, mirror path is not empty"
    exit "2"
fi

echo "Crawls dir is ""$crawls_dir"
echo "Result dir is ""$result_dir"
echo "Script dir is ""$script_dir"
echo "Base script dir is ""$base_script_dir"


for query_id in $(ls "${crawls_dir}")
do
    # create query directory in mirror_path if it is not there
    if [ ! -e "${result_dir}/${query_id}" ]
    then
        mkdir "${result_dir}/${query_id}"
    fi
    for normalized_url_id in $(ls "${crawls_dir}/${query_id}")
    do
        outdir="${result_dir}/${query_id}/${normalized_url_id}"
        mkdir ${outdir}
        #echo "${crawls_dir}/${query_id}/${normalized_url_id}/wget_log"
        python $base_script_dir/py/extractor.py "${crawls_dir}/${query_id}/${normalized_url_id}/wget_log" $normalized_url_id ${outdir}
    done

done
