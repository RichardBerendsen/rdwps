#!/usr/local/bin/bash

# read URLs from stdin, which associated search result id.

# format
# query_id ; normalized_url_id ; time_out; url


if [ "$#" -ne 1 ]
then
    echo "Error, please provide crawls directory"
    exit 2
fi
mypath=$1
if [ ! -d "$mypath" ]
then
    echo "Error, crawls dir path is not a directory"
    exit 2
fi
if [ "$(ls -A "$mypath")" ]
then
    echo "Notice: crawls dir is not empty"
fi

# let's go to the root path where we will store our crawl hierarchy
cd "${mypath}"
abs_path=$(pwd)
unset mypath

while read line;
do
    query_id=$(echo "$line" | cut -f 1 -d ';')
    normalized_url_id=$(echo "$line" | cut -f 2 -d ';')
    timeout=$(echo "$line" | cut -f 3 -d ';')
    url=$(echo "$line" | cut -f 4 -d ';')

    # let's sleep a bit, according to the timeout in our input
	sleep "$timeout"

    # create query directory if it is not there 
    if [ ! -e "${abs_path}/${query_id}" ]
    then
        mkdir "${abs_path}/${query_id}"
    fi

    cd "${abs_path}/${query_id}"

    # create normalized_url_id directory if it is not there 
    if [ -e "${normalized_url_id}" ]
    then
        echo "Notice: skipping ${normalized_url_id}, it exists already"
        continue
    fi

    mkdir "${normalized_url_id}"
    cd "${normalized_url_id}"

    # now let's let wget rock and roll
    wget \
    --append-output "wget_log" \
    --user-agent="Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.2.14) Gecko/20110218 Firefox/3.6.14" \
    --convert-links \
    --page-requisites \
    --backup-converted \
    --span-hosts \
    --html-extension \
    --timestamping \
    --execute robots=off \
    $url

    # and this is the explanation of all these options
    #wget \
    #--append-output "wget_log" \ # error messages, status, anything.
    #--user-agent="Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.2.14) Gecko/20110218 Firefox/3.6.14" \ # or risk being refused at facebook and other sites
    #--convert-links \ # for local storing
    #--page-requisites \ # for storing images and other 'internal links' in leaf nodes
    #--backup-converted \ # if you rewrite with --html-extension, you back it up with the original extension, to let wget know when it downloaded a page already.
    #--span-hosts \ # allow images, stylesheets and so on from other hosts to be retrieved.
    #--html-extension \ # if page mimetype is html, then this changes .asp or .php to .html. 
    #--timestamping # sets the modified date to the server modified date.

    # let's go back to the root path (in absolute form)
    cd "${abs_path}"

done
