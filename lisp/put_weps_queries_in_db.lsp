#!/home/bkovach1/bin/newlisp

; put_weps_queries_in_db.lsp directory_with_queries

; load the mysql module
(load "/home/bkovach1/share/newlisp/modules/mysql.lsp")

; initialize db connection
(MySQL:init)
(MySQL:connect "qassir" "bogomil" "wieowie" "people")

(define (usage)
     (println "Usage: put_weps_queries_in_db.lsp directory_with_queries")
     (exit))

(define (execute mysql_query)
  ;(println mysql_query))
  (setq res (MySQL:query mysql_query))
  (println res)
  (if (not res) ((println (append "Couldn't execute query " mysql_insert_query "\n") (exit)))))

(setq topdir (main-args 2))
(if (not topdir) (usage))
(if (not (change-dir topdir)) ((println (append "Couldn't open directory " topdir "\n") (exit))))

(define (doquery dirname)
  (setq name (parse dirname "_"))
  (setq fname (name 0))
  (setq lname (name 1))
  (setq mysql_insert_query (append "insert into selected_queries_weps (FirstName, LastName) values ('" fname "','" lname "')"))
  (execute mysql_insert_query))

(setq queries (directory))
(define (procquery x)
  (if (and (directory? x) (!= x "." ) (!= x ".." )) (doquery x) nil))

(map procquery queries)

(exit)
