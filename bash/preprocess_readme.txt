Call the preprocess.sh script with first argument a directory with crawls and
a second argument an empty directory that will hold all the results.

The result directory will contain the following subdirectories:

links_to_crawls_no_extensions
links_to_crawls_with_extensions
crawls_copy
w3mmed_documents
stemmed_documents
matrices

Noscript tags are removed before passing through w3m.
w3mmed_documents will contain the documents after w3mming but before
stemming, stemmed_documents will contain the documents after stemming and
matrices will hold the matrices.

There are three file per query in the matrices subdirectory:
clustering  matrix  similarity_matrix

Clustering is a the final clustering with the history visible by the nestings.
Matrix is the matrix of the tf-idf vectors where the first line are the terms
and the first column are the document ids.
Similarity_matrix is the similarity matrix for the documents which is fed to
the clustering algorithm. 
