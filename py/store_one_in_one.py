"""
Store a one in one baseline run in the db for the given dataset

Usage:
    %s dataset

Where dataset is one of:
    weps1, weps2, wieowie, wieowiesers, wieowieprofiles
"""

import sys
import db
import get_queries


def store_doc_ids_for_weps1(conn):
    pass

def store_doc_ids_for_weps2(conn):
    pass

def store_doc_ids_for_wieowie(conn):
    pass

def store_doc_ids_for_wieowiesers(conn):
    pass

def store_doc_ids_for_wieowieprofiles(conn):
    run_id = store_wieowie_run(conn, 'profiles')
    # line below not good: it may discard ids that for one query were not obtained via a platform but for another query they were obtained througha platform.
    #sql_select = "select query_id, id from search_results where in_weps_corpus='yes' and type in ('hyves','facebook','linkedin','twitter','myspace') group by query_id, id;"
    for query in get_queries.get_queries():
        # hmm, line below still suffers from the same mistake. Probably I did it like this everywhere. Yep, I did. Probably it's sain anyway.
        sql_select = "select distinct id from search_results where query_id = %s and in_weps_corpus='yes' and type in ('hyves', 'facebook','linkedin','twitter','myspace');"
        params_select = (query['idx'],)
        res = db.select(sql_select, params=params_select, conn=conn)
        for rec in res:
            store_wieowie_singleton_cluster(conn, run_id, query['idx'], rec['id'])

def store_wieowie_run(conn, dataset):
    return db.insert_select_last_insert_id("insert into run (name, description) values (%s, '');", params=("one_in_one_%s" % dataset,), conn=conn)

def store_wieowie_singleton_cluster(conn, run_id, query_id, doc_id):
    """
    stores a singleton cluster for a given run_id, query_id and doc_id
    """
    # store cluster
    sql_insert_cluster = "insert into run_cluster (run_id, query_id) values (%s, %s)"
    params_insert_cluster = (run_id, query_id)
    cluster_id = db.insert_select_last_insert_id(sql_insert_cluster, params=params_insert_cluster, conn=conn)
    # store the dac_id in just created cluster, thus creating a singleton cluster for the doc_id:
    sql_insert_doc = "insert into run_clustering (cluster_id, normalized_url_id) values (%s, %s)"
    params_insert_doc = (cluster_id, doc_id)
    db.insert(sql_insert_doc, params=params_insert_doc, conn=conn)

fun_store_doc_ids_for_dataset = {
    'weps1' : store_doc_ids_for_weps1,
    'weps2' : store_doc_ids_for_weps2,
    'wieowie' : store_doc_ids_for_wieowie,
    'wieowiesers' : store_doc_ids_for_wieowiesers,
    'wieowieprofiles' : store_doc_ids_for_wieowieprofiles
}

########## main
if len(sys.argv) != 2 or sys.argv[1] not in fun_store_doc_ids_for_dataset:
    print __doc__ % sys.argv[0]
    sys.exit(2)

conn = db.connect()
fun_store_doc_ids_for_dataset[sys.argv[1]](conn)
conn.close()
