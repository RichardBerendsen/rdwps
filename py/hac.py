#!/usr/bin/python
import sys
import re
import util
from pprint import pprint


def _find_closest(cluster_similarities):
  # had to change it to something negative. It was 0.0 before, but total zero's resulted in an error
  sim = -0.1
  res = ()
  n = len(cluster_similarities)
  for i in range(0, n):
    for j in range(0, i):
      if cluster_similarities[i][j] > sim:
        sim = cluster_similarities[i][j]
        res = (i, j)
  #if len(res) == 0:
  #print cluster_similarities
  return res[0], res[1], sim

def cluster_sim_single_link_max(clustering, cluster1_index, cluster2_index, matrix):
  cl1 = util.linearize(clustering[cluster1_index])
  cl2 = util.linearize(clustering[cluster2_index])
  sim = 0.0
  for el1 in cl1:
    for el2 in cl2:
      if matrix[el1][el2] > sim:
        sim = matrix[el1][el2]
  return sim

def cluster_sim_centroid_max(clustering, cluster1_index, cluster2_index, matrix):
    cl1 = util.linearize(clustering[cluster1_index])
    cl2 = util.linearize(clustering[cluster2_index])
    sim_sum = 0
    for el1 in cl1:
        for el2 in cl2:
            sim_sum += matrix[el1][el2]
    return float(sim_sum) / (len(cl1) * len(cl2))

# an element of the clustering is a list of indices of documents
# actually we put nested lists here just to be able to reconstruct
# the clustering tree later

def _build_cluster_similarity_matrix(clustering, matrix, cluster_sim_funct):
  result = []
  nclusters = len(clustering)
  for i in range(0,nclusters):
    cluster = clustering[i]
    # the similarities to this cluster
    cluster_row = []

    for j in range(0,i):
      similarity = cluster_sim_funct(clustering, i, j, matrix)
      cluster_row.append(similarity)
    result.append(cluster_row)

  #pprint(clustering)
  #pprint(result)
  # now we populate the part of the matrix above the diagonal by symmetry
  for i in range(0, nclusters):
    result[i][i:] = [0]
    for j in range(i+1, nclusters):
      #result[i][j] = result[j][i]
      result[i][len(result[i]):] = [result[j][i]]
  #pprint(result)
  return result

def _merge_clusters(clustering, cluster1_index, cluster2_index, cluster_similarities, matrix, cluster_similarity_funct):
  n = len(clustering)
  merged_cluster=[clustering[cluster1_index], clustering[cluster2_index]]
  new_clustering = []
  for i in range(0,n):
    if i != cluster1_index and i != cluster2_index:
      new_clustering.append(clustering[i])
  new_clustering.append(merged_cluster)

  new_cluster_similarities = []
  how_many_passed = 0    # 0,1,2 depending on how far we have goon in the next loop
  for i in range(0, n):
    if i != cluster1_index and i != cluster2_index:
      new_row = []
      for j in range(0,n):
        if j != cluster1_index and j != cluster2_index:
          new_row.append(cluster_similarities[i][j])
      #new_row.append(cluster_dist_funct(new_clustering,new_clustering[i - how_many_passed], new_clustering[n-2] ,matrix))
      new_row.append(cluster_similarity_funct(new_clustering,i - how_many_passed, n-2 ,matrix))
      new_cluster_similarities.append(new_row)
    else:
      how_many_passed += 1
  return new_clustering, new_cluster_similarities

# checks input for correctness

def _check_matrix(matrix, docids):
  n = len(matrix)
  for m in matrix:
    if len(m) != n:
      raise Exception("Matrix not square.")
  for i in range(0,n):
    if matrix[i][i] < 0.99:
      #raise Exception("Distance to self not zero for element " + str(i))
      raise Exception("Similarity " + str(matrix[i][i]) + " to self not one for element " + str(i))
    for j in range(0,i):
      if matrix[i][j] != matrix[j][i]:
        print matrix[i][j]
        print matrix[j][i]
        raise Exception("Matrix not symmetric at " + str(i) + " " + str(j))
  if len(docids) != n:
    raise Exception("Document names don't match matrix")

# first argument - a list of n lists of size n - this list is the similarity document matrix
# second argument - a list of n strings - these are the names of the documents (normalized urlids)
# third argument - a cluster distance function having the following signature:
#
#    cluster_distance_function(clustering, cluster1_index, cluster2_index, matrix)
#
#      where matrix is the similarity matrxi between documents
#
# fourth argument - a similarity threshold
# fifth argument - a parital clustering
# returns a pair - a cluster with indices and a cluster with labels

def cluster_from_partial(matrix, docids, cluster_similarity_function, thresh, clustering):
  thresh=float(thresh)
  _check_matrix(matrix, docids)
  #clustering = [[i] for i in range(0,len(matrix))]

  while True:
    if len(clustering) == 1:
      return clustering, get_clustering_with_labels(clustering, docids)
    cluster_similarity_matrix = _build_cluster_similarity_matrix(clustering, matrix, cluster_similarity_function)
    i,j,sim = _find_closest(cluster_similarity_matrix)
    #print "AAAA",i,j,sim, thresh
    if (sim < thresh):
      return clustering, get_clustering_with_labels(clustering, docids)
    else:
      clustering, cluster_similarity_matrix = _merge_clusters(clustering, i, j, cluster_similarity_matrix, matrix, cluster_similarity_function)

def cluster(matrix, docids, cluster_similarity_function, thresh):
  return cluster_from_partial(matrix, docids, cluster_similarity_function, thresh, [[i] for i in range(0,len(matrix))])

def cluster_single_link_maximum(matrix, docids, thresh):
  return cluster(matrix, docids, cluster_sim_single_link_max, thresh)

def cluster_centroid_maximum(matrix, docids, thresh):
  return cluster(matrix, docids, cluster_sim_centroid_max, thresh)

def get_clustering_with_labels(clustering, docids):
  string_to_exec = str(clustering)
  #x.replace("'", "")
  #print string_to_exec
  for i in range(0, len(docids)):
    string_to_exec = string_to_exec.replace("["+str(i)+"]", "[\"" +docids[i] +"\"]")
    string_to_exec = string_to_exec.replace( " " + str(i)+",", " \"" +docids[i] +"\",")
    string_to_exec = string_to_exec.replace( "[" + str(i)+",", " [\"" +docids[i] +"\",")
    string_to_exec = string_to_exec.replace( " " + str(i)+"]", " \"" +docids[i] +"\"]")
  #string_to_exec="result=" + string_to_exec
  #print string_to_exec
  #exec(string_to_exec)
  #return result
  return string_to_exec

if __name__=="__main__":
  #sim_matrix=[[1,1,100],[1,1,100], [100,100,1]]
  #labels = ["red", "green", "blue"]
  sim_matrix=[[1.0, 1/20.2, 1/30.5, 1/300.0],\
              [1/20.2, 1.0, 1/120.5, 1/300.0],\
              [1/30.5, 1/120.5, 1.0, 1/300.0],\
              [1/300.0, 1/300.0, 1/300.0, 1.0]]
  labels = ["apple", "pear", "orange", "banana"]

  clustering, labelled_clustering = cluster_single_link_maximum(sim_matrix, labels , 1/100.0)
  print clustering
  print labelled_clustering

  clustering, labelled_clustering = cluster_from_partial(sim_matrix, labels ,cluster_sim_single_link_max, 0.8, [[[0],[3]],[1],[2]])
  print clustering
  print labelled_clustering
  print _build_cluster_similarity_matrix(clustering, sim_matrix, cluster_sim_single_link_max)
