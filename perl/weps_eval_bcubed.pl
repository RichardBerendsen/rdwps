#!/usr/bin/perl -w

use strict;
use warnings;

#my($key_dir,$run_dir)=@ARGV;

my($run_dir,@key_dirs)=@ARGV;


my $ind_results=0;
my $alpha=0.5;

my $no_results=0;
my %truth_files;

for(my $i=0; $i<@key_dirs; $i++) {
    my $key_dir=$key_dirs[$i];
    opendir(K,"$key_dir");
    while(defined(my $file=readdir(K))) {
	next if($file!~/\.xml$/);
	
	$truth_files{"$key_dir/$file"}=1;
    }
    closedir(K);
}

my $new_entity_id=1000000;

my %test_files;
opendir(T,"$run_dir");
while(defined(my $file=readdir(T))) {
    next if($file!~/\.xml$/);

    $test_files{$file}=1;
}
closedir(T);


my $avg_prec_bcubed=0;
my $avg_rec_bcubed=0;
my $avg_fscore_bcubed=0;
my $no_files=0;

foreach my $file (keys %truth_files) {
#    $no_files++;
#    print STDERR "file=$file\n";

    my %truth_cluster_of_doc;
    my %truth_docs_in_cluster;
    my %test_cluster_of_doc;
    my %test_docs_in_cluster;


#    open(F,"<$key_dir/$file");
    open(F,"<$file");
    my $text='';
    while(defined(my $line=<F>)) {
	chomp($line);
	$text.=" $line";
    }
    close(F);

#    print STDERR "text=$text\n";
    my %discarded;

    if($text=~/<discarded[^>]*>(.*?)<\/discarded>/) {
	my $doc_string=$1;

	while($doc_string=~s/<doc rank=\"([^\"]+)\"[^>]*>//) {
	    my $doc_id=$1;
	    $discarded{$doc_id}=1;
#	    print STDERR "discarded{$doc_id}\n";
	}
    }
    
    while($text=~s/<entity id=\"([^\"]+)\"[^>]*>(.*?)<\/entity>//) {
	my $entity_id=$1;
	my $doc_string=$2;
	while($doc_string=~s/<doc rank=\"([^\"]+)\"[^>]*>//) {
	    my $doc_id=$1;
	    $truth_cluster_of_doc{$doc_id}=$entity_id;
	    $truth_docs_in_cluster{$entity_id}{$doc_id}=1;
	}
    }

    while($text=~s/<entity>(.*?)<\/entity>//) {
	my $doc_string=$1;
	my $entity_id=$new_entity_id;
	$new_entity_id++;
	while($doc_string=~s/<doc rank=\"([^\"]+)\"[^>]*>//) {
	    my $doc_id=$1;
	    $truth_cluster_of_doc{$doc_id}=$entity_id;
	    $truth_docs_in_cluster{$entity_id}{$doc_id}=1;
	}
    }


    my $run_file=$file;
    $run_file=~s/^.*\/([^\/]+)$/$1/;
    $run_file=~s/\.clust\././;

    if(!(-e "$run_dir/$run_file")) {
	print STDERR "File $run_dir/$run_file does not exist.\n";
	next;
    }

    $no_files++;

    open(F,"<$run_dir/$run_file");
    $text='';
    while(defined(my $line=<F>)) {
	chomp($line);
	$text.=" $line";
    }
    close(F);

    while($text=~s/<entity id=\"([^\"]+)\"[^>]*>(.*?)<\/entity>//) {
	my $entity_id=$1;
	my $doc_string=$2;
	while($doc_string=~s/<doc rank=\"([^\"]+)\"[^>]*>//) {
	    my $doc_id=$1;

	    next if(defined($discarded{$doc_id}));
	    next if(!defined($truth_cluster_of_doc{$doc_id}));

	    $test_cluster_of_doc{$doc_id}=$entity_id;
	    $test_docs_in_cluster{$entity_id}{$doc_id}=1;
	}
    }


    my $thrash_cluster_id=12345567890;
    foreach my $doc_id (keys %truth_cluster_of_doc) {
	if(!exists($test_cluster_of_doc{$doc_id})) {
	    $test_cluster_of_doc{$doc_id}=$thrash_cluster_id;
	    $test_docs_in_cluster{$thrash_cluster_id}{$doc_id}=1;
	}
    }

    my $avg=0;
    my $c=0;
    foreach my $e (keys %test_cluster_of_doc) {
	$c++;
	my $avg1=0;
	my $c1=0;

	foreach my $e1 (keys %{ $test_docs_in_cluster{$test_cluster_of_doc{$e}} }) {
	    $c1++;
#	    print STDERR "correctness($e,$e1,...)=",&correctness($e,$e1,\%truth_cluster_of_doc,\%test_cluster_of_doc) ,"\n";
	    $avg1+=&correctness($e,$e1,\%truth_cluster_of_doc,\%test_cluster_of_doc);	    
	}

	if($c1>0) {
	    $avg1/=$c1;
	} else {
	    $avg1=0;
	}

	$avg+=$avg1;
    }

    if($c>0) {
	$avg/=$c;
    } else {
	$avg=0;
    }

    my $precision_bcubed=$avg;

    $avg=0;
    $c=0;
    foreach my $e (keys %truth_cluster_of_doc) {
	$c++;
	my $avg1=0;
	my $c1=0;

	foreach my $e1 (keys %{ $truth_docs_in_cluster{$truth_cluster_of_doc{$e}} }) {
	    $c1++;
#	    print STDERR "correctness($e,$e1,...)=",&correctness($e,$e1,\%truth_cluster_of_doc,\%test_cluster_of_doc) ,"\n";
	    $avg1+=&correctness($e,$e1,\%truth_cluster_of_doc,\%test_cluster_of_doc);	    
	}

	if($c1>0) {
	    $avg1/=$c1;
	} else {
	    $avg1=0;
	}

	$avg+=$avg1;
    }

    if($c>0) {
	$avg/=$c;
    } else {
	$avg=0;
    }

    my $recall_bcubed=$avg;

    my $fscore=1/($alpha*(1/$precision_bcubed)+(1-$alpha)*(1/$recall_bcubed));


    $avg_prec_bcubed+=$precision_bcubed;
    $avg_rec_bcubed+=$recall_bcubed;
    $avg_fscore_bcubed+=$fscore;
    

    if($ind_results) {
	printf "$file: PrecBCubed=%.3f RecBCubed=%.3f F-Score(alpha=$alpha)=%.3f\n", $precision_bcubed,$recall_bcubed,$fscore;
    }
}

$avg_prec_bcubed/=$no_files;
$avg_rec_bcubed/=$no_files;
$avg_fscore_bcubed/=$no_files;

print "Avg. Prec-BCubed=$avg_prec_bcubed\n";	
print "Avg. Rec-BCubed=$avg_rec_bcubed\n";	
print "Avg. F-Score(alpha=$alpha)=$avg_fscore_bcubed\n";	

sub correctness {
    my($e1,$e2,$truth_cluster_of_doc,$test_cluster_of_doc)=@_;

#    print STDERR "truth_cluster_of_doc{$e1}=$$truth_cluster_of_doc{$e1}\n";
#    print STDERR "truth_cluster_of_doc{$e2}=$$truth_cluster_of_doc{$e2}\n";
#    print STDERR "test_cluster_of_doc{$e1}=$$test_cluster_of_doc{$e1}\n";
#    print STDERR "test_cluster_of_doc{$e2}=$$test_cluster_of_doc{$e2}\n";

    if($$truth_cluster_of_doc{$e1} eq $$truth_cluster_of_doc{$e2}
       && $$test_cluster_of_doc{$e1} eq $$test_cluster_of_doc{$e2}) {
	return 1;
    } elsif($$truth_cluster_of_doc{$e1} ne $$truth_cluster_of_doc{$e2}
       && $$test_cluster_of_doc{$e1} ne $$test_cluster_of_doc{$e2}) {
	return 1;
    } else {
	return 0;
    }
}




