import connect
import sys
import os


def get_sessions(conn):
    cursor = conn.cursor()
    cursor.execute("select distinct SessionID from Searches")
    res = cursor.fetchall()
    cursor.close()
    return res

def get_actions_ordered_by_time_asc(conn, session):
    cursor = conn.cursor()
    sql = """(select Time as time, "search" as type, "" as url, "" as platform \
from Searches \
where SessionID = %s) \
union \
(select c.Time, "click", c.URL, c.type \
from Searches as s, Clickouts as c \
where \
s.SessionID = %s and s.SearchID = c.SearchID \
order by time asc)"""
    cursor.execute(sql, (session['SessionID'], session['SessionID']))
    res = cursor.fetchall()
    cursor.close()
    return res

def plus_is(dict, key, add):
    if key in dict:
        dict[key] += add
    else:
        dict[key] = add

def main(args):

    conn = connect.connect()

    bins = {} # nr of seconds, count (finest grained level)

    for session in get_sessions(conn):
        actions = get_actions_ordered_by_time_asc(conn, session)
        if len(actions) > 1:
            for i in range(1, len(actions)):
                plus_is(bins, (actions[i]['time'] - actions[i - 1]['time']).seconds, 1)

    conn.close()

    for key in sorted(bins.iterkeys()):
        print "%d\t%d" % (key, bins[key])


if __name__ == "__main__":
    sys.exit(main(sys.argv))
