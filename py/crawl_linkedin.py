#!/usr/bin/python

from BeautifulSoup import BeautifulSoup
import urllib # for quote function
import urllib2 # for urlopen
import urlparse # to split the url, and urlencode only path part.

import get_queries
import sys
import time
from pprint import pprint
from util import normalize
from get_queries import get_queries
from store_urls import store_urls
import random
import time

def _query_url(firstname, lastname):
    # This URL was created from the URL that wieowie.nl offers
    # when you click on 'more linkedin results'
    firstname = firstname.replace('"', '').strip()
    lastname = lastname.replace('"', '').strip()
    words = lastname.split()
    query_string = '+'.join(words)
    query_string = firstname + "/" + query_string
    url = "http://nl.linkedin.com/pub/dir/" \
        + query_string
    return url

def _get_page(url):
    return urllib2.urlopen(url).read()

# keep search results in case we would want to get the snippets later.  to
# retrieve these, we would only need the query_id and the rank of each search
# result.
def _store_linkedin_search_result_html(page, query_id):
    try:
        f = open('../../exp/linkedin_search_results_per_query/%s.html' % (query_id,), 'w')
    except IOError, e:
        sys.stderr.write('Error: Exception caught \
while storing linkedin search results html page\n')
        sys.stderr.write(str(e) + '\n')
        sys.exit(2)
    else:
        f.write(page)
        f.close()

def _parse_search_results(page):
    res = []
    soup = BeautifulSoup(page)
    #f = open("page", "w")
    #f.write(soup.prettify())
    #f.close()

    #ts = soup.findAll(lambda t: t.name=="a" and t.parent.name=="li" and t.parent.name=="ul" and t["class"]=="fn url beacon" and t.has_key("href"))
    ts = soup.findAll(lambda t: t.name=="a" and t.parent.name=="strong"  and t.parent.parent.name=="h2" and t.has_key("title") and t.has_key("href"))
    return [ t['href'] for t in ts ]

def _fetch_urls(firstname, lastname, query_id):
    query_url = _query_url(firstname, lastname)
    print query_url
    try:
        page = _get_page(query_url)
    except urllib2.HTTPError, e:
        err = "ERROR: %s -- %s\n" % (e.code, e.read())
        print err
        if e.code == 404:
            print "Returning no urls!!!"
            return []
    else:
        _store_linkedin_search_result_html(page, query_id)
        urls = _parse_search_results(page)
        return urls

def _main():
    print "Starting linkedin crawling..."
    print "############################"
    print "############################"

    queries = get_queries()
    queries = queries[:23]
    print queries
    for query in queries:
      firstname = query['FirstName']
      lastname = query['LastName']
      urls = _fetch_urls(firstname, lastname, query['idx'])
      n_urls=[normalize(u) for u in urls]
      #pprint(n_urls)
      store_urls(firstname, lastname, "linkedin", urls[:50], n_urls[:50])
      time.sleep(167 + random.randrange(1,6,1))

if __name__ == "__main__":
    _main()
