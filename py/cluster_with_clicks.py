"""
Cluster documents based on 'co-dwelling' evidence obtained  from wieowie log files.

Usage: %s out_dir 

and in outdir the resulting partial clusterings will be stored, as well as the run.conf 
file that allows us to see what is going on.
"""

import db
import operator
import idiom
import sys
import hac
import util
import store_run
from optparse import OptionParser
import get_queries
import cluster_helper



def _get_actions(conn, conf, query, session):

    # depending on conf['group_actions_by'] we have a UserID, a SessionID or a SearchID in session
    # the actions we return are always just searches and clicks.
    # we also include 'more google results' like links, since these tell us that in fact no other
    # search result completely satisfied the information need for this platform, so that means no
    # other should probably be a clear winner, unless really a lot of dwelling time went there.

    # NB: this query will break if lastname or firstname contains single quotes.
    # However, we refrain from the nice sql preprocessing that cursor.execute does here because we
    # need to pass in a column name automatically and cursor.execute quotes strings.
    # this leaves us to escape mysql characters ourselves but we don't do this here because there
    # are no single quotes or other funky in our 120 names.
    # NB2: we don't quote the %s corresponding to the value of the
    # automatically inserted column name because the type of this column may be
    # either an int or  a string. So depending on this type, we quote the inserted value instead.
    def quote_if_str(str, type):
        if type == "str":
            return "'%s'" % str
        return str

    cursor = conn.cursor()
    sql = """(select Time as time, "search" as type, "" as url, 0 as urlid, "" as platform \
from Searches \
where `%s` = %s and FirstName = '%s' and LastName = '%s') \
union \
(select c.Time, "click", c.URL, c.urlid, c.type \
from Searches as s, selected_queries_clicks as c \
where \
s.%s = %s and s.SearchID = c.SearchID \
and s.FirstName = '%s' and s.LastName = '%s') \
order by Time asc""" % (conf['group_actions_by']['column'], \
quote_if_str(session['id'], conf['group_actions_by']['type']), \
query['FirstName'], query['LastName'], conf['group_actions_by']['column'], \
quote_if_str(session['id'], conf['group_actions_by']['type']), query['FirstName'], query['LastName']);
    cursor.execute(sql)
    actions = cursor.fetchall()
    cursor.close()
    return actions
    cursor.close()

# assume each action has a key 'dwelling_time'
def _get_dwelling_time_per_platform_per_url(conf, actions):
    ret = {}
    # only use dwelling times on docs clicked from the search results
    clicks = [a for a in actions if a['type'] == 'click']
    for action in clicks:
        if ret.has_key(action['platform']):
            if ret[action['platform']].has_key(action['url']):
                ret[action['platform']][action['url']] = (action['dwelling_time'] + \
                        ret[action['platform']][action['url']][0], action['urlid'])
            else:
                ret[action['platform']][action['url']] = (action['dwelling_time'], action['urlid'])
        else:
            ret[action['platform']] = {action['url'] : (action['dwelling_time'], action['urlid'])}
    return ret

# will add a key 'dwelling_time' to each action dict in actions 
def _set_dwelling_times(conf, actions):
    if len(actions) > 1:
        for idx in range(1, len(actions)):
            actions[idx - 1]['dwelling_time'] = (actions[idx]['time'] - actions[idx - 1]['time']).seconds
    if len(actions) >=1:
        actions[-1]['dwelling_time'] = conf['last_action_dwelling_time']

# compute 'clear winners' for each platform
# a clear win is if the ratio dwelling time on one URL over total dwelling time
# on URLs of the same type is bigger than conf['winning_ratio_of_dwelling_time']
# in addition, a clear winner has to have a non zero urlid to be of use: this urlid
# matches it to the search result urlids that we are trying to cluster.
# if results from two different platform have the same urlid, what do we do?
# first note that this will not normally be the case in social media platforms.
# but in regular search engine results it may be common.
# should the dwelling times be summed together? Note also that it is rather redundant for
# a user to click identical search results in both platforms.  Therefore, for the sake
# of simplicity, let's not add anything together here, and base our decision on dwelling
# times per platform.
# Second, should we use some kind of minimal threshold dwelling time? If I have a dwelling
# time of one second on just one Hyves profile before I click through to the Facebook profiles,
# I will still have a ratio of 1 for that Hyves profile, and it is classified as a clear winner.
# Therefore, I think we should put a threshold on it. But let's save that for later experiments,
# first do it a bit rough so that we get some decisions out of it.
def _get_clear_winners(conf, dwelling_times):
    winners = {}
    for platform_key, platform_dict in dwelling_times.iteritems():
        if platform_key in conf['platforms']:
            url_with_max_dwelling_time = ''
            urlid_with_max_dwelling_time = 0
            max_dwelling_time = 0
            total_dwelling_time = 0
            for url, (dwelling_time, urlid) in platform_dict.iteritems():
                total_dwelling_time += dwelling_time
                if dwelling_time > max_dwelling_time:
                    url_with_max_dwelling_time = url
                    urlid_with_max_dwelling_time = urlid
                    max_dwelling_time = dwelling_time
            if total_dwelling_time != 0:
                ratio = (max_dwelling_time * 1.0) / total_dwelling_time
            else:
                ratio = 0
            if ratio > conf['winning_ratio_of_dwelling_time']:
                if urlid_with_max_dwelling_time is not None and urlid_with_max_dwelling_time != 0:
                    winners[platform_key] = urlid_with_max_dwelling_time
    #print >> sys.stderr, "Winners this session: %s" % winners
    return winners

def _find_co_dwelled_in_session(conn, conf, query, session):
    # for each platform find if there is a 'clear winner':
    # a profile/search result that got most of the dwelling time.
    actions = _get_actions(conn, conf, query, session)
    _set_dwelling_times(conf, actions)
    dwelling_times_per_platform_per_url = _get_dwelling_time_per_platform_per_url(conf, actions)
    winners = _get_clear_winners(conf, dwelling_times_per_platform_per_url)

    # make and return a list of unique winning url ids here. 
    winning_urlids = set()
    for key, urlid in winners.iteritems():
        winning_urlids.add(urlid)
    return [key for key in winning_urlids]

def _find_sessions_with_cross_platform_coclicks(conn, conf, query):
    cursor = conn.cursor()
    sql = "select distinct convert(`%s`, char) as id from selected_queries_clicks where FirstName = '%s' and LastName = '%s'" % \
            (conf['group_actions_by']['column'], query['FirstName'], query['LastName'])
    cursor.execute (sql, )
    res = cursor.fetchall()
    cursor.close()
    return res



def _find_co_dwelled(conn, conf, query):
    # build a similarity matrix for urlids, urlids should be added as they come in.
    # store it as a hash table
    co_dwells = {}

    sessions = _find_sessions_with_cross_platform_coclicks(conn, conf, query)
    print >> sys.stderr, "number of sessions: %s" % len(sessions)

    for session in sessions:
        co_dwelled_urlids = _find_co_dwelled_in_session(conn, conf, query, session)
        co_dwelled_urlids.sort() # ensures that co_dwells will hold each tuple as a key in only one ordering.
        for i in range(0,len(co_dwelled_urlids)):
            for j in range(i + 1, len(co_dwelled_urlids)):
                key = (co_dwelled_urlids[i], co_dwelled_urlids[j])
                if co_dwells.has_key(key):
                    co_dwells[key] += 1
                else:
                    co_dwells[key] = 1

    # now use the minimum number of co_dwells to produce the similarity matrix (a binary one)
    sim = set([]) # set
    for key, val in co_dwells.iteritems():
        if val >= conf['min_sessions_with_codwelling']:
            sim.add(key)

    # for debugging, let's print this similarity matrix
    print >> sys.stderr, "Similarity matrix: %s" % sim
    return sim


# returns run_id of just stored run
def _store_run(conn, conf, out_dir):
    name = "co_dwelled_grouped_by_%s" % (conf['group_actions_by']['column'],)
    description = '\n'.join(["%s : %s" % (key, value) for key, value in conf.iteritems()])
    params = (name, description)
    return cluster_helper.store_run_and_get_run_id(name, description, out_dir, conn=conn)

def _store_clustering(conn, conf, options, run_id, query, out_dir, query_dir):
    co_dwelled = _find_co_dwelled(conn, conf, query)
    # we coulds (ab)use this similarity matrix to implement single link clustering by hand.
    # just group all documents that are connected by ones to each other in a cluster. 
    # Or, equivalently: find connected components in the graph where edges corresponds to ones in the similarity matrix.
    # but we could also, to get a feel for bogomil his code, play with hac.py. It should have no problem with
    # this matrix.
    # prepare input for hac.py
    labels, sim_matrix  = cluster_helper.expand_sim_set(co_dwelled)
    print >> sys.stderr, "Labels: %s\nExpanded similarity matrix:%s" % (labels, sim_matrix)
    if sim_matrix:
        clustering, clustering_with_labels = hac.cluster_single_link_maximum(sim_matrix, labels, 1.0)
        print >> sys.stderr, "Clustering: %s\nClustering with labels: %s" % (clustering, clustering_with_labels)
        if not options.dry_run:
            store_run.store_clustering_in_db(clustering_with_labels, run_id, query['idx'])
            cluster_helper.write_clustering_to_disk(clustering_with_labels, out_dir, query_dir)

def _main(args):
    progname = args[0]
    parser = OptionParser(usage=__doc__ % progname)
    parser.add_option("-d", "--dry-run", dest="dry_run", action="store_true", default=False)
    parser.add_option("-f", "--query_id_from", dest="query_id_from", action="store", type="int", default=3)
    parser.add_option("-t", "--query_id_to", dest="query_id_to", action="store", type="int", default=122)
    parser.add_option("-q", "--query_result_set", dest="query_result_set", action="store", type="choice", choices=['all','socialmedia','web'], default="all")
    options, args = parser.parse_args()

    if len(args) != 1:
        parser.print_help()
        return 2

    out_dir = args[0].rstrip('/')
    out_dir_ls = idiom.ls_dir_or_exit(out_dir)
    if out_dir_ls:
        print >> sys.stderr, "Error: output directory %s not empty" % (out_dir,)
        return 2

    conf = {
        "group_actions_by": {"column":"UserID", "type":"str"},
        #"group_actions_by": {"column":"SessionID", "type":"int"},
        #"group_actions_by": {"column":"SearchID", "type":"str"},
        "last_action_dwelling_time" : 60, # seconds
        "winning_ratio_of_dwelling_time" : 0.7,
        "min_sessions_with_codwelling" : 2,
    }
    if options.query_result_set == "all":
        conf["platforms"] = set(["hyves", "facebook", "linkedin", "twitter", "myspace", "google", "yahoo", "bing"])
    elif options.query_result_set == "socialmedia":
        conf["platforms"] = set(["hyves", "facebook", "linkedin", "twitter", "myspace"])
    elif options.query_result_set == "web":
        conf["platforms"] = set(["google", "yahoo", "bing"])

    conn = db.connect()
    if not options.dry_run:
        run_id = _store_run(conn, conf, out_dir)
    queries = get_queries.get_queries(options.query_id_from, options.query_id_to)
    for query in queries:
        query_dir = util.get_dirname_from_query(query)
        _store_clustering(conn, conf, options, run_id, query, out_dir, query_dir)
    conn.close()

if __name__ == "__main__":
    sys.exit(_main(sys.argv))
