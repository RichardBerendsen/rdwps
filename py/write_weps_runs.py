"""write runs stored in the database to xml

Usage: python %s empty_dir which_weps [run1 run2 ...]

Will store one dir for each run in empty_dir. Inside each run dir, will store xml file for each query in WePS-2 format.
which_weps should be 1 or 2 or 3, to select the right runs on the right queries.

run1 run2 .. are names of runs as stored in the database. If you don't specify any, will store all as xml.

optional arguments: 
    --query_id_from X, __query_id_to Y
"""

import sys
import os
from optparse import OptionParser
import db

def _get_queries_weps(conn, which_weps, query_id_from, query_id_to):
    cursor = conn.cursor()
    cursor.execute("select * from selected_queries_weps where annotated = 'yes' and which_weps = %s and idx >= %s and idx <= %s" % (which_weps, query_id_from, query_id_to))
    res = cursor.fetchall()
    cursor.close()
    return res

def _query_xml_filename(firstname, lastname, which_weps):
    firstname = firstname.replace('"', '').strip()
    lastname = lastname.replace('"', '').strip()
    words = firstname.split() + lastname.split()
    query_string = '_'.join(words)
    if which_weps == 2:
        query_string = query_string.upper()
    filename = query_string + ".clust.xml"
    return filename

def _get_entities(conn, run, query):
    cursor = conn.cursor()
    cursor.execute("select * from run_cluster_weps where run_id = %s and query_id = %s order by id asc", (run['id'], query['idx']))
    res = cursor.fetchall()
    cursor.close()
    return res

def _get_normalized_url_ids(query, cluster, conn):
    cursor = conn.cursor()
    cursor.execute("select normalized_url_id as id from run_clustering_weps where cluster_id = %s", (cluster['id'],))
    res = cursor.fetchall()
    cursor.close()
    return res

def _get_xml_doc(doc):
    xml = "\t\t<doc rank=\"%s\"/>\n" % doc['id']
    return xml

def _get_xml_entity(run, query, cluster, conn):
    docs = [_get_xml_doc(doc) for doc in _get_normalized_url_ids(query, cluster, conn)]
    xml = ""
    if docs != []:
        xml += "\t<entity id =\"%s\">\n" % cluster['id']
        xml += "".join(docs)
        xml += "\t</entity>\n"
    return xml

def _get_xml_entities(conn, run, query):
    entities = [_get_xml_entity(run, query, entity, conn) for entity in _get_entities(conn, run, query)]
    xml = "".join(entities)
    return xml

def _get_xml_uniques(conn, run, query):
    """Since we may not store uniques (unclustered documents) in the database, \
    we retrieve them here:
    """
    sql_create = "create temporary table if not exists clustered_docs_weps (id int);"
    sql_insert = "insert into clustered_docs_weps (id) select distinct r2.normalized_url_id from run_cluster_weps as r1 inner join run_clustering_weps as r2 on r1.id = r2.cluster_id where r1.run_id = %s and r1.query_id = %s;"
    params_insert = (run['id'], query['idx'])
    sql_select = """
    select distinct s.doc_id
    from search_results_weps as s 
    left outer join clustered_docs_weps as r
    on s.doc_id = r.id
    where s.query_id = %s 
    and which_weps = %s
    and r.id is null
    """
    params_select = (query['idx'], query['which_weps'])
    sql_drop = "drop table clustered_docs_weps"
    db.insert(sql_create, conn=conn)
    db.insert(sql_insert, params=params_insert, conn=conn)
    res = db.select(sql_select, params_select, conn)
    db.insert(sql_drop, conn=conn)
    xml = ""
    for rec in res:
        xml += "\t<entity id = \"doc_%s\">\n" % (rec['doc_id'],)
        xml += "\t\t<doc rank=\"%s\"/>\n" % rec['doc_id']
        xml += "\t</entity>\n"
    return xml

def _write_clustering(conn, run, query, run_dir, which_weps):
    xml_entities = _get_xml_entities(conn, run, query)
    xml_uniques = _get_xml_uniques(conn, run, query)
    if not (xml_entities == "" and xml_uniques == ""):
        xml = "<clustering>\n" + xml_entities + xml_uniques + "</clustering>\n"

        basename = _query_xml_filename(query['FirstName'], query['LastName'], which_weps)
        f = open("%s/%s" % (run_dir, basename), 'w')
        f.write(xml)
        f.close()

def _get_runs(conn, which_weps, runs):
    if len(runs): # user supplied run ids, let's select only these from the db.
        str_runs = "(" + ",".join(runs) + ")" # vulnerable to sql injection, but just take care with your arguments.
        sql = "select * from run_weps where which_weps = %%s and id in %s" % str_runs
        print >> sys.stderr, sql
        params = (which_weps)
    else: # just match all runs.
        sql = "select * from run_weps where which_weps = %s"
        params = (which_weps)
    return db.select(sql, params, conn)

def _is_empty_dir(dir):
    try:
        ls = os.listdir(dir)
    except OSError:
        return False
    else:
        return ls == []

def main(argv):
    parser = OptionParser()
    parser.add_option("-f", "--query_id_from", dest="query_id_from", action="store", type="int", default=1)
    parser.add_option("-t", "--query_id_to", dest="query_id_to", action="store", type="int", default=10000)
    options, args = parser.parse_args()

    if len(args) < 2 or not _is_empty_dir(args[0]) or not int(args[1]) or int(args[1]) > 3 or int(args[1]) < 1:
        print >> sys.stderr, __doc__ % sys.argv[0]
        return 2
    dir = args[0].rstrip('/')
    which_weps = int(args[1])

    conn = db.connect()
    for run in _get_runs(conn, which_weps, args[2:]):
        run_dir = "%s/%s_%s" % (dir, run['id'], run['name'])
        try:
            os.mkdir(run_dir)
        except OSError:
            print >> sys.stderr, "Could not make directory" % run_dir
            return 3
        else:
            for q in _get_queries_weps(conn, which_weps, options.query_id_from, options.query_id_to):
                _write_clustering(conn, run, q, run_dir, which_weps)
    conn.close()
    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
