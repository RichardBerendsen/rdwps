"""
Checks if all ids that are in the corpus are also in the gold truth, annotated and well.

Usage:

%s dir_gold_truth

Where dir_gold_truth is the directory in which the gold truth files will be written
"""
import get_queries
import sys
import db
from optparse import OptionParser

def _check_gold_truth(conn, options, query, annotator, dir):
    sql = """select distinct id from search_results where query_id = %s and in_weps_corpus = 'yes'\
and id not in (select distinct c2.normalized_url_id from cluster as c1, \
clustering as c2 where c1.author = %s and c1.id = c2.cluster_id)"""
    params = (query['idx'], annotator)
    res = db.select(sql, params, conn)
    print "%s:\t%s %s: %s" % (query['idx'],query['FirstName'],query['LastName'], ','.join(["%s" % rec['id'] for rec in res]))

def get_annotator(conn, query):
    annotated_by = ''
    cursor = conn.cursor()
    sql = "select count(*) as n from cluster where query_id = %s and author = %s"
    richard = db.select_unique(sql, (query['idx'], 'richard'))
    if richard['n']:
        annotated_by = 'richard'
    else:
        thomas = db.select_unique(sql, (query['idx'], 'great_annotator'))
        if thomas['n']:
            annotated_by = 'great_annotator'
        else:
            bogomil = db.select_unique(sql, (query['idx'], 'bogomil'))
            if bogomil['n']:
                annotated_by = 'bogomil'
    cursor.close()
    return annotated_by

def main(argv):
    parser = OptionParser(usage=__doc__ % argv[0])
    parser.add_option("-f", "--query_id_from", dest="query_id_from", action="store", type="int", default=3)
    parser.add_option("-t", "--query_id_to", dest="query_id_to", action="store", type="int", default=45)
    parser.add_option("-s", "--dataset", dest="dataset", action="store", type="choice", choices=["SERs", "Profiles", "both"], default="both")
    options, args = parser.parse_args()

    queries = get_queries.get_queries(options.query_id_from, options.query_id_to)

    conn = db.connect()
    for q in queries:
        annotator = get_annotator(conn, q)
        if annotator != "":
            _check_gold_truth(conn, options, q, annotator, dir)

    conn.close()

if __name__ == "__main__":
    sys.exit(main(sys.argv))
