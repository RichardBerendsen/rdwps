#!/usr/bin/python

import serializer
import store_run
import cluster_helper
import sys
import os
import util
import db

usage = "Usage: script input_dir name description"

if len(sys.argv) < 4:
  print usage
  sys.exit(2)

input_dir = sys.argv[1]
name = sys.argv[2]
description = sys.argv[3]

conn = db.connect()

runid = cluster_helper.store_run_and_get_run_id(sys.argv[2], sys.argv[3], input_dir, conn=conn)

qs = os.listdir(sys.argv[1])
for q in qs:
  if os.path.isdir(input_dir + "/" + q):
    print q
    qid = util.get_query_from_dirname(conn, q)['idx']
    clustering = serializer.deserialize_clustering_to_top_level_only(input_dir + "/" + q + "/clustering")
    #first_level_clustering = util.string_array_extract_first_level(clustering.__repr__())
    #print first_level_clustering
    store_run.store_clustering_in_db(clustering.__repr__(), runid, qid)
conn.close()
