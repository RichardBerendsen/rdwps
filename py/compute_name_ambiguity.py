"""
computes name ambiguity according to us cencus data.

Accepts format:

name;male_first_name_proportion;female_first_name_proportion;family_name proportion.

We assume the data comes from the US Cencus 1990 dataset
"""

import sys
import os

sample_size_all = 6290251.0
sample_size_men = 3184399.0
sample_size_women = 3003954.0

proportion_men = sample_size_men / (sample_size_men + sample_size_women)
proportion_women = sample_size_women  / (sample_size_men + sample_size_women)

lines = sys.stdin.readlines()
records = [line.strip().split(';') for line in lines]

# indexes for proportion array
male_first_name = 0
female_first_name = 1
family_name = 2

# we use add n smoothing:
add_n = 3.0

# proportion is interpreted as probability. 
# Drawing first and last names is modeled as independent.
for record in records:
    if len(record) != 4:
        print >> sys.stderr, """
        Error, there should be four fields, showing wrong record:
        %s
        """ % record
        sys.exit(2)

    proportions = record[1:]
    if proportions[male_first_name] == "":
        proportions[male_first_name] = 0.0
    if proportions[female_first_name] == "":
        proportions[female_first_name] = 0.0
    if proportions[family_name] == "":
        proportions[family_name] = 0.0
    for i in range(len(proportions)):
        proportions[i] = float(proportions[i])
    if proportions[male_first_name] == 0.0:
        proportions[male_first_name] = add_n / sample_size_men
    if proportions[female_first_name] == 0.0:
        proportions[female_first_name] = add_n / sample_size_women
    if proportions[family_name] == 0.0:
        proportions[family_name] = add_n / sample_size_all
    weighted_male = proportions[male_first_name] * proportion_men
    weighted_female = proportions[female_first_name] * proportion_women
    p_name = (weighted_male + weighted_female) * proportions[family_name]

    print "%s;%f" % (record[0], p_name)
