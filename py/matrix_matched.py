#!/usr/bin/python

import get_queries
from util import normalize
from pprint import pprint
import sys

def _connect():
  return get_queries._connect().cursor()

  
# aggregate_over is SessionID, SearchID or UserID
def _get_aggregator_ids(FirstName, LastName, aggregate_over, cursor):
  cursor.execute("select distinct(" + aggregate_over + ") from pilot_train_val_test_clicks where FirstName=%s and LastName=%s and urlid != 0", (FirstName, LastName))
  res = cursor.fetchall()
  aggids = [x[aggregate_over] for x in res]
  #print "$$$$$"
  #print res
  return aggids
  
## get one non-normalized version of the url  
def get_url(urlid, cursor):
  cursor.execute("select url from search_results where id=%s limit 1", (urlid, ))
  res = cursor.fetchone()
  return res['url']

def get_urlids(FirstName, LastName):
  res = []
  cursor = _connect()
  cursor.execute("select distinct(urlid) from pilot_train_val_test_clicks where FirstName=%s and LastName=%s and urlid != 0 ", (FirstName, LastName))
  res = cursor.fetchall()
  urlids = [x['urlid'] for x in res]
  return urlids
  #return _get_unique(urlids)

def _get_coclicks(FirstName, LastName, urlid1, urlid2, aggregate_over, cursor):
  coclicks = 0
  aggr_ids = _get_aggregator_ids(FirstName, LastName, aggregate_over, cursor)  
  for aggr_id in aggr_ids:
    cursor.execute("select urlid from pilot_train_val_test_clicks where FirstName=%s and LastName=%s and " + aggregate_over + "=%s and urlid != 0", (FirstName, LastName, aggr_id))
    res = cursor.fetchall()
    all_url_ids = [x['urlid'] for x in res]
    if urlid1 in all_url_ids and urlid2 in all_url_ids:
      coclicks += 1
  return coclicks

def product(FirstName, LastName, urlids, aggregate_over, out):
  cursor = _connect()
  for u in urlids:
    out.write("\n")
    for v in urlids:
      if u != v:
        #print u
        #print v
        out.write(str(_get_coclicks(FirstName, LastName, u, v, aggregate_over, cursor)) + " ")
        #_get_coclicks(FirstName, LastName, u, v, cursor)
      else:
        out.write("# ")
  
  for u in urlids:
    url = get_url(u, cursor)
    out.write("\n" + str(u) + "  " + url)
  out.write("\n")
  cursor.close()
  
def _get_unique(somelist):
  res = []
  for l in somelist:
    if l not in res:
      res.append(l)
  return res
  
if __name__ == "__main__":
  #res = get_urlids('Geert', "Wilders")
  #print res
  #product('Geert', "Wilders", res, "SessionID", sys.stdout)
  
  out_dir = sys.argv[1]
  aggregate_over = sys.argv[2]
  if (aggregate_over not in ["UserID", "SearchID", "SessionID"]):
    raise "Second argument should be UserID SearchID or SessionID"
  
  print "Outputting to:" + out_dir
  queries = get_queries.get_queries()
  for q in queries:
    res = get_urlids(q['FirstName'], q['LastName'])
    filename = q['FirstName'] + "_" + q['LastName']
    filename = filename.replace(" ", "_")
    filename = filename.replace("\"", "#")
    print filename
    out = open(out_dir + "/" + filename, "w")
    product(q['FirstName'], q['LastName'], res, aggregate_over, out)
    out.close()
    
  #res = get_normalized_urls("Wendy", "\"de Wit\"")
  #pprint(len(res))
  #product("Wendy", "\"de Wit\"", res, sys.stdout)