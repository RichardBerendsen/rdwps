
from get_queries import get_queries
import sys

def _query_url(firstname, lastname):
    firstname = firstname.replace('"', '').strip()
    lastname = lastname.replace('"', '').strip()
    words = firstname.split() + lastname.split()
    query_string = '+'.join(words)
    url = "http://www.google.nl/search?num=50&oe=utf8&ie=utf8&source=uds&start=0&lr=lang_&hl=en&q=%22" \
            + query_string + "%22+&cr=countryNL&lr=lang_nl"
    return url


if __name__ == "__main__":
    queries = get_queries()
    for q in queries:
        sys.stdout.write('%s;%s\n' % (q['idx'], _query_url(q['FirstName'], q['LastName'])))
