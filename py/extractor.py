#!/usr/bin/python
"""
Output:
username
profile_picture_url
list of frends separated by #
"""
import shutil
import db
import sys
import os
import re
from BeautifulSoup import BeautifulSoup
import markup_util
import extractor_hyves
import extractor_myspace
import extractor_twitter
import extractor_linkedin
import extractor_facebook
import util
import codecs

def _get_surface_form(normalized_url_id):
  conn = db.connect()
  cur = conn.cursor()
  cur.execute("select normalized_url from normalized_url where id= " + str(normalized_url_id))
  url = str(cur.fetchone()['normalized_url'])
  dict=[("%2F","/"), ("%2D", "-"), ("%5F", "_"), ("%3F", "?"),\
    ("%2B", "+"), ("%3A", ":"), ("%2E", "."), ("%3D", "=")]
  url = re.sub("http%3A%2F", "http://", url)
  for d in dict:
    url = re.sub(d[0], d[1], url)
  cur.close()
  conn.close()
  return url

def extract(path_to_wget_log, extr, output_dir, normalized_url_id):
  """
  Expects full pathname.
  """
  features_file = output_dir + "/features"
  output = codecs.open(features_file, "w", "utf-8")
  output.write("url:" + _get_surface_form(normalized_url_id) + "\n")
  dir = os.path.dirname(path_to_wget_log)
  log = open(path_to_wget_log, "r").read()
  match = re.search(r"^Saving to: `(.*)'", log, re.MULTILINE)
  if match is None:
    raise Exception("crawl not there")
    #sys.exit(0)
  index = match.group(1)
  index_full_path = dir + "/" + index
  #print index
  f = open(index_full_path, "r")
  text = f.read()
  f.close()
  soup = BeautifulSoup(text)
  platform = extr.__name__[extr.__name__.index("_") + 1:]
  #print "platform:" + platform
  output.write("platform:" + platform + "\n")
  #sys.stdout.write("name:")
  output.write("name:")
  try:
    name = extr.get_profilename(soup)
    name = re.sub("[^\w]", " ", name)
    name = re.sub("\s+", " ", name)
    #sys.stdout.write(name + "\n")
    output.write(name + "\n")
  except Exception, e:
    sys.stderr.write(e.message)
    #sys.stdout.write("\n")
  pic = extr.get_picture(soup)
  #sys.stderr.write("Index: " + os.path.dirname(index_full_path) + "\n")
  #sys.stderr.write("Pic: " + pic + "\n")
  try:
    pic = util.apply_rel_path(os.path.dirname(index_full_path), pic)
    #print "pic:" + pic
    output.write("pic:" + pic + "\n")
    shutil.copyfile(pic, output_dir + "/pic" + pic[pic.rfind("."):])
  except Exception, e:
    #print "pic:"
    output.write("pic:\n")
    sys.stderr.write(e.message)
  #print output_dir
  friends_titles, friends_names  = extr.get_friends(soup)
  text = extr.get_text(soup)
  for i in range(0, len(friends_names)):
    try:
      title = friends_titles[i].strip()
      title = re.sub("\s+", " ", title)
      name = friends_names[i].strip()
      name = re.sub("\s+", " ", name)
      #print "friend title:" , title
      #print "friend name:" , name
      output.write("friend title:" + title + "\n")
      output.write("friend name:" + name + "\n")
    except Exception, e:
      sys.stderr.write(e.message)

  #print "text:" + text
  output.write("text:")
  #output.write(unicode.encode(text, "utf-8"))
  output.write(text)
  output.write("\n")
  output.close()

def _infer_type(url_id):
  conn = db.connect()
  cur = conn.cursor()
  cur.execute("select normalized_url from normalized_url where id= " + str(url_id))
  #sys.stderr.write("urlid is: " + url_id + "\n")
  try:
    id = str(cur.fetchone()['normalized_url'])
  except Exception, e:
    sys.stderr.write("url not in db! " + e.message + "\n")
    return "none"
  finally:
    cur.close()
    conn.close()
  if "hyves" in id:
    return "hyves"
  elif "myspace" in id:
    return "myspace"
  elif "twitter" in id:
    return "twitter"
  elif "linkedin" in id:
    return "linkedin"
  elif "facebook" in id:
    return "facebook"
  else:
    return "none"

if __name__ == "__main__":
  normalized_url_id  = sys.argv[2]
  output_dir = sys.argv[3]
  type = _infer_type(normalized_url_id)
  if type == "hyves":
    extr = extractor_hyves
  elif type == "myspace":
    extr = extractor_myspace
  elif type == "twitter":
    extr = extractor_twitter
  elif type == "linkedin":
    extr = extractor_linkedin
  elif type == "facebook":
    extr = extractor_facebook
  else:
    sys.exit(1)
  extract(sys.argv[1], extr, output_dir, normalized_url_id)
