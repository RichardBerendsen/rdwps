#!/usr/bin/python
"""
Usage:
    %s

Reads in a html document from stdin and serializes it to stdout with its
words normalized
"""

import re
import sys

def normalize(word):
    re_punct = re.compile(r'[.,:;\'|+\-!?()[\]{}\\/_*&@`"<>#]')
    re_year = re.compile(r'[1-2][0-9][0-9][0-9]')
    re_number = re.compile(r'[0-9]+')
    re_wide = re.compile(r'[\200-\377]*')

    # preserve years
    if len(word) == 4 and re_year.match(word) is not None:
        return word

    # else, normalize word 
    word = word.lower() # lowercase
    word = re.sub(re_punct, r'', word) # remove punctuation
    word = re.sub(re_number, r'', word) # remove numbers
    word = re.sub(re_wide, r'', word) # remove non ascii characters

    # after normalization, if it's a stopword, it will still be thrown away in create matrices, if so desired. 
    return word.strip()

def main(args):

    doc = sys.stdin.read()
    #decoded_doc = unicode(doc, 'utf-8', 'ignore')

    words = doc.strip().split()

    for word in words:
        if word != "":
            print >> sys.stdout, normalize(word)

if __name__ == "__main__":
    sys.exit(main(sys.argv))
