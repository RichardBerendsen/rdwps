#!/usr/bin/python

from BeautifulSoup import BeautifulSoup
import urllib # for quote function
import urllib2 # for urlopen
import get_queries
import sys
import time
import oauth2 as oauth
import simplejson as json
from pprint import pprint
from util import normalize
from get_queries import get_queries
from store_urls import store_urls
import random
import time

#Access Token (oauth_token)
#277928803-zRbzt5m964hN4yUansKFDGuOBCHvPIMJYI7Bb4VA#
#Access Token Secret (oauth_token_secret)
#4J0JS9dDDQY7UkWuvsUT357TSDgAqXzL8GQQyQWg8
#Consumer key
#n4adFBeYxuzZmuYJy2McEQ
#Consumer secret
#4FjAMV95BMdyrDosoEpZVIlC2vfE7wL3ERRVrrZAe4
#Request token URL
#http://twitter.com/oauth/request_token
#Access token URL
#http://twitter.com/oauth/access_token
#Authorize URL
#http://twitter.com/oauth/authorize *We support hmac-sha1 signatures. We do not support the plaintext signature method.





def _query_string(first_name, last_name):
    # This URL was created from the URL that wieowie.nl offers
    # when you click on 'more Hyves results'
    first_name = first_name.replace('"', '').strip()
    last_name = last_name.replace('"', '').strip()
    words = first_name.split() + last_name.split()
    query_string = '+'.join(words)
    return urllib.quote(query_string)

def _get_page(url):
   return urllib2.urlopen(url).read()

# parses json result of request for user profiles to twitter api
# returns list of urls
def _parse_json(users_json):
    res = []

    # The url is in the href of an <a> element matching those criteria.
    # Strangely it is the last part of the href right after two consecutive stars.

    #ts = soup.findAll(lambda t: t.name=="a" and t.parent.name=="li" and t.parent.name=="ul" and t["class"]=="fn url beacon" and t.has_key("href"))
    #ts = soup.findAll(lambda t: t.name=="a" and t.has_key("class") and t.has_key("href"))
    #ts = [t for t in ts if t["class"] == "fn url beacon" ]
    #return [ "www.myspace.com" + t['href'] for t in ts ]

def _get_users_json_oauth(first_name, last_name, client):
    err = ''
    repeat = True
    response = ()

    while repeat:
        repeat = False
        timea = time.time()
        try:
            response = client.request("http://api.twitter.com/1/users/search.json?q=%s&per_page=%d" % (
                        _query_string(first_name, last_name), 20))
        except urllib2.HTTPError, e:
            err = "ERROR: %s -- %s\n" % (e.code, e.read())
            if e.code == 400 or e.code == 502:
                repeat = True
                err = ''
                if e.code == 400:
                    sys.stderr.write("[NOTICE] reached the hourly limit, keep trying the same user id (%d)\n" % userid)
                elif e.code == 502:
                    sys.stderr.write("[NOTICE] Twitter is over capacity. Retrying in 1 sec..\n")
                    time.sleep(1)
        timeb = time.time()

        if timeb-timea < 60: # seconds difference
            sys.stderr.write("[NOTICE] I'm too fast, slowing down. sleeping for %f secs.\n" % (61-timeb+timea))
            time.sleep(61-timeb+timea)
    if err:
        sys.stderr.write(err)
        sys.exit(2)
    else:
        return response

# returns json with user profiles
def _get_users_json(first_name, last_name, client):
    err = ''
    repeat = True

    while repeat:
        repeat = False
        timea = time.time()
        try:
            u = urllib2.urlopen(
                    "http://api.twitter.com/1/users/search.json?q=%s&per_page=%d" % (
                        _query_string(first_name, last_name), 20))
        except urllib2.HTTPError, e:
            err = "ERROR: %s -- %s\n" % (e.code, e.read())
            print err
            if e.code == 400 or e.code == 502:
                repeat = True
                err = ''
                if e.code == 400:
                    sys.stderr.write("[NOTICE] reached the hourly limit, keep trying the same user id (%d)\n" % userid)
                elif e.code == 502:
                    sys.stderr.write("[NOTICE] Twitter is over capacity. Retrying in 1 sec..\n")
                    time.sleep(1)

        timeb = time.time()
        if timeb-timea < 60: # seconds difference
            sys.stderr.write("[NOTICE] I'm too fast, slowing down. sleeping for %f secs.\n" % (61-timeb+timea))
            time.sleep(61-timeb+timea)

    if err:
        sys.stderr.write(err)
        sys.exit(2)
    else:
        return u.read().encode('utf-8')


def _main():
    print "Starting twitter crawling..."
    print "############################"
    print "############################"

    consumer = oauth.Consumer('n4adFBeYxuzZmuYJy2McEQ','4FjAMV95BMdyrDosoEpZVIlC2vfE7wL3ERRVrrZAe4')
    token = oauth.Token('277928803-zRbzt5m964hN4yUansKFDGuOBCHvPIMJYI7Bb4VA','4J0JS9dDDQY7UkWuvsUT357TSDgAqXzL8GQQyQWg8')
    client=oauth.Client(consumer,token)

    queries = get_queries()
    for query in queries:
        firstname = query['FirstName']
        lastname = query['LastName']
        print "Querying for: ", firstname, lastname
        response = _get_users_json_oauth(firstname, lastname, client)
        #pprint(response)
        users_json = json.loads(response[1])
        urls = [ "http://twitter.com/" + str(u["screen_name"]) for u in users_json ]
        n_urls=[normalize(u) for u in urls]
        #pprint(n_urls)
        store_urls(firstname, lastname, "twitter", urls[:50], n_urls[:50])
        time.sleep(60 + random.randrange(1,6,1))


if __name__ == "__main__":
    _main()
