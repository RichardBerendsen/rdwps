#!/usr/bin/python
"""crawl yahoo with a query, retrieve top k results"""

from BeautifulSoup import BeautifulSoup
import urllib # for quote function
import urllib2 # for urlopen
import urlparse # to split the url, and urlencode only path part.
import MySQLdb
import MySQLdb.cursors # to use DictCursor
from pprint import pprint
from util import normalize
from get_queries import get_queries
from store_urls import store_urls
import random
import time

def _query_url(firstname, lastname):
    # This URL was created from the URL that wieowie.nl offers
    # when you click on 'more Bing results'
    firstname = firstname.replace('"', '').strip()
    lastname = lastname.replace('"', '').strip()
    words = firstname.split() + lastname.split()
    query_string = '+'.join(words)
    # this URL was created from the URL that wieowie.nl offers
    # when you click on 'more Google results'
    # and from there I went to 'advanced search' and entered the request that
    # the top 50 would be returned. RB: but why does the URL request the top 100 then? :-)
    url = "http://nl.search.yahoo.com/search?" \
            + "n=100&ei=UTF-8&va_vt=any&vo_vt=any&ve_vt=any&vp_vt=any&vd=all&vst=0&vf=all&vm=p&fl=1&fr=yfp-t-701&vs=&" \
            + "p=%22" + query_string \
            + "%22+&vl=lang_nl&vc=nl"
    return url

def _get_page(url):
    try:
        return urllib2.urlopen(url).read()
    except Exception, e:
        print "Exception occurred!!!"
        print type(e)
        print e
        time.sleep(23*60)
        return _get_page(url)

# keep search results in case we would want to get the snippets later.  to
# retrieve these, we would only need the query_id and the rank of each search
# result.
def _store_yahoo_search_result_html(page, query_id):
    try:
        f = open('../../exp/yahoo_search_results_per_query/%s.html' % (query_id,), 'w')
    except IOError, e:
        sys.stderr.write('Error: Exception caught \
while storing yahoo search results xml page\n')
        sys.stderr.write(str(e) + '\n')
        sys.exit(2)
    else:
        f.write(page)
        f.close()


def _parse_search_results(page):
    res = []
    soup = BeautifulSoup(page)
    #f = open("page", "w")
    #f.write(soup.prettify())
    #f.close()

    # The url is in the href of an <a> element matching those criteria.
    # Strangely it is the last part of the href right after two consecutive stars.

#    ts = soup.findAll(lambda t: t.parent.name=="h3" and t.name=="a" and t.has_key("class") and t.has_key("data-bk") \
#                    and t.has_key("href") and t.has_key("lang"))

    ts = soup.findAll(lambda t: t.parent.name=="h3" and t.name=="a" and t.has_key("class") and t.has_key("href"))
    return [ t['href'].split("**")[-1] for t in ts]

def _fetch_urls(firstname, lastname, query_id):
    query_url = _query_url(firstname, lastname)
    print query_url
    page = _get_page(query_url)
    _store_yahoo_search_result_html(page, query_id)
    urls = _parse_search_results(page)
    #print urls
    return urls

if __name__ == "__main__":
    #firstname, lastname = ("Jan", "Hollander")
    #urls = _fetch_urls("Wesley", "Schuts")
    #print urls
    queries = get_queries()
    #print queries

    for q in queries:
      firstname = q['FirstName']
      lastname = q['LastName']
      urls = _fetch_urls(firstname, lastname, q['idx'])
      n_urls=[normalize(u) for u in urls]
      #for i in range(0, len(urls)):
      #  print urls[i]
      #  print n_urls[i]
      #pprint(urls)
      #pprint(n_urls)
      store_urls(firstname, lastname, "yahoo", urls, n_urls)
      time.sleep(167 + random.randrange(1,6,1))
