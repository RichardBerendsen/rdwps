#!/usr/local/bin/bash

# walk over stored crawls from wget,
# and mirror that directory structure with only the needed symbolic links to the actual pages

if [ "$#" -lt "2" ]
then
    echo "Error, please provide crawls directory and an empty directory and optionally an extension to be appended to links. (e.g. .html)"
    exit 2
fi

# links relative to self path
rel_crawls_path=$1
rel_mirror_path=$2
extension=$3
rel_self_path=$(dirname "$0")


if [ ! -d $rel_crawls_path ]
then
    echo "Error, crawls dir path is not a directory"
    exit 2
fi

if [ ! -d $rel_mirror_path ]
then
    echo "Error, mirror path is not a directory"
    exit 2
fi

if [ "$(ls -A $rel_mirror_path)" ]
then
    echo "Error, mirror path is not empty"
    exit "2"
fi

# let's make the relative paths absolute, it will make things easier 
# note that all relative paths are relative to the current working directory (cwd)
# and also note that this will break if you use an absolute path to call this script,
# or if you put this script in usr/bin or whatever and it is automatically looked up.
cwd="$(pwd)"
self_path="${cwd}/${rel_self_path}"
cd "${rel_crawls_path}"
crawls_path=$(pwd)
cd "${cwd}"
cd "${rel_mirror_path}"
mirror_path=$(pwd)
cd "${cwd}"

echo $self_path
echo $crawls_path
echo $mirror_path

unset rel_crawls_path
unset rel_mirror_path
unset rel_self_path

for query_id in $(ls "${crawls_path}")
do
    # create query directory in mirror_path if it is not there 
    if [ ! -e "${mirror_path}/${query_id}" ]
    then
        mkdir "${mirror_path}/${query_id}"
    fi

	# create 'Trash' directory in there if it is not there
	if [ ! -e "${mirror_path}/${query_id}/discard" ]
	then
		mkdir "${mirror_path}/${query_id}/discard"
	fi

    for normalized_url_id in $(ls "${crawls_path}/${query_id}")
    do
		doc_path="${crawls_path}/${query_id}/${normalized_url_id}"
		if [ ! -e  "${doc_path}/wget_log" ]
        then
			echo "Error, no wget_log found in ${doc_path}/" >&2
            exit 2
        fi

        saved_to=$(python "${self_path}/../py/parse_wget_log.py" < \
                "${crawls_path}/${query_id}/${normalized_url_id}/wget_log")
        # create symbolic link if it is not already there and if the target exists
        if [ ! -e "${mirror_path}/${query_id}/${normalized_url_id}${extension}" ] \
            && [ -e "${crawls_path}/${query_id}/${normalized_url_id}/$saved_to" ]
        then
            ln -s \
                "${crawls_path}/${query_id}/${normalized_url_id}/${saved_to}" \
                "${mirror_path}/${query_id}/${normalized_url_id}${extension}"
        fi
    done
done
