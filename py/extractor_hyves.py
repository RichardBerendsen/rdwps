#!/usr/bin/python
"""
Output:
username
profile_picture_url
list of frends separated by #
"""
import sys
import os
import re
from BeautifulSoup import BeautifulSoup
import markup_util

hyves_boilerplate_phrases = ["Naam:", "Mijn merken:", "Spots:", "School:", "Tv programma's:", "Muziek:", "Films:", "Media:", "Sport:", "Reizen:", "Bekijk volledig profiel", ", Hyver sinds", "x gezien", "bekeken", "Profiel", "Hyver", "sinds"]
#hyves_boilerplate_phrases = []

def get_profilename(soup):
  name_div = soup.findAll(lambda x: x.name == "div" and x.has_key("id") and x['id'] == "page-con-info")
  text = name_div[0].__repr__()
  #print text
  name = markup_util.decode_utf_ref(markup_util.remove_markup(text, ["h1", "img", "em", "!--", "--", "a"]).strip())
  if name != "":
    return name
  name_div = name_div[0].findAll(lambda x: x.name == "div" and x.has_key("class") and x['class'] == "bottom-inline-block")
  text = name_div[0].__repr__()
  #print text
  #print "\n\n\n\n\n"
  name = markup_util.decode_utf_ref(markup_util.remove_markup(text, ["h1", "img", "em", "a"]).strip())
  print name
  if name != "":
    return name
  name_div = name_div[0].findAll(lambda x: x.name == "div" and x.has_key("class") and x['class'] == "bottom-inline-block")
  text = name_div[0].__repr__()
  name = markup_util.decode_utf_ref(markup_util.remove_markup(text, ["h1", "img", "em", "a"]).strip())
  
  return name

def get_picture(soup):
  """
  Returns the absolute path to the pic.
  """
  path = ""
  div_candidates = soup.findAll(lambda x:x.name == "div" and x.has_key("class"))
  for d in div_candidates:
    if "avatar" in d['class'] and "profile" in d['class']:
      img = d.findAll(lambda x:x.name == "img")
      path = img[0]['src']
  return path


def get_friends(soup):
  """
  Return the 'titles' of the friends followed by their "surface" forms.
  """
  elems = soup.findAll(lambda x: x.name== "a" and x.has_key('title') and x.has_key('class') and x['class'] == "avatar-name-link")
  #print elems
  return [markup_util.decode_utf_ref(x['title'].strip()) for x in elems], [markup_util.decode_utf_ref(markup_util.remove_markup(x.__repr__(), ["strong"]).strip()) for x in elems]

def get_text(soup):
  """
  Returns the non-boilerplate from the page.
  """
  #elem = soup.findAll(lambda x: x.name == "div" and x.has_key('class') and (x['class'] =="section s2" or x['class'] == "unit s2"))[0]
  elems = soup.findAll(lambda x: x.name == "div" and x.has_key('class') and x['class'] == "box profile-overview")
  if len(elems) == 0:
    return "\n"
  elem = elems[0]
  all = [x for x in elem.findAll(text=True) if len(x) > 1]
  res = ""
  for a in all:
    a = a.strip()
    #if a not in hyves_boilerplate_phrases:
    if len(a) > 1 and not markup_util.tag_in_list(a, hyves_boilerplate_phrases):
      res += " " + a
  return res
