"""
Usage:
    %s web_pages_dir which_weps

where which_weps = 1 or 2

stores in the db which doc ids are in each query dir:
    we need to add docs that went missing during preprocessing as singletons to the clusterings before evaluating.

I guess the shortest cut is to just do a unix find kind of thing on the directories themselves,
and store the bleeding stuff.
"""

import sys
import os
import db
import idiom
import util
if len(sys.argv) != 3:
    print __doc__ % sys.argv[0]
    sys.exit(2)

web_pages_dir = sys.argv[1]
which_weps = int(sys.argv[2])

query_dirs = [dir for dir in idiom.ls_dir_or_exit(web_pages_dir) if not dir == ".svn"]

conn = db.connect()

for query_dir in query_dirs:
    query = util.get_query_from_dirname_weps(conn, query_dir)

    if which_weps != 1 and which_weps != 2:
        print __doc__ % sys.argv[0]
        sys.exit(2)

    if which_weps == 2:
        docs = [doc for doc in idiom.ls_dir_or_exit(os.path.join(web_pages_dir, query_dir)) if not doc == ".svn"]

    if which_weps == 1:
        docs = [doc for doc in idiom.ls_dir_or_exit(os.path.join(web_pages_dir, query_dir, 'raw')) if not doc == ".svn"]

    sql = "insert into search_results_weps (query_id, doc_id, which_weps) values (%s, %s, %s)"
    for doc in docs:
        doc_id, ext = os.path.splitext(os.path.basename(doc))
        params = (query['idx'], doc_id, which_weps)
        print >> sys.stderr, "inserting: %s %s, query_id %s, doc_id %s" % (query['FirstName'], query['LastName'], query['idx'], doc_id)
        db.insert(sql, params, conn)

conn.close()


