#!/usr/bin/python
"""
Utilities for dealing with markup in addition to BeautifulSoup.
"""
import re


def decode_utf_ref(word):
  """
  Some names have stuff like &#039; inside. This is ', since 039 is the ascii
DECIMAL for apostrophe.
  """
  to_repl = re.findall(r'(&#(\d+);)', word) 
  for x in to_repl:
    word = word.replace(x[0], chr(int(x[1])))
  word = word.replace(r'&amp;', "&")
  return word


def tag_in_list(tag, list):
  for l in list:
    #if len(tag) >= len(l) and tag[:len(l)] == l:
    if len(l) == 1 and tag[0] == l:
      return True 
    elif len(l) > 1 and l in tag:
      #print "Checking tag: " + tag + " true" + " " + l
      return True
  #print "Checking tag: " + tag + " false"
  return False

# only get the text that is a direct child of element or a child of a child of element if this child is in the
# exception_list e.g assuming that excpetin list consists of h1 : <elem> get_me <h1> getme too </h1> <bla> don't get me </blah> and get me </elem>
def remove_markup(text, exception_list):
  res = ""
  markup_word = ""
  markup_level = 0
  reading_starting_tag = False
  reading_closing_tag = False
  i = 0
  while i < len(text):
    if reading_starting_tag:
      if text[i] == ">":
        #print "<" + markup_word + ">"
        #print markup_level
        reading_starting_tag = False
        if not tag_in_list (markup_word, exception_list):
          markup_level += 1
        i = i + 1
        markup_word = ""
      else:
        markup_word += text[i]
        i = i + 1
    elif reading_closing_tag:
      if text[i] == ">":
        #print "</" + markup_word + ">"
        #print markup_level
        reading_closing_tag = False
        if not tag_in_list (markup_word,exception_list):
          markup_level -= 1
        i = i + 1
        markup_word = ""
      else:
        markup_word += text[i]
        i = i + 1
    else:
      if text[i] == "<" and text[i+1] == "/":
        reading_closing_tag = True
        markup_word = ""
        i = i + 2
      elif text[i] == "<":
        reading_starting_tag = True
        markup_word = ""
        i = i + 1
      else:
        if markup_level < 2:
          #print text[i]
          res += text[i]
          i = i + 1
        else:
          i = i + 1
  return res
