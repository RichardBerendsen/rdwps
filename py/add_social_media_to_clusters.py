#!/usr/bin/python

import sys
import extractor
import serializer
import util
import hac

def get_indexes_of_social(labels):
  res = []
  for i in range(0, len(labels)):
    if extractor._infer_type(labels[i]) != "none":
      res.append(i)
  return res

def get_sim_single_link_max(clustering, social_weights, cluster1_index, page_index, sim_matrix, social_factor):
  #print len(clustering)
  #print len(social_weights)
  cl1 = util.linearize(clustering[cluster1_index])
  #print sim_matrix[cl1[0]][page_index]
  #print cl1
  sim = 0.0
  for el1 in cl1:
    #if social_weights[cluster1_index] == 1:
    #  weighted_sim = sim_matrix[el1][page_index]
    #else:
    
    weighted_sim = sim_matrix[el1][page_index]/(1 + social_weights[cluster1_index]*social_factor)
    if  weighted_sim > sim:
      sim = weighted_sim
  #print sim
  return sim

#* marks an element for deletion
def put_in_cluster(clustering, social_weights, i, free_social, k):
  #print free_social
  #print k
  #clustering[i].append(free_social[k])
  clustering[i].append(k)
  #print clustering
  social_weights[i] += 1
  #free_social[k] = "*"
  free_social[free_social.index(k)] = "*"
  return clustering, social_weights, free_social

# create a cluster for k
def create_cluster(clustering, social_weights, free_social, k):
  #print k
  clustering.append([k])
  #clustering.append(free_social[k])
  social_weights.append(1)
  free_social[free_social.index(k)] = "*"
  #free_social[k] = "*"
  return clustering, social_weights, free_social

def assimilate_candidates(clustering, social_weights, free_social, similarities):
  #print clustering
  #print similarities
  for i in range(0, len(clustering)):
    candidates = [x for x in similarities if x[0] == i]
    #print candidates
    if len(candidates ) < 1:
      continue
    closest = max(candidates, key = lambda x: x[1])
    #print closest
    clustering, social_weights, free_social = put_in_cluster(clustering, social_weights, i, free_social, closest[2])
  #print clustering
  return clustering, social_weights, free_social

#def get_index(url, matrix):

def get_points(clustering):
  res = []
  for c in clustering:
    res.extend(c)
  return res

# works for top level clustering
def clustering_from_labelled_clustering(labelled_clustering, labels):
  res = []
  for l in labelled_clustering:
    res.append([labels.index(k) for k in l])
  return res

# social weight = number of social pages in cluster
# sim_matrix = the big similarity matrix including social profiles etc.
# free_social is a list of indexes from the similarity matrix
def do_one_step(clustering, social_weights, sim_matrix, free_social, threshold, social_factor):
  print "STEP"
  #print clustering
  #print free_social
  similarities = []
  #print free_social
  for i in range(0, len(free_social)):
    #print i
    clust, similarity = -1, -1
    for j in range(0, len(clustering)):
       sim = get_sim_single_link_max(clustering, social_weights, j, free_social[i], sim_matrix, social_factor)
       #print "sim", sim
       if sim > similarity:
         clust = j
         similarity = sim
    #print "similarity: ", similarity, "threshold", threshold, clustering[clust], free_social[i]
    if similarity < threshold:
       #clustering, social_weights, free_social = put_in_cluster(clustering, social_weights, clust, free_social, k)
       clustering, social_weights, free_social = create_cluster(clustering, social_weights, free_social, free_social[i])
    else:
       similarities.append((clust, similarity, free_social[i]))
  #print similarities
  clustering, social_weights, free_social = assimilate_candidates(clustering, social_weights, free_social, similarities)
  free_social = filter( lambda x: x!= "*", free_social)
  return clustering, social_weights, free_social

if __name__=="__main__":
  usage = "Useage: progname clustering_of_web_part simliarity_matrix outfile similarity_threshold social_factor"
  labelled_clustering = serializer.deserialize_clustering_to_top_level_only(sys.argv[1])
  sim_matrix, labels = serializer.deserialize_similarity_matrix(sys.argv[2])
  clustering = clustering_from_labelled_clustering(labelled_clustering, labels)
  free_social = [labels.index(x) for x in labels if labels.index(x) not in get_points(clustering)]
  social_weights = [0 for i in clustering]
  indexes_of_social = get_indexes_of_social(labels)
  outfile = sys.argv[3]
  threshold = float(sys.argv[4])
  social_factor = float(sys.argv[5])
  #print clustering
  #print get_points(clustering)
  #print free_social
  while True:
    clustering, social_weights, free_social = do_one_step(clustering, social_weights, sim_matrix, free_social, threshold, social_factor)
    #print free_social
    #print clustering
    if len(free_social) < 1:
      break
  to_serialize = hac.get_clustering_with_labels(clustering, labels)
  to_serialize = to_serialize.replace(" ", "")
  serializer.serialize_clustering(outfile, to_serialize)
