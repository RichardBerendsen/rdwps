#!/bin/bash

# run an exp 

function is_empty {
    if [ "$(ls -A "$mypath")" ]
    then
        echo "no"
    else
        echo "yes"
    fi
}

script_path="$(dirname $0)"
if [ "$#" -lt 2 ]
then
    echo "Usage: progname dataset dir_to_store_exp_dir_in [[ runid ] ...]" 1>&2
    echo "dataset can have these values: ( weps1 | weps2 | wieowie | wieowiesers | wieowieprofiles | weps1train)" 1>&2
    exit 2
fi
dataset=$1
case $dataset in
"weps1")
    truth_files_dir="$script_path/../../exp/gold_truth/weps1-test"
    ;;
"weps2")
    truth_files_dir="$script_path/../../exp/gold_truth/weps2"
    ;;
"wieowie")
    truth_files_dir="$script_path/../../exp/gold_truth_both"
    ;;
"wieowiesers")
    truth_files_dir="$script_path/../../exp/gold_truth_sers"
    ;;
"wieowieprofiles")
    truth_files_dir="$script_path/../../exp/gold_truth_profiles"
    ;;
"weps1train")
    truth_files_dir="$script_path/../../exp/gold_truth/weps1-train"
    ;;
*)
    echo "Please specify a valid value for the dataset." 1>&2
    exit 2
    ;;
esac
if [ ! -e $truth_files_dir ]
then
    echo "Thruth files dir does not exist, maybe you are on the wrong machine?" 1>&2
    exit 2
fi
shift # next arg

exp_dir=$1
if [ ! -d $exp_dir ]
then
    echo "Please specify a directory to store the experiment directory in" 1>&2
    exit 2
fi
shift # next arg

# rest of the arguments are run_ids
run_ids=$@
echo $run_ids

# apparently we are good now, argument wise. We now make a directory for the
# experiment, with a unique name: the current time.

cur_exp_dir="$(date +'%F_%H-%M-%S')"
echo $cur_exp_dir
if [ -e "$exp_dir/$cur_exp_dir" ] 
then
    echo "Directory $cur_exp_dir already exists, wait a second and retry" 1>&2
    exit 2
else
    mkdir "$exp_dir/$cur_exp_dir"
    run_dir="$exp_dir/$cur_exp_dir/runs"
    results_dir="$exp_dir/$cur_exp_dir/results"
    mkdir $run_dir
    mkdir $results_dir
fi

# first write the runs
{
    case $dataset in
    "weps1train")
        python "$script_path/../py/write_weps_runs.py" "$run_dir" 3 $run_ids
        ;;
    "weps1")
        python "$script_path/../py/write_weps_runs.py" "$run_dir" 1 $run_ids
        ;;
    "weps2")
        python "$script_path/../py/write_weps_runs.py" "$run_dir" 2 $run_ids
        ;;
    "wieowie")
        python "$script_path/../py/write_runs.py" --dataset=both "$run_dir" $run_ids
        ;;
    "wieowiesers")
        python "$script_path/../py/write_runs.py" --dataset=SERs "$run_dir" $run_ids > $exp_dir/$cur_exp_dir/write_runs.out 2> $exp_dir/$cur_exp_dir/write_runs.err
        ;;
    "wieowieprofiles")
        python "$script_path/../py/write_runs.py" --dataset=Profiles "$run_dir" $run_ids
        ;;
    esac

# now call the weps scorer
    java -jar "$script_path/../java/wepsEvaluation.jar" \
    "$truth_files_dir" \
    "$run_dir" \
    "$results_dir" \
    -ALLMEASURES \
    -AllInOne -OneInOne -Combined 2> "$exp_dir/$cur_exp_dir/wepsEvaluation.log" > "$exp_dir/$cur_exp_dir/wepsEvaluation.out"

# now call our simple py script that creates a table with just BEP, BER and F-0.5 of the average of all experiments.
    python "$script_path/../py/write_results_table.py" "$results_dir"  1 > "$results_dir/averages_of_all_runs"

# now call our simple py script that writes a data frame to analyse results with R.
    python "$script_path/../py/write_data_frame_results.py" "$results_dir" 1 > "$results_dir/results.R"

# now call our own weps-eval script, which writes detailed results per run per query.
    python "$script_path/../py/weps_eval.py" "$truth_files_dir" "$exp_dir/$cur_exp_dir"
}
