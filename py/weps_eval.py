"""evaluates a people search clustering with B Cubed precision and recall and
their macro averaged harmonic mean (F-measure with alpha = 0.5)

Macro averaged means that the F-measure is first calculated for each query.
Then the F-measures are averaged over queries. Because the harmonic mean of two
numbers is closer to the smaller one, this may cause the macro averaged
F-measure to fall below the average of the two numbers.

Usage:
    %s  gold_truth_dir exp_dir 

Expects to see filenames with the same name in gold_truth_dir and each dir in
exp_dir/runs/, each containing a clustering in weps format, up to case.
In each run dir, it will create a directory results, with detailed bep and ber per document.

Will write a table for each run with results per query and average results to
exp_dir/results_own_eval_script. 

Because it just creates these directories like that, it should only be called once after 
run_exp.sh.

As a matter of fact, I'll code the call into run_exp.sh, so don't call this standalone then.
"""

import xml.dom.minidom as dom
import sys
import os
import idiom
import query_eval as qe
import numpy as np

def same_strings_up_to_case(l1, l2):
    """Expects two lists of strings, and returns true if they are of equal
    length, and contain the same set of strings up to case.
    
    If they are the same, however, we would also like to have the correspondence
    between them, so we return two dicts: l1_to_l2 and l2_to_l1
    
    Note that we also assume that nor l1 nor l2 contains duplicates"""

    if len(l1) != len(l2):
        return False, {}, {}

    s1 = set([s.lower() for s in l1])
    s2 = set([s.lower() for s in l2])

    if len(s1 ^ s2):
        return False, {}, {}

    l1_to_l2 = {}
    l2_to_l1 = {}
    for s in l1:
        for t in l2:
            if s.lower() == t.lower():
                l1_to_l2[s] = t
                l2_to_l1[t] = s
    return True, l1_to_l2, l2_to_l1


def eval_run(gold_truth_dir, ls_gold_truth_dir, run_dir, results_dir):
    """Expects directory with Weps-style gold truth, the names of xml files in
    there (one for each query), a directory where clusterings for each query
    are stored, and a directory where to store the results"""

    ls_run_dir = [f for f in idiom.ls_dir_or_exit(run_dir) if f.endswith(".xml")]

    same, gold2run, run2gold = same_strings_up_to_case(ls_gold_truth_dir, ls_run_dir)
    if not same:
        print >> sys.stderr, """Error: the same queries are not present in the
        gold truth dir and the run dir.
        
        gold_truth_dir: %s
        run_dir: %s
        
        Skipping this run""" % (gold_truth_dir, run_dir)
        return

    # they are the same, create a directory for detailed results of this run,
    # which will contain one file for every query.
    run_name = os.path.basename(run_dir)
    print run_name
    query_2_eval = {}
    for query_file in ls_gold_truth_dir:
        query_eval = qe.QueryEval(gold_truth_dir, query_file, run_dir, gold2run[query_file])
        query_eval.print_detailed_results(os.path.join(results_dir, query_file, "%s.eval" % (run_name,)))
        query_2_eval[query_file] = (query_eval.bep, query_eval.ber, query_eval.f05, query_eval.f02)

    # write results run
    f = open(os.path.join(results_dir, "%s.eval" % (run_name,)), 'w')
    print >> f, "%s\t%s\t%s\t%s\t%s" % ("topic", "BEP", "BER", "FMeasure_0.5_BEP.BER", "FMeasure_0.2_BEP.BER")
    for topic, (bep, ber, f05, f02) in query_2_eval.iteritems():
        print >> f, "%s\t%s\t%s\t%s\t%s" % (topic, bep, ber, f05, f02)
    avg_bep, avg_ber, avg_f05, avg_f02 = np.mean(np.array(zip(*query_2_eval.itervalues())), axis=1)
    print >> f, "Average\t%s\t%s\t%s\t%s" % (avg_bep, avg_ber, avg_f05, avg_f02)
    f.close()


def main(args):
    if len(args) != 3:
        print >> sys.stderr, __doc__ % args[0]
        return 2

    gold_truth_dir = args[1].rstrip('/')
    exp_dir = args[2].rstrip('/')
    runs_dir = os.path.join(exp_dir, "runs")
    results_dir = os.path.join(exp_dir, "results_own_eval_script")
    os.mkdir(results_dir)

    ls_gold_truth_dir = idiom.ls_dir_or_exit(gold_truth_dir)
    ls_runs_dir = idiom.ls_dir_or_exit(runs_dir)

    # keep only xml files
    ls_gold_truth_dir = [i for i in ls_gold_truth_dir if i.endswith('.xml')]
    for query_file in ls_gold_truth_dir:
        os.mkdir(os.path.join(results_dir, query_file))

    for run_dir in [d for d in ls_runs_dir if d != ".svn"]:
        eval_run(gold_truth_dir, ls_gold_truth_dir, "%s/%s" % (runs_dir, run_dir), results_dir)

if __name__ == "__main__":
    sys.exit(main(sys.argv))
