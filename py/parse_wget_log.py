# read from stdin a wget log file

import sys
import re

log = sys.stdin.read()

match = re.search(r"^Saving to: `(.*)'", log, re.MULTILINE)
if match is None:
    print "crawl not there"
    sys.exit(0)
print match.group(1)
