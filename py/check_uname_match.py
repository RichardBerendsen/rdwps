#!/usr/bin/python
"""
Usage: script input_dir
"""
import sys
import os

input_dir = sys.argv[1]
urls = os.listdir(input_dir)

names = {}
platforms = {}

def find(what, lines):
  for l in lines:
    #print l
    l = l.strip()
    n = len(what)
    if l[:n] == what:
      return l[n:]
  return ""

for u in urls:
  try:
    f = open(input_dir + "/" + u + "/features", "r")
    #print "OPENED", u
    lines = f.readlines()
    f.close()
    name = find("name:", lines)
    platform = find("platform:", lines)
    #if platform == "twitter" or platform == "hyves" :
    #  print u, platform, name
    names[u] = find("name:", lines)
    platforms[u] = find("platform:", lines)
  except Exception, ex:
    pass #print ex.message


printed_names=[]
for u in names.keys():
  name = names[u]
  platform = platforms[u]
  for v in names.keys():
    if v != u and names[u] == names[v] and platforms[u] != platforms[v] and len(names[u]) > 0:
      pass #print "Match: ", u, v, names[u]
      if name not in printed_names:
        print "\t\t" + name
        printed_names.append(name)


