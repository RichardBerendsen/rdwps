#!/usr/bin/python

import get_queries
from util import normalize
from pprint import pprint
import sys

def _connect():
  return get_queries._connect().cursor()

def _get_session_ids(FirstName, LastName, cursor):  
  cursor.execute("select SessionID from pilot_train_val_test_sessions_with_coclicks where FirstName=%s and LastName=%s", (FirstName, LastName))
  res = cursor.fetchall()
  sessids = [x['SessionID'] for x in res]
  return sessids
    
def _get_search_ids(FirstName, LastName, SessionID, cursor):  
  cursor.execute("select SearchID from Searches where FirstName=%s and LastName=%s and SessionID=%s", (FirstName, LastName, SessionID))
  res = cursor.fetchall()
  search_ids = [x['SearchID'] for x in res]
  return search_ids
  
def _get_normalized_urls(SearchID, cursor):
  cursor.execute("select URL from Clickouts where SearchID = %s", (SearchID, ))
  res = cursor.fetchall()
  
  return [normalize(x['URL']) for x in res]
  
def get_normalized_urls(FirstName, LastName):
  res = []
  cursor = _connect()
  sess_ids = _get_session_ids(FirstName,LastName, cursor)
  for sess_id in sess_ids:
    search_ids = _get_search_ids(FirstName, LastName, sess_id, cursor)
    for srch_id in search_ids:
        res.extend(_get_normalized_urls(srch_id, cursor))
  cursor.close()
  res = _get_unique(res)
  return res

def _get_coclicks(FirstName, LastName, url1, url2, cursor):
  coclicks = 0
  sess_ids = _get_session_ids(FirstName, LastName, cursor)
  for sess_id in sess_ids:
    cursor.execute("select SearchID from Searches where FirstName=%s and LastName=%s and SessionID=%s", (FirstName, LastName, sess_id))
    res = cursor.fetchall()
    all_search_ids = [x['SearchID'] for x in res]
    for srch_id in all_search_ids:
      #cursor.execute("select URL from Clickouts where SearchID=%s", (srch_id,))
      cursor.execute("select URL from pilot_train_val_test_clickouts where SearchID=%s", (srch_id,))
      res = cursor.fetchall()
      urls = [u['URL'] for u in res]
      n_urls = [normalize(u) for u in urls]
      if url1 in n_urls and url2 in n_urls:
        coclicks += 1    
  return coclicks

def product(FirstName, LastName, n_urls, out):
  cursor = _connect()  
  for u in n_urls:
    out.write("\n")
    for v in n_urls:
      if u != v:                
        #print u
        #print v
        out.write(str(_get_coclicks(FirstName, LastName, u, v, cursor)) + " ")
        #_get_coclicks(FirstName, LastName, u, v, cursor)
      else:
        out.write("# ")      
  cursor.close()
  
def _get_unique(somelist):
  res = []
  for l in somelist:
    if l not in res:
      res.append(l)
  return res
  
if __name__ == "__main__":
  out_dir = sys.argv[1]
  print "Outputting to:" + out_dir
  queries = get_queries.get_queries()  
  for q in queries:    
    res = get_normalized_urls(q['FirstName'], q['LastName'])
    filename = q['FirstName'] + "_" + q['LastName']
    filename = filename.replace("\"", "#")
    print filename
    out = open(out_dir + "/" + filename, "w")
    product(q['FirstName'], q['LastName'], res, out)
    out.close()
  #res = get_normalized_urls("Wendy", "\"de Wit\"")
  #pprint(len(res))
  #product("Wendy", "\"de Wit\"", res, sys.stdout)