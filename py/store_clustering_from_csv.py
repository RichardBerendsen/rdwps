"""
Usage:
    %s run_name < clustering

Expected format on stdin:

query_id TAB doc_id [TAB doc_id]*

will store run in db, for wieowie only now.
"""

import sys
import db
import os
import store_run

if len(sys.argv) != 2:
    print __doc__ % sys.argv[0]
    sys.exit(2)

run_name = sys.argv[1]

lines = sys.stdin.readlines()

# insert run
sql_insert_run =  "insert into run (name, description) values (%s, '')"
params_insert_run = (run_name,)
run_id = db.insert_select_last_insert_id(sql_insert_run, params_insert_run)

# insert cluster
sql_insert_cluster = "insert into run_cluster (query_id, run_id) values (%s, %s)"
for line in lines:
    parts = [int(i) for i in line.strip().split()]
    query_id = parts[0]
    doc_ids = parts[1:]

    # insert cluster
    params_insert_cluster = (query_id, run_id)
    cluster_id = db.insert_select_last_insert_id(sql_insert_cluster, params_insert_cluster)

    # insert docs in cluster
    sql_insert_doc = "insert into run_clustering (cluster_id, normalized_url_id) values (%s,%s)"
    for doc_id in doc_ids:
        params_insert_doc = (cluster_id, doc_id)
        db.insert(sql_insert_doc, params_insert_doc)

print run_id
