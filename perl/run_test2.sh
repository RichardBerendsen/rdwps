#!/bin/bash

# indir for this script should be a directory where each subdirectory is
# a person's name and each file in a subdirectory ends in .txt
#

indir=$1
outdir=$2
annotation_dir=$3
names=(`ls $indir`)
./weps-cluster.pl $outdir $indir
./weps_eval_bcubed.pl $outdir/ $annotation_dir > $outdir/results.txt
cat $outdir/results.txt
