"""
situation: we have a directory structure created by ../bash/run_exp.sh

in there there are a number of experiments:

    runs/
        31_expname/
            query_1
            query_2
            ...
        32_expname/
            query_1
            query_2
            ...
        ...
    results/
        31_expname.eval
        32_expname.eval

    and then in 31_expname.eval we have

                BEP BER .... F_measure
    query_1
    query_2

and our goal is to find:

    for each query,
        the id of the run that produced the best result, in terms of F-measure.

Therefore, we:

    list all run dirs in runs/
    for each run:
        find corresponding .eval file in results/
        for each query in .eval file
            store (run_id, query_id, f-measure)

    for each query:
        find the best performing run_id 
        output query_id, run_id 
"""

import idiom
import sys

if len(sys.argv) != 3:
    print >> sys.stderr, "please specify a runs/ dir and a results/ dir"
    exit(2)

runs_dir = sys.argv[1]
results_dir = sys.argv[2]

ls_runs_dir = idiom.ls_dir_or_exit(runs_dir)
ls_results_dir = idiom.ls_dir_or_exit(results_dir)

query2run_fm = {}

for run in ls_runs_dir:
    run_id = run.split('_')[0]
    f = open("%s/%s.eval" % (results_dir, run))
    lines = f.readlines()
    for line in lines[1:-1]: # these contain queries (first line is header, last line is average)
        fields = line.strip().split()
        name = fields[0]
        f_measure = fields[3]
        if name in query2run_fm:
            if f_measure > query2run_fm[name][1]:
                query2run_fm[name] = (run_id, f_measure)
        else:
            query2run_fm[name] = (run_id, f_measure)
    f.close()

for query, (run_id, fm) in query2run_fm.iteritems():
    print "%s;%s" % (query, run_id)




