#!/usr/bin/perl -w

use strict;
use warnings;

my($out_dir,@in_dirs)=@ARGV;

my $total_name_file_pairs=0;
my $no_top_terms=5;
my $min_sim=0.005;

my $max_centroid_terms=100;

my $idf_param='global';
my $stemming=1;
my %stem2unstemmed;
my $strategy='complete';
my $use_bigrams=0;

my $lib='/home/christof/scripts/lib';
unshift (@INC, "$lib");
require 'stop-words.pl';
require 'porter_stemmer.pl';

my %stop_words;

&load_stopwords_strict('/home/christof/scripts/dowser/lib/stop_short.wrd', \%stop_words);

require 'porter_stemmer.pl';

my %file_term;
my %term_occ;
my %idf;
my $total_files=0;
for(my $d=0; $d<@in_dirs; $d++) {
    my $dir=$in_dirs[$d];
    opendir(D,"$dir");
    while(defined(my $file=readdir(D))) {
	next if($file=~/^\./);

	my $file_id=$file;
	$total_files++;

	open(F,"<$dir/$file");
	    
	my @tokens;
	while(defined(my $line=<F>)) {
	    chomp($line);
	    $line=~s/\[[^ ]*\]/ /g;
	    
	    $line=&naive_tokenize($line);
	    
	    my @line_tokens=split(/[\s\t]+/,$line);
	    unshift(@line_tokens,'<s>');
	    push(@line_tokens,'</s>');
	    push(@tokens,@line_tokens);
	    
	}

	my $prev_token;
	for(my $i=0; $i<@tokens; $i++) {
	    $tokens[$i]=&normalize_word($tokens[$i]);
	    next if($tokens[$i]=~/^[\s\t]*$/);

	    if($stemming) {
		my $unstemmed=$tokens[$i];
		$tokens[$i]=&porter_stem($tokens[$i]);
#		$tokens[$i]=~s/^(.......).+$/$1/;
		$stem2unstemmed{$tokens[$i]}{$unstemmed}=1;
	    }
		    
	    next if($tokens[$i]=~/^[\s\t]*$/);
	    $file_term{$file_id}{$tokens[$i]}++;
	    $term_occ{$tokens[$i]}{$file_id}=1;

	    if($use_bigrams && defined($prev_token)) {
		my $bigram="$prev_token\_$tokens[$i]";
		$file_term{$file_id}{$bigram}++;
		$term_occ{$bigram}{$file_id}=1;
	    }
	    $prev_token=$tokens[$i];

	}
    }
    closedir(D);
}

#compute idf scores for each term 
foreach my $word (keys %term_occ) {
	$idf{$word}=log($total_files/(keys %{ $term_occ{$word} }));
}

my %doc_weight;
#compute the doc weight for each file (later used for length normalization)
&compute_doc_weights($idf_param);


my %sim;
my %sim_matrix;
my %sim_contribution;

#compute all similarities (cosine) between all individual documents (wrt a name)
print STDERR "Building similarity matrix ... \n";
my @sorted_ids=(sort (keys %file_term));
for(my $i=0; $i<@sorted_ids-1; $i++) {
    my $file1=$sorted_ids[$i];
    for(my $j=$i+1; $j<@sorted_ids; $j++) {
	my $file2=$sorted_ids[$j];
	my $sim=&sim_cosine($idf_param,$file1,$file2);
	
	$sim_matrix{$file1}{$file2}=1-$sim;
	$sim_matrix{$file2}{$file1}=1-$sim;	
    }
}
print STDERR "done.\n";


my %cluster;
my %centroids;
my %centroid_weights;
my %point_centroid_matrix;


#initialize all clusters (as one doc per cluster)
my $id=-1; 
foreach my $file (keys %file_term) {
    $id++;
    $cluster{$id}{$file}=1;
    &compute_centroid($id,$idf_param,\%centroids,\%centroid_weights);
    $point_centroid_matrix{$id}{$file}=0;   
}


my $change=1;
my $no_iterations=0;
my $min_iterations=20;
while($change) {	    
    print STDERR "iteration=$no_iterations\n";
    my @cluster_ids=(keys %cluster);
    
    if(@cluster_ids==1) {
	print STDERR "iteration=$no_iterations\n";
	last;
    }

    my $min_dist=10**10;
    my $dist_threshold=0.0;
    my $min_i;
    my $min_j;
    $change=0;
    for(my $i=0; $i<@cluster_ids-1; $i++) {
	for(my $j=$i+1; $j<@cluster_ids; $j++) {
	    my $d=cluster_dist($strategy,$cluster_ids[$i],$cluster_ids[$j],\%point_centroid_matrix);       	
#		print STDERR "d=$d i=$cluster_ids[$i] j=$cluster_ids[$j]\n";
	    if($d<=$min_dist) {
		$min_dist=$d;
		$min_i=$cluster_ids[$i];
		$min_j=$cluster_ids[$j];
	    }
	    
	}
    }

    if($min_dist<=1-$min_sim) {
	$change=1;
    }
    
    $id++;
    my $cluster_i_string;
    my $cluster_j_string;
    foreach my $file (keys %{ $cluster{$min_i} }) {
	$cluster{$id}{$file}=$min_i;
#	    $inactive_cluster{$min_i}{$file}=$cluster{$min_i}{$file};
	$cluster_i_string=join(" ",(keys %{ $cluster{$min_i} }));
    }
    
    foreach my $file (keys %{ $cluster{$min_j} }) {
	$cluster{$id}{$file}=$min_j;
#	    $inactive_cluster{$min_j}{$file}=$cluster{$min_j}{$file};
	$cluster_j_string=join(" ",(keys %{ $cluster{$min_j} }));
    }
    
#	print STDERR "compute_centroid($id,$name,...)\n";
    
    &compute_centroid($id,$idf_param,\%centroids,\%centroid_weights);
    &fill_point_centroid_matrix($id,$idf_param,\%point_centroid_matrix);

    delete($cluster{$min_i});
    delete($cluster{$min_j});
    delete($centroids{$min_i});
    delete($centroids{$min_j});
    delete($point_centroid_matrix{$min_i});
    delete($point_centroid_matrix{$min_j});
    
    $no_iterations++;

    if($no_iterations>1000 || $change==0) {
	print STDERR "iteration=$no_iterations\n";
	last;
    }
}


my $filename="cluster.xml";
open(RES,">$out_dir/$filename");
print RES "<clustering>\n";
my $entity_id=0;
foreach my $c (keys %cluster) {
    print RES "\t<entity id=\"$entity_id\">\n";
    foreach my $cluster_doc (sort (keys %{ $cluster{$c} })) {
	$cluster_doc=~s/^[0]+([0-9]+)$/$1/;
	print RES "\t\t<doc rank=\"$cluster_doc\" \/>\n";
    }
    print RES "\t</entity>\n";
    $entity_id++;
}
print RES "<\/clustering>\n";
close(RES);








sub sim_cosine {
    my ($idf_param,$file1,$file2)=@_;
    my $pair="$file1 $file2";

    my $sum=0;
    if($idf_param eq 'local') {
	foreach my $term (keys %{ $file_term{$file1} }) {
	    if(defined($file_term{$file2}{$term})) {
		$sum+=$file_term{$file1}{$term}*$idf{$term}**2*$file_term{$file2}{$term};
		$sim_contribution{$pair}{$term}=$file_term{$file1}{$term}*$idf{$term}**2*$file_term{$file2}{$term};
	    }
	}
    } elsif ($idf_param eq 'global') {
	foreach my $term (keys %{ $file_term{$file1} }) {
	    if(defined($file_term{$file2}{$term})) {
		$sum+=$file_term{$file1}{$term}*$idf{$term}**2*$file_term{$file2}{$term};
		$sim_contribution{$pair}{$term}=$file_term{$file1}{$term}*$idf{$term}**2*$file_term{$file2}{$term};
	    }
	}
    }
    $sum/=($doc_weight{$file1}*$doc_weight{$file2});

    return $sum;
}


sub compute_doc_weights {
    my($idf_param)=@_;

    if($idf_param eq 'global') {
	foreach my $file (keys %file_term) {
	    $doc_weight{$file}=0;
	    foreach my $term (keys %{ $file_term{$file} }) {
		$doc_weight{$file}+=($file_term{$file}{$term}*$idf{$term})**2;
	    }
	    
	    $doc_weight{$file}=sqrt($doc_weight{$file});
	}
    }
}




sub compute_centroid {
    my($i,$idf_param,$centroid_pointer,$centroid_weight_pointer,$point_centroid_matrix)=@_;

    my $no_docs=0;
    foreach my $file (keys %{ $cluster{$i} }) {
	foreach my $term (keys %{ $file_term{$file} }) {
#	    print STDERR "cluster{$i}{$file}{$term}=$name_file_term{$name}{$file}{$term}\n";
	    $$centroid_pointer{$i}{$term}+=$file_term{$file}{$term};
	}
    }
    foreach my $term (keys %{ $$centroid_pointer{$i} }) {
	$$centroid_pointer{$i}{$term}/=(keys %{ $cluster{$i} });
    }

    #limit centroid to top $max_centroid_terms terms
    my $term_counter=0;
    foreach my $term (sort { -1*($$centroid_pointer{$i}{$a}*$idf{$a}<=>$$centroid_pointer{$i}{$b}*$idf{$b} )} (keys %{ $$centroid_pointer{$i} })) {
	if($term_counter>$max_centroid_terms) {
	    delete($$centroid_pointer{$i}{$term});
	}
	$term_counter++;
    }

    #compute the document weight (for length normalization) for centroid:
    $$centroid_weight_pointer{$i}=0;
    if($idf_param eq 'global') {
	foreach my $term (keys %{ $$centroid_pointer{$i} }) {
	    $$centroid_weight_pointer{$i}+=($$centroid_pointer{$i}{$term}*$idf{$term})**2;
	}
    }
    $$centroid_weight_pointer{$i}=sqrt($$centroid_weight_pointer{$i});    
}


sub cluster_dist {
    my($strategy,$i,$j,$point_centroid_matrix)=@_;

    if($strategy eq 'single') {
	my $min_dist=1;
	foreach my $w1 (keys %{ $cluster{$i} }) {
	    foreach my $w2 (keys %{ $cluster{$j} }) {
#		print STDERR "sim_matrix{$name}{$w1}{$w2}=$sim_matrix{$name}{$w1}{$w2}\n";
		if(defined($sim_matrix{$w1}{$w2}) && $sim_matrix{$w1}{$w2}<=$min_dist) {
		    $min_dist=$sim_matrix{$w1}{$w2};
		}
	    }
	}
	return $min_dist;
    } elsif($strategy eq 'complete') {
        my $max_dist=-1;
        foreach my $w1 (keys %{ $cluster{$i} }) {
            foreach my $w2 (keys %{ $cluster{$j} }) {		
                if(defined($sim_matrix{$w1}{$w2}) && $sim_matrix{$w1}{$w2}>$max_dist) {
                    $max_dist=$sim_matrix{$w1}{$w2};
                }
            }
        }
        $max_dist=10000 if($max_dist==-1);
        return $max_dist;
    }
}


sub fill_point_centroid_matrix {
    my($centroid_id,$idf_param,$point_centroid_matrix)=@_;
    foreach my $file (keys %{ $cluster{$centroid_id} }) {       
	$$point_centroid_matrix{$centroid_id}{$file}=(1-&sim_cosine_point_centroid($idf_param,$file,$centroid_id));
    }
}


sub sim_cosine_centroid {
    my($idf_param,$centroid_i,$centroid_j)=@_;
    my $sum=0;

    if ($idf_param eq 'global') {
	foreach my $term (keys %{ $centroids{$centroid_i} }) {
	    if(defined($centroids{$centroid_j}{$term})) {
		$sum+=$centroids{$centroid_i}{$term}*$centroids{$centroid_j}{$term}*$idf{$term}**2;
#		print STDERR "sum+=centroids{$centroid_i}{$term}=$centroids{$centroid_i}{$term} + centroids{$centroid_j}{$term}=$centroids{$centroid_j}{$term}\n";
	    }
	}
    }
    $sum/=($centroid_weights{$centroid_i}*$centroid_weights{$centroid_j});
    return $sum;
}

sub sim_cosine_point_centroid {
    my($idf_param,$file,$centroid_id)=@_;
    my $sum=0;
    if($idf_param eq 'global') {
	foreach my $term (keys %{ $file_term{$file} }) {
	    if(defined($centroids{$centroid_id}{$term})) {
		$sum+=$file_term{$file}{$term}*$idf{$term}**2*$centroids{$centroid_id}{$term};
	    }
	}
    }
    $sum/=($doc_weight{$file}*$centroid_weights{$centroid_id});
    return $sum;
}





sub naive_tokenize {
    my($line)=@_;
    $line=~s/([\,\(\)\!\:\"\'\[\]\{\}\+\?\>\<\_\#\|\=])/ $1 /g;
    $line=~s/\.\.\./ ... /g;
    $line=~s/([^ ])\. /$1 . /g;
    $line=~s/([^ ])\.$/$1 ./g;
    $line=~s/ +/ /g;
    $line=~s/^[\s\t]*(.*?)[\s\t]$/$1/;
    return $line;
}

sub normalize_word {
    my($word);

    ($word)=@_;

    $word=~tr/A-Z/a-z/;
    $word=~s/[\.\,\:\;\'|'\+\-\!\?\(\)\[\]\/\\\|\{\}\_\*\&\@\!\`\"\<\>\s\t\#]//g;
    $word=~s/[0-9]//g;

    if(!defined($stop_words{$word})) {
	return $word;
    } else {
	return "";
    }
}
