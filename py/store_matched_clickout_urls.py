#!/usr/bin/python

from BeautifulSoup import BeautifulSoup
import get_queries
import MySQLdb
import MySQLdb.cursors
import util
import re

def _connect():
	return MySQLdb.connect (host = "qassir",
						   user = "bogomil",
						   passwd = "wieowie",
						   db = "people",
                           cursorclass = MySQLdb.cursors.DictCursor)

def _get_clickouts(conn, first_name, last_name):
    cursor = conn.cursor()
    cursor.execute("select c.ClickID as ClickID, c.type as type, c.URL as URL from pilot_train_val_test_clickouts as c where \
            c.FirstName = %s and \
            c.LastName = %s;", (first_name, last_name))
    clickouts = cursor.fetchall()
    cursor.close()
    return clickouts

def _google_de_cache(conn, url):
    if re.search(r'search\?q=cache', url) is None:
        return url
    else:
        cursor = conn.cursor()
        cursor.execute("select translated_URL from google_cached_urls_to_translate where URL = %s", (url,))
        res = cursor.fetchone() # translated_url may be an empty string.
        cursor.close()
        if res is None: # empty result set, not supposed to happen, but ala.
            translated_url = ''
        else:
            translated_url = res['translated_URL']
        return translated_url


def _store_matched_clickout(conn, first_name, last_name, clickout, normalized_url):
    cursor = conn.cursor()
    cursor.execute("call store_matched_clickout_url('%s', '%s', '%s', '%s', '%s')" %
            (first_name, last_name, clickout['type'], clickout['ClickID'], normalized_url))
    cursor.close()

def _match_clickout_urls(conn, first_name, last_name):
    # get clickouts
    clickouts = _get_clickouts(conn, first_name, last_name)
    for clickout in clickouts:
        if clickout['type'] == 'google':
            clickout['URL'] = _google_de_cache(conn, clickout['URL'])
        normalized_url = util.normalize(clickout['URL'].strip())
        _store_matched_clickout(conn, first_name, last_name, clickout, normalized_url)

def _main():
    conn = _connect()
    queries = get_queries.get_queries()
    for query in queries:
        _match_clickout_urls(conn, query['FirstName'], query['LastName'])
    conn.close()


if __name__ == "__main__":
    _main()
