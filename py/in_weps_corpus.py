
# the final crawl is a mixture of two crawls:
# 1) wget for non social media profiles
# 2) wget-crowbar crawl for social media profiles (crowbar is an ajax scraper
# from the simile project, MIT)

# this python module gives you for a range of queries the documents that are in the final merged crawl.
# the module makes availabe three functions:
# 
# 1) get_wget_docs_per_query
# 2) get_crowbar_docs_per_query
# 3) in_weps_corpus, this one returns 

# NB: note that with a crawl we mean the documents that were ultimately selected to be in the corpus
# that was annotated by annotators. 
def get_wget_docs_per_query(conn, query_id_from = 3, query_id_to = 45):
    cursor = conn.cursor()
    cursor.execute("select s.firstname, s.lastname, s.query_id, s.id \
from search_results as s, normalized_url as n where s.query_id >= %s and s.query_id <= %s \
and s.name_found_in_doc_2 = 'yes' and s.id = n.id and n.mimetype like 'text/html%%' group by s.query_id, s.id \
having \
sum(if(s.type in ('hyves', 'facebook', 'linkedin', 'twitter', 'myspace') or \
s.url like '%%hyves.nl%%' or \
s.url like '%%facebook.com%%' or \
s.url like '%%linkedin.com%%' or \
s.url like '%%twitter.com%%' or \
s.url like '%%myspace.com%%', 1, 0)) = 0 order by s.query_id, s.id",
            (query_id_from, query_id_to))
    srch_results = cursor.fetchall()
    cursor.close()
    return srch_results

def get_crowbar_docs_per_query(conn, query_id_from = 3, query_id_to = 45):
    cursor = conn.cursor()
    cursor.execute("select firstname, lastname, query_id, id \
from search_results where query_id >= %s and query_id <= %s \
and name_found_in_crowbarred_doc_2 = 'yes' group by query_id, id \
having \
sum(if(type in ('hyves', 'facebook', 'linkedin', 'twitter', 'myspace') or \
url like '%%hyves.nl%%' or \
url like '%%facebook.com%%' or \
url like '%%linkedin.com%%' or \
url like '%%twitter.com%%' or \
url like '%%myspace.com%%', 1, 0)) > 0 order by query_id, id",
            (query_id_from, query_id_to))
    srch_results = cursor.fetchall()
    cursor.close()
    return srch_results

# call this once and get a set with all (query_id, doc_id) tuples that are in the weps corpus for the given query
def in_weps_corpus(conn, query_id_from = 3, query_id_to = 45):

    query_doc_tuples = set([])

    for row in get_wget_docs_per_query(conn, query_id_from, query_id_to):
        query_doc_tuples.add((row['query_id'], row['id']))

    for row in get_crowbar_docs_per_query(conn, query_id_from, query_id_to):
        query_doc_tuples.add((row['query_id'], row['id']))

    return query_doc_tuples
