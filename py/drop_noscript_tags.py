#!/usr/bin/python

# Warning destructive - don't call on a master copy of the crawls directory!
# drop_noscript_tags dir_with_links

from subprocess import Popen, PIPE
from BeautifulSoup import BeautifulSoup
import sys
import os

if len(sys.argv) < 2:
  print "Warning destructive - don't call on a master copy of the crawls directory!"
  print "drop_noscript_tags dir_with_links"
  sys.exit(2)

basedir = (sys.argv[1])

def _doqry(qry):
  print qry
  qrydir = basedir + "/" + qry  
  urls = os.listdir(qrydir)
  for url in urls:
    #print url
    urlfile = qrydir + "/" + url
    tocall = ["/usr/bin/file" , "--brief", "--mime", "--dereference",  urlfile]
    x = Popen(tocall, stdout=PIPE)
    mime = x.stdout.readline()
    #print mime
    if mime.startswith("text/html") or mime.startswith("application/xhtml+xml"):
      contents = open(urlfile, "r").read()
      try:
        soup = BeautifulSoup(contents)
      except Exception, e:
        if contents.find("noscript") >= 0:
           print "WARNING couldn't beautiful soup " + url + " but there is a noscript tag inside"
        else:
           print "Couldn't beautiful soup " + url
        continue
      noscripts = soup.findAll(lambda t: t.name=="noscript")
      [el.extract() for el in noscripts]
      out_file = open(urlfile, "w")
      out_file.write(soup.prettify())
      
  
  
qries = os.listdir(basedir)
for q in qries:
   #if q == "10_Marlies_de_Boer":
     _doqry(q)


  
  
