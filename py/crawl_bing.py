#!/usr/bin/python

#$import xml.parsers.expat
from BeautifulSoup import BeautifulSoup
import xml.dom.minidom
import urllib # for quote function
import urllib2 # for urlopen
#from crawl_google import _normalize_url
#from crawl_google import _connect
from pprint import pprint
from util import normalize
from get_queries import get_queries
from store_urls import store_urls
import MySQLdb
import random
import time
import sys


#Sample: http://api.bing.net/xml.aspx?AppId=2C09C5057AA6E6986E526CBEB2EA00AB81C6CEF7&Query="Jan+Vissers"+++language:nl&Sources=web&Web.Count=50&mkt=en-US

appid="2C09C5057AA6E6986E526CBEB2EA00AB81C6CEF7"


def _query_url(firstname, lastname):
    firstname = firstname.replace('"', '').strip()
    lastname = lastname.replace('"', '').strip()
    words = firstname.split() + lastname.split()
    query_string = '+'.join(words)
    query_string += '%22++loc:nl+language:nl&mkt=nl-NL'
    url = "http://www.bing.com/search?" \
        + "q=%22" + query_string
    return url


def _query_url_api(firstname, lastname):
    # This URL was created from the URL that wieowie.nl offers
    # when you click on 'more Bing results'
    # It passes the correct parameters to the api so the
    # request to the api with this query matches the results
    # returned by the clickout on 'more Bing results'

    firstname = firstname.replace('"', '').strip()
    lastname = lastname.replace('"', '').strip()
    words = firstname.split() + lastname.split()
    query_string = '+'.join(words)
    query_string = "\"" + query_string + "\""
    query_string += '++loc:nl+language:nl'
    url = "http://api.bing.net/" \
        + "xml.aspx?" \
        + "AppID=" + appid \
        + "&Query=" + query_string \
        + "&Sources=web&Web.Count=50&mkt=nl-NL"
    return url


def _get_page(url):
    content = ""
    try:
        content = urllib2.urlopen(url).read()
    except Exception, e:
        print "Exception!!!"
        sys.stderr.write(e)
        sys.stderr.write('\n')
        time.sleep(66)
        content = urllib2.urlopen(url).read()
    return content

    test_page = \
"""<?xml version="1.0" encoding="utf-8" ?>
  <?pageview_candidate ?>
 <SearchResponse xmlns="http://schemas.microsoft.com/LiveSearch/2008/04/XML/element" Version="2.2">
 <Query>
  <SearchTerms>"Jan Vissers" language:nl</SearchTerms>
  </Query>
 <web:Web xmlns:web="http://schemas.microsoft.com/LiveSearch/2008/04/XML/web">
  <web:Total>179000</web:Total>
  <web:Offset>0</web:Offset>
 <web:Results>
 <web:WebResult>
  <web:Title>JanVissersWeer.nl - weerbericht</web:Title>
  <web:Description>Dagelijks het weer, weerkaarten, de weersvoorspelling voor vandaag en morgen: vooruitzichten tot 6 dagen vanaf nu.</web:Description>
  <web:Url>http://www.janvissersweer.nl/</web:Url>
  <web:CacheUrl>http://cc.bingj.com/cache.aspx?q=%22jan+vissers%22&d=4633668595286707&w=a7261124,864e09d1</web:CacheUrl>
  <web:DisplayUrl>www.janvissersweer.nl</web:DisplayUrl>
  <web:DateTime>2011-03-15T19:04:00Z</web:DateTime>
  </web:WebResult>
   <web:WebResult>
  <web:Title>JanVissersWeer.nl - weerbericht</web:Title>
  <web:Description>Dagelijks het weer, weerkaarten, de weersvoorspelling voor vandaag en morgen: vooruitzichten tot 6 dagen vanaf nu.</web:Description>
  <web:Url>http://janvissers.hyves.nl/</web:Url>
  <web:CacheUrl>http://cc.bingj.com/cache.aspx?q=%22jan+vissers%22&d=4633668595286707&w=a7261124,864e09d1</web:CacheUrl>
  <web:DisplayUrl>www.janvissersweer.nl</web:DisplayUrl>
  <web:DateTime>2011-03-15T19:04:00Z</web:DateTime>
  </web:WebResult>
  </web:Results>
  </web:Web>
  </SearchResponse>
"""

def _parse_search_results(page):
    soup = BeautifulSoup(page)
    ts = soup.findAll(lambda t: t.name=="a" and t.has_key("onmousedown") and t.parent.name=="h3")
    return [ t['href'] for t in ts]
    return res


def _parse_search_results_api(page):
    res = []
    dom = xml.dom.minidom.parseString(page)
    return [ x.firstChild.data for x in dom.getElementsByTagName("web:Url")]
    return res

# keep search results in case we would want to get the snippets later.  to
# retrieve these, we would only need the query_id and the rank of each search
# result.
def _store_bing_search_result_xml(page, query_id):
    try:
        f = open('../../exp/bing_search_results_per_query/%s.xml' % (query_id,), 'w')
    except IOError, e:
        sys.stderr.write('Error: Exception caught \
while storing bing search results xml page\n')
        sys.stderr.write(str(e) + '\n')
        sys.exit(2)
    else:
        f.write(page)
        f.close()

def _fetch_urls(query_record):
    firstname = query_record['FirstName']
    lastname = query_record['LastName']
    query_url = _query_url_api(firstname, lastname)
    print query_url
    page = _get_page(query_url)
    _store_bing_search_result_xml(page, query_record['idx'])
    urls = _parse_search_results_api(page)
    #print urls
    return urls

if __name__ == "__main__":
    print "Starting bing crawling..."
    print "############################"
    print "############################"

    #firstname, lastname = ("Jan", "Hollander")
    #urls = _fetch_urls("Wesley", "Schuts")
    #print urls
    queries = get_queries()
    #print queries
    for q in queries:
        urls = _fetch_urls(q)
        n_urls = []
        for u in urls:
            n_urls.append(normalize(u))
        #pprint(urls)
        #pprint(n_urls)
        store_urls(q['FirstName'], q['LastName'], "bing", urls, n_urls)
        time.sleep(23 + random.randrange(1,6,1))
