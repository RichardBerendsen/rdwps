#!/bin/bash

# Assumes the script resides alongside the other preprocessing scripts.
# First argument a weps crawls directory.
# Second argument an empty results directory.
# 

print_usage() {
    cat 1>&2 <<EOF
Usage: 
$1 texts_dir empty_dir
EOF
}

if [ "$#" -ne "2" ] || [ ! -d "$1" ] || [ ! -d "$2" ] || [ "$(ls -A "$2")" ]
then
    print_usage $0
    exit 2
fi

texts_dir=$(echo "$1" | sed 's+/*$++')
result_dir=$(echo "$2" | sed 's+/*$++')
script_dir="$(dirname "$0")"

mkdir $result_dir/stemmed
#mkdir $result_dir/normalized
mkdir $result_dir/matrices_from_stemmed
#mkdir $result_dir/matrices_from_normalized

echo "Tokenizing with generic tokenizer and stemming.. " 1>&2
$script_dir/stem_htmls_after_w3m.sh $texts_dir $result_dir/stemmed

#echo "Normalizing words.." 1>&2
#$script_dir/normalize_words.sh $result_dir/stemmed $result_dir/normalized

echo "Creating matrices from stemmed documents" 1>&2
$script_dir/../py/create_matrices.py \
"--input-dir=$result_dir/stemmed" \
"--output-dir=$result_dir/matrices_from_stemmed" \
"--occurrence-limit=5" \
"--logged-tfs" \
"--run-name=wieowie${which_weps}_glob_log_tf_occ5_from_stemmed" \
"--feature-vector-creator=tf_idf_vectors_global"

#echo "Creating matrices from stemmed documents" 1>&2
#$script_dir/../py/create_matrices.py \
#"--input-dir=$result_dir/normalized" \
#"--output-dir=$result_dir/matrices_from_normalized" \
#"--occurrence-limit=5" \
#"--logged-tfs" \
#"--run-name=weps_${which_weps}_glob_log_tf_occ5_from_normalized" \
#"--feature-vector-creator=tf_idf_vectors_global"
