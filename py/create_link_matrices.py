#!/usr/bin/python

import db
import sys
import os
import serializer
import getopt
import store_run_weps
import store_run
import re
import extractor

def create_link_matrices(base_dir, target_dir, dataset, allow_same_platform):
  print allow_same_platform
  if os.listdir(target_dir):
    raise Exception("Target directory not empty: " + target_dir)
  conn = db.connect()
  cur = conn.cursor()
  abs_base = os.path.abspath(base_dir)
  abs_targ = os.path.abspath(target_dir)
  queries = os.listdir(abs_base)
  for q in queries:
    print q
    source_dir_for_feature_vectors= abs_base + "/" + q
    target_dir_for_matrices= abs_targ + "/" + q
    os.mkdir(target_dir_for_matrices)
    qid = ""
    if dataset == "weps2":
      qid = str(store_run_weps.get_query_id(q))
    else:
      qid = str(store_run.get_query_id(q))
    docs_as_urls=serializer.get_query_docs_as_urls(source_dir_for_feature_vectors)
    feature_matrix = []
    keys = sorted(docs_as_urls.keys())
    query_descriptors = []
    labels = []
    sim_matrix = [[0 for i in keys] for i in keys]
    for k in keys:
      rank = k[k.index("|") + 1:]
      labels.append(rank)
      rank_nopad = re.sub("^0+", "", rank)
      normalized_url = rank_nopad # for wieowie this is the scheme we use
      if dataset == "weps2":
        cur.execute("select normalized_url from normalized_url_weps2 where query_id=" +qid + " and rank=" + rank_nopad)
        normalized_url = cur.fetchone()['normalized_url']
      else:
        cur.execute("select normalized_url from normalized_url where id=" + rank_nopad )
        normalized_url = cur.fetchone()['normalized_url']
      outlinks = docs_as_urls[k]
      type = extractor._infer_type_also_meta_from_string(normalized_url)
      query_descriptors.append([rank, normalized_url, outlinks, type])
    for desc in query_descriptors:
      for other_desc in query_descriptors:
        if other_desc != desc:
          #if allow_same_platform or (other_desc[3] != desc[3] or other_desc[3] == "none" or desc[3] == "none"):
              ix1 = labels.index(other_desc[0])
              ix2 = labels.index(desc[0])
              sim_matrix[ix1][ix1] = 1
              sim_matrix[ix2][ix2] = 1
              if desc[1] in other_desc[2] and (allow_same_platform or desc[3] != other_desc[3] or desc[3] == "none"):
                #print "BINGOOOO", q
                sim_matrix[ix1][ix2] = 1
                sim_matrix[ix2][ix1] = 1
                #print other_desc[0], " links to ", desc[0]
    serializer.serialize_similarity_matrix( target_dir_for_matrices + "/similarity_matrix" , sim_matrix, labels)

if __name__ == "__main__":
  usage="""Usage:
     create_link_matrices_weps2
        --input-dir=<input-dir>
        --output-dir=<output-dir>
        --dataset=weps2|wieowie
        [--disallow-same-platform]"""
  try:
    optlist, args = getopt.getopt(sys.argv[1:], '',['input-dir=', 'output-dir=', 'dataset=', 'disallow-same-platform'])
  except getopt.GetoptError, err:
    print usage
    print err
    sys.exit(2)
  allow_same_platform = True

  for o, a in optlist:
    if o == "--input-dir":
      source_dir = os.path.abspath(a)
    elif o == "--output-dir":
      target_dir = a
    elif o == "--dataset":
      dataset = a
    elif o == "--disallow-same-platform":
      allow_same_platform = False
  if dataset == "weps2":
    create_link_matrices(source_dir, target_dir, "weps2", allow_same_platform)
  elif dataset == "wieowie":
    create_link_matrices(source_dir, target_dir, "wieowie", allow_same_platform)
  else:
    print usage
    raise Exception("Dataset unsupported.")
 

