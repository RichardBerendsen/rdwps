firstname	lastname	click_entropy	hyves_profiles_clicked	sessions	clicks
Bart	Zeilstra	2.61767	3	13	30
Chantal	Mulder	4.20072	14	16	64
Linda	"de Jong"	4.92565	11	16	40
Monique	"de Groot"	4.17868	9	13	32
Laura	"de Boer"	4.22791	11	15	44
Chantal	Jansen	4.91716	11	19	53
Erik	Holland	4.10875	9	20	64
Martin	"van den Berg"	4.24737	10	12	40
Annelies	Rozema	2.15737	3	38	104
Lisanne	"de Jong"	4.24822	10	12	40
Danny	"Van Oosten"	2.82001	4	12	29
Petra	Visser	4.35626	8	14	38
Monique	Aarts	3.62165	10	12	40
Annemarie	Mulder	3.92381	11	15	46
Vincent	Tabak	2.11717	3	41	95
Ron	Pelgrim	3.08721	3	12	31
Helga	"van Leur"	3.08311	3	12	41
Anton	"van Haren"	3.62084	4	20	55
Jack	"van Eijk"	3.14594	4	16	55
Antonie	Kamerling	3.38069	5	226	611
Heleen	"de Geest"	2.62292	5	25	63
Joyce	Exalto	2.17263	3	233	533
Monique	Smeets	3.9935	12	12	36
Marlou	Bakker	2.09714	3	86	214
Chantal	Visser	5.05155	13	12	41
Marcel	"van Loon"	4.23733	9	12	36
Bianca	Meijer	4.23321	10	13	42
Monique	Pisters	3.15353	3	19	48
Sandra	Jansen	4.71457	13	22	70
Esther	"de Jong"	5.03196	12	13	45
Monique	Mol	4.24056	12	18	65
Monique	Jacobs	4.5928	14	16	51
Bente	"Van Den Berg"	4.75068	4	26	122
Hans	Kuipers	2.60452	8	17	36
Marcel	"Van Herpen"	2.7013	3	15	38
Tatjana	Liefhebber	1.47353	3	159	335
Bianca	Jansen	4.54035	11	12	33
Mirjam	Smit	4.30787	16	13	53
Saskia	"de Vries"	5.09777	12	19	49
Martin	"van Vliet"	3.68163	8	12	31
Dean	Saunders	3.56843	13	69	178
Karin	"de Jong"	5.04135	14	21	59
Gerard	"de Boer"	6.19592	4	29	96
Babette	"van Veen"	4.17013	7	14	47
Jolanda	"van Dijk"	4.73485	9	18	63
Linda	Kok	3.83628	12	12	34
Peter	"van der Velden"	4.10371	7	13	39
Kiki	Rutten	2.81316	6	28	75
Suzanne	"van den Berg"	3.73979	12	12	38
Peter	"de Vries"	5.99211	9	24	56
Monique	Janssen	4.8438	10	13	34
Sietske	Hoekstra	3.61457	10	110	304
Ingrid	Bakker	4.50937	13	18	46
Jolanda	"de Vries"	4.89535	11	20	50
Petra	Hoekstra	3.73195	10	16	48
Monique	Willems	4.30618	12	15	46
Jeroen	Dekkers	5.49031	5	17	42
Joris	"van Leeuwen"	3.86373	10	17	57
Carla	Mulder	4.49091	11	18	55
Peter	"van der Meer"	4.53784	14	13	44
Peter	Bruggeman	3.58584	6	12	40
Tanja	"de Jong"	3.89613	11	17	47
Saskia	Visser	3.90831	7	12	34
Marion	Mulder	3.66703	8	15	45
Peter	"van Mourik"	4.461	10	12	39
Kim	Bakker	4.95482	11	15	36
Marjolein	"van der Linden"	3.71618	11	15	74
Jelle	"de Jong"	4.56626	9	13	33
Anita	"de Vries"	4.06797	9	12	29
Lonneke	Meijer	3.83514	7	12	35
Marianne	"van Dijk"	5.07809	8	15	48
Sander	Jacobs	4.03184	11	13	28
Monique	"de Jong"	4.42834	12	13	30
Nathalie	Hendriks	3.91153	11	13	52
Peter	"van Dijk"	4.85622	6	16	50
Inge	Schrama	3.90665	11	33	98
Jens	Bakker	4.3975	12	12	69
Claudia	Visser	4.62785	11	12	41
Corine	Bos	3.56035	10	13	41
Patricia	Paay	3.37081	11	15	46
Hans	"de Groot"	4.80516	13	15	50
Maxime	Verhagen	3.58537	3	12	30
Kim	Vermeulen	4.13376	10	17	46
Yvonne	"van Veen"	3.87636	7	12	32
Marcel	"van Es"	4.12835	10	14	33
Martin	"van Kan"	2.02151	3	14	34
Joost	"van der Linden"	3.96032	7	12	36
Ingrid	Bosma	3.87737	12	17	62
Ben	"van Bokhoven"	2.83289	6	14	36
Sandra	"Van Beek"	4.36938	8	12	39
