"""
Cluster documents based on 'co-last-clicking' evidence obtained  from wieowie log files.

Usage: %s out_dir 

and in outdir the resulting partial clusterings will be stored, as well as the run.conf 
file that allows us to see what is going on.

a 'co-last-click' happens:

by a user: in all her interactions for all pairs of last-clicked docs of each platform.
in a session: in all its interactions for all pairs of last-clicked docs in each platform
in a search: in all its interactions for all pairs of last-clicked docs in each platform.
"""

import db
import operator
import idiom
import sys
import hac
import util
import store_run
from optparse import OptionParser
import get_queries
import cluster_helper



def _get_actions(conn, conf, query, session):

    # depending on conf['group_actions_by'] we have a UserID, a SessionID or a SearchID in session
    # the actions we return are always just searches and clicks.
    # we also include 'more google results' like links, since these tell us that in fact no other
    # search result completely satisfied the information need for this platform, so that means no
    # other should probably be a clear winner, unless really a lot of dwelling time went there.

    # NB: this query will break if lastname or firstname contains single quotes.
    # However, we refrain from the nice sql preprocessing that cursor.execute does here because we
    # need to pass in a column name automatically and cursor.execute quotes strings.
    # this leaves us to escape mysql characters ourselves but we don't do this here because there
    # are no single quotes or other funky in our 120 names.
    # NB2: we don't quote the %s corresponding to the value of the
    # automatically inserted column name because the type of this column may be
    # either an int or  a string. So depending on this type, we quote the inserted value instead.
    def quote_if_str(str, type):
        if type == "str":
            return "'%s'" % str
        return str

    cursor = conn.cursor()
    sql = """(select Time as time, "search" as type, "" as url, 0 as urlid, "" as platform \
from Searches \
where `%s` = %s and FirstName = '%s' and LastName = '%s') \
union \
(select c.Time, "click", c.URL, c.urlid, c.type \
from Searches as s, selected_queries_clicks as c \
where \
s.%s = %s and s.SearchID = c.SearchID \
and s.FirstName = '%s' and s.LastName = '%s') \
order by Time asc""" % (conf['group_actions_by']['column'], \
quote_if_str(session['id'], conf['group_actions_by']['type']), \
query['FirstName'], query['LastName'], conf['group_actions_by']['column'], \
quote_if_str(session['id'], conf['group_actions_by']['type']), query['FirstName'], query['LastName']);
    cursor.execute(sql)
    actions = cursor.fetchall()
    cursor.close()
    return actions
    cursor.close()

def _find_co_last_clicked_in_session(conn, conf, query, session):
    # for each platform find if there is a 'clear winner':
    # a profile/search result that got most of the dwelling time.
    actions = _get_actions(conn, conf, query, session)

    last_action_per_platform = {}
    last_urlid_per_platform = set([])

    for action in actions: # just list the last action per platform we are interested in.
        if action['platform'] in conf['platforms']:
            last_action_per_platform[action['platform']] = action

    for platform, action in last_action_per_platform.iteritems():
        if action['urlid'] is not None and action['urlid'] != 0: # last action corresponds to a matched search result.
            last_urlid_per_platform.add(action['urlid'])

    return [i for i in last_urlid_per_platform]


def _find_sessions_with_cross_platform_coclicks(conn, conf, query):
    cursor = conn.cursor()
    sql = "select distinct convert(`%s`, char) as id from selected_queries_clicks where FirstName = '%s' and LastName = '%s'" % \
            (conf['group_actions_by']['column'], query['FirstName'], query['LastName'])
    cursor.execute (sql, )
    res = cursor.fetchall()
    cursor.close()
    return res

def _find_co_last_clicked(conn, conf, query):
    # build a similarity matrix for urlids, urlids should be added as they come in.
    # store it as a hash table
    co_last_clicks = {}

    sessions = _find_sessions_with_cross_platform_coclicks(conn, conf, query)
    print >> sys.stderr, "number of sessions: %s" % len(sessions)

    for session in sessions:
        co_last_clicked_urlids = _find_co_last_clicked_in_session(conn, conf, query, session)
        co_last_clicked_urlids.sort() # ensures that co_last_clicks will hold each tuple as a key in only one ordering.
        for i in range(0,len(co_last_clicked_urlids)):
            for j in range(i + 1, len(co_last_clicked_urlids)):
                key = (co_last_clicked_urlids[i], co_last_clicked_urlids[j])
                if co_last_clicks.has_key(key):
                    co_last_clicks[key] += 1
                else:
                    co_last_clicks[key] = 1

    # now use the minimum number of co_last_clicks to produce the similarity matrix (a binary one)
    sim = set([]) # set
    for key, val in co_last_clicks.iteritems():
        if val >= conf['min_sessions_with_colastclick']:
            sim.add(key)

    # for debugging, let's print this similarity matrix
    print >> sys.stderr, "Similarity matrix: %s" % sim
    return sim


# returns run_id of just stored run
def _store_run(conn, conf, out_dir):
    name = "co_last_clicked_grouped_by_%s" % (conf['group_actions_by']['column'],)
    description = '\n'.join(["%s : %s" % (key, value) for key, value in conf.iteritems()])
    params = (name, description)
    return cluster_helper.store_run_and_get_run_id(name, description, out_dir, conn=conn)

def _store_clustering(conn, conf, options, run_id, query, out_dir, query_dir):
    co_last_clicked = _find_co_last_clicked(conn, conf, query)
    # we coulds (ab)use this similarity matrix to implement single link clustering by hand.
    # just group all documents that are connected by ones to each other in a cluster. 
    # Or, equivalently: find connected components in the graph where edges corresponds to ones in the similarity matrix.
    # but we could also, to get a feel for bogomil his code, play with hac.py. It should have no problem with
    # this matrix.
    # prepare input for hac.py
    labels, sim_matrix  = cluster_helper.expand_sim_set(co_last_clicked)
    print >> sys.stderr, "Labels: %s\nExpanded similarity matrix:%s" % (labels, sim_matrix)
    if sim_matrix:
        clustering, clustering_with_labels = hac.cluster_single_link_maximum(sim_matrix, labels, 1.0)
        print >> sys.stderr, "Clustering: %s\nClustering with labels: %s" % (clustering, clustering_with_labels)
        if not options.dry_run:
            store_run.store_clustering_in_db(clustering_with_labels, run_id, query['idx'])
            cluster_helper.write_clustering_to_disk(clustering_with_labels, out_dir, query_dir)

def _main(args):
    progname = args[0]
    parser = OptionParser(usage=__doc__ % progname)
    parser.add_option("-d", "--dry-run", dest="dry_run", action="store_true", default=False)
    parser.add_option("-f", "--query_id_from", dest="query_id_from", action="store", type="int", default=3)
    parser.add_option("-t", "--query_id_to", dest="query_id_to", action="store", type="int", default=122)
    parser.add_option("-q", "--query_result_set", dest="query_result_set", action="store", type="choice", choices=['all','socialmedia','web'], default="all")
    options, args = parser.parse_args()

    if len(args) != 1:
        parser.print_help()
        return 2

    out_dir = args[0].rstrip('/')
    out_dir_ls = idiom.ls_dir_or_exit(out_dir)
    if out_dir_ls:
        print >> sys.stderr, "Error: output directory %s not empty" % (out_dir,)
        return 2

    conf = {
        "group_actions_by": {"column":"UserID", "type":"str"},
        #"group_actions_by": {"column":"SessionID", "type":"int"},
        #"group_actions_by": {"column":"SearchID", "type":"str"},
        "min_sessions_with_colastclick" : 2,
    }
    if options.query_result_set == "all":
        conf["platforms"] = set(["hyves", "facebook", "linkedin", "twitter", "myspace", "google", "yahoo", "bing"])
    elif options.query_result_set == "socialmedia":
        conf["platforms"] = set(["hyves", "facebook", "linkedin", "twitter", "myspace"])
    elif options.query_result_set == "web":
        conf["platforms"] = set(["google", "yahoo", "bing"])

    conn = db.connect()
    if not options.dry_run:
        run_id = _store_run(conn, conf, out_dir)
    queries = get_queries.get_queries(options.query_id_from, options.query_id_to)
    for query in queries:
        query_dir = util.get_dirname_from_query(query)
        _store_clustering(conn, conf, options, run_id, query, out_dir, query_dir)
    conn.close()

if __name__ == "__main__":
    sys.exit(_main(sys.argv))
