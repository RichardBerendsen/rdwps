"""
get topic stats from topic_stats, and output latex table.
"""

import db
import sys

sql = "select discarded_urls - discarded_profile_urls as discarded_web_docs, total_urls - profile_urls as web_docs, profile_urls, discarded_profile_urls from topic_stats order by total_urls - discarded_urls desc;"

res = db.select(sql)

cols = {}

for rec in res:
    for key in rec.iterkeys():
        if key in cols:
            cols[key].append(rec[key])
        else:
            cols[key] = [rec[key]]

for key, col in cols.iteritems():
    print "%s & %s\\\\" % (key, ' & '.join([str(i) for i in col]))

