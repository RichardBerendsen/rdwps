import get_queries
import MySQLdb
import MySQLdb.cursors
import sys
from in_weps_corpus import in_weps_corpus
import connect


def _in_weps_corpus_set(query_id, doc_id):
    if (query_id, doc_id) in _in_weps_corpus_set.weps_corpus:
        return 'yes'
    else:
        return 'no'

def _query_xml_attribute(firstname, lastname):
    firstname = firstname.replace('"', '').strip()
    lastname = lastname.replace('"', '').strip()
    words = firstname.split() + lastname.split()
    query_string = ' '.join(words)
    return query_string

def _query_xml_filename(firstname, lastname):
    firstname = firstname.replace('"', '').strip()
    lastname = lastname.replace('"', '').strip()
    words = firstname.split() + lastname.split()
    query_string = '_'.join(words)
    return query_string + ".clust.xml"

def _get_docs(query):
    conn = connect.connect()
    cursor = conn.cursor()
    cursor.execute("select s.name_found_in_doc, n.* from normalized_url as n, search_results as s where n.id = s.id and s.query_id = %s group by n.id order by n.id asc", (query['idx'],))
    res = cursor.fetchall()
    cursor.close()
    conn.close()
    return res

def _get_xml_doc(query_id, doc):
    xml = """
    <doc rank=\"%(normalized_url_id)s\" title=\"\" url=\"\" mimeType=\"%(mimetype)s\" inWepsCorpus=\"%(in_weps_corpus)s\">
        <snippet></snippet>
    </doc>""" % {"normalized_url_id" : doc['id'], "mimetype" : doc['mimetype'], "in_weps_corpus" : _in_weps_corpus_set(query_id, doc['id'])}
    return xml

def _get_xml_docs(query):
    docs = [_get_xml_doc(query['idx'], doc) for doc in _get_docs(query)]
    xml = "".join(docs)
    return xml

def _write_metadata(query, dir):
    xml_docs = _get_xml_docs(query)
    if xml_docs != "":
        xml = """<corpus searchString=\"&quot;%s&quot;\"
                date=\"2011/04/18 00:00:00\" lang=\"nl\"
                totalResultsEstimation=\"0\">""" % (_query_xml_attribute(query['FirstName'], query['LastName']),)
        xml += xml_docs
        xml += "\n</corpus>\n"
        basename = _query_xml_filename(query['FirstName'], query['LastName'])
        f = open("%s/%s" % (dir, basename), 'w')
        f.write(xml)
        f.close()


if __name__ == "__main__":

    query_id_from = 3
    query_id_to = 45
    if len(sys.argv) != 4 and len(sys.argv) != 2:
        print >> sys.stderr, "Usage: %s dir_name_metadata [from to]" % sys.argv[0]
        print >> sys.stderr, "where from and to with from <= to is a query id range"
        sys.exit(2)
    dir = sys.argv[1].strip().rstrip('/')
    if len(sys.argv) == 4:
        query_id_from = sys.argv[2]
        query_id_to = sys.argv[3]
        queries = get_queries.get_queries(query_id_from, query_id_to)
    else:
        queries = get_queries.get_queries()

    conn = connect.connect()
    _in_weps_corpus_set.weps_corpus = in_weps_corpus(conn, query_id_from, query_id_to)
    conn.close()

    for q in queries:
        if q['annotated'] == 'yes':
            _write_metadata(q, dir)
