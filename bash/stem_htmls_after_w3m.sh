#!/usr/local/bin/bash

# Duplicates the structure with stemmed versions.
# Accepts absolute paths.

htmls_dir=$1
stemmed_htmls_dir=$2
script_dir="$( cd "$( dirname "$0" )" && pwd )"
lang=$3

if [ "$lang" == "" ]
then
  lang="dutch"
fi

if [ "$#" -lt "2" ]
then
    echo "Error, please provide an absolute htmls directory, an absolute empty directory and optionally a language - dutch or english (default dutch)."
    exit 2
fi
if [ ! -d $htmls_dir ]
then
    echo "Error, htmls dir path is not a directory"
    exit 2
fi

if [ ! -d $stemmed_htmls_dir ]
then
    echo "Error, mirror path is not a directory"
    exit 2
fi

if [ "$(ls -A $stemmed_htmls_dir)" ]
then
    echo "Error, mirror path is not empty"
    exit "2"
fi

echo $htmls_dir
echo $stemmed_htmls_dir
echo $script_dir

process_url() {
  dir=$1
  newdir=$2
  url=$3
  #echo $url
  #lines=$(cat $url | wc -l)
  #for (( i=0; i < $lines; i++ ))
  #do
  #  line=$(head -"$i" $url | tail -1)
  #  echo $line >> "$newdir"/"${url}"
  #done

  # preventing the whole mess with star expansion
  set -f
  #words=($(cat $url))
  if [ "$lang" == "english" ]
  then
    echo "Using english tokenizer."
    /home/ilps/smt/scripts/tokenizeE.pl "$url" "$newdir"/"$url".tok
  else
    echo "Using generic tokenizer."
    /home/ilps/smt/scripts/tokenizeANY.pl "$url" "$newdir"/"$url".tok
  fi
 
  words=($(cat "$newdir"/"$url".tok))
  rm "$newdir"/"$url".tok
  touch "$newdir"/"$url".temp
  for word in ${words[@]}
  do
    if [ "${#word}" -gt "1" ]
    then
      if [ "$normalize_words" == "true" ]
      then
        echo "$word" | "$script_dir"/normalize.pl  >> "$newdir"/"$url".temp
      else
        echo "$word" >> "$newdir"/"$url".temp
      fi
    fi
  done
  "$script_dir"/"$lang"_stemmer -i "$newdir"/"$url".temp -o "$newdir"/"$url"
  rm "$newdir"/"$url".temp
  #for word in ${words[@]}
  #do
  #  stemmed=$(echo "$word" | "$script_dir"/dutch_stemmer)
  #  echo $stemmed >> "$newdir"/"${url}"
  #done
  #enabling star expansion again
  set +f
}

process_qry_dir() {
    echo "$1"
    subdir=$htmls_dir"/""$1"
    newdir=$stemmed_htmls_dir"/""$1"

    mkdir $newdir
    cd $subdir
    for u in $(ls)
    do
      process_url $subdir $newdir $u
    done
    cd ..
}

for qry in $(ls "${htmls_dir}")
do
  process_qry_dir $qry
done



