# read from stdin an html document file and check whether or not the person name is in there.
import MySQLdb
import MySQLdb.cursors
import sys
import re

def _connect():
	return MySQLdb.connect (host = "qassir",
						   user = "bogomil",
						   passwd = "wieowie",
						   db = "people",
                           cursorclass = MySQLdb.cursors.DictCursor)

def _compile_regexp(firstname, lastname):
    firstname = firstname.replace('"', '').strip()
    lastname = lastname.replace('"', '').strip()
    words = firstname.split() + lastname.split()
    pattern = r'\s+'.join(words)
    return re.compile(pattern, re.MULTILINE | re.IGNORECASE)

def _get_query_details(id):
    conn = _connect()
    cursor = conn.cursor()
    cursor.execute("select * from selected_queries where idx = %s", (id))
    res = cursor.fetchone()
    cursor.close()
    conn.close()
    return res

def _store_name_found(query_id, normalized_url_id, found):
    if found:
        value = 'yes'
    else:
        value = 'no'
    conn = _connect()
    cursor = conn.cursor()
    cursor.execute("update search_results set name_found_in_doc = %s where query_id = %s and id = %s", (value, query_id, normalized_url_id))
    res = cursor.fetchall()
    cursor.close()
    conn.close()
    return res

if __name__ == "__main__":

    if len(sys.argv) != 3:
        sys.stderr.write("Usage: %s query_id doc_id\n" % (sys.argv[0],))
        sys.exit(2)

    query_id = sys.argv[1].strip()
    doc_id = sys.argv[2].strip()

    query = _get_query_details(query_id)
    doc = sys.stdin.read()

    prog = _compile_regexp(query['FirstName'], query['LastName'])
    match = prog.search(doc)
    if match is None:
        _store_name_found(query_id, doc_id, False)
    else:
        _store_name_found(query_id, doc_id, True)
