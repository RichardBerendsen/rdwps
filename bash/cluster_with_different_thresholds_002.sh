#!/usr/local/bin/bash

# Assumes the script resides alongside the other preprocessing scripts.
# First argument a weps crawls directory.
# Second argument an empty results directory.
# 

print_usage() {
    cat 1>&2 <<EOF
Usage: 
$1 matrices_dir empty_dir which_weps

where which_weps must be 1 (weps-1-test), 2 (weps-2) or 3 (weps-1-train)
EOF
}

if [ "$#" -ne "3" ] || [ ! -d "$1" ] || [ ! -d "$2" ] || [ "$(ls -A "$2")" ]
then
    print_usage $0
    exit 2
fi

if [ "$3" -ne "1" ] && [ "$3" -ne "2" ] && [ "$3" -ne "3" ]
then
    print_usage $0
    exit 2
fi

matrices_dir=$(echo "$1" | sed 's+/*$++')
result_dir=$(echo "$2" | sed 's+/*$++')
script_dir="$(dirname "$0")"

for threshold in "0.02" "0.04" "0.06" "0.08" "0.10" "0.12" "0.14" "0.16" "0.18" "0.20" "0.22" "0.24" "0.26" "0.28" "0.30" "0.32" "0.34" "0.36" "0.38" "0.40"
do
    mkdir "$result_dir/$threshold"
    python $script_dir/../py/cluster_with_text.py --which-weps $3 --similarity-threshold $threshold "$matrices_dir" "$result_dir/$threshold"
done
