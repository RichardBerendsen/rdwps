"""
create R data frame containing results of runs, with
- filtered out averages per run
- not considering the .eval file with averages of all systems

Usage: %s results_dir which_evaluator

where which_evaluator is either
 1 : the weps1 evaluator (wepsEvaluation.jar)
 2 : the weps2 evaluator (wepsScorer-weps2.jar)

They give the same results, good for us.
"""

import os
import sys
import idiom

def main(args):
    if len(args) != 3:
        print >> sys.stderr, __doc__ % args[0]
        return 2

    results_dir = args[1]
    which_evaluator = int(args[2])
    if which_evaluator != 1 and which_evaluator != 2:
        print >> sys.stderr, __doc__ % args[0]
        return 2

    files = idiom.ls_dir_or_exit(results_dir)

    results_files = [file for file in files if file.endswith(".eval")]

    # get header
    if results_files:
        f = open(os.path.join(results_dir, results_files[0]))
        header = f.readline().strip()
        print "run\t%s" % (header,)
        f.close()
    else:
        print >> sys.stderr, "No results files found, quitting"
        return 2
    for results_file in results_files:
        run = results_file[:-5]
        f = open("%s/%s" % (results_dir, results_file))
        lines = f.readlines()
        if len(lines) < 2:
            print >> sys.stderr, "No results in result file, skipping"
            continue
        for line in lines[1:]:
            if not line.startswith("Average"):
                print "%s\t%s" % (run, line.strip())

if __name__ == "__main__":
    sys.exit(main(sys.argv))
