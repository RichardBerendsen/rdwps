"""
contains class QueryEval, which can be used to evaluate a query WePS style
"""

import numpy as np
import os
import sys
import xml.dom.minidom as dom

def F(alpha, P, R):
    """
    returns F score, as described in WePS-2 (2009) overview paper
    """
    P = float(P)
    R = float(R)
    alpha = float(alpha)
    return 1 / ((alpha / P) + ((1 - alpha) / R))

class QueryEval:
    """
    Class to evaluate a query WePS style
    """

    def __init__(self, gold_truth_dir, gold_truth_query_file, run_dir, run_query_file):
        """
        Expects two files with an xml clustering, a gold truth and a run clustering.

        Returns B cubed precision, recall and their harmanic mean.
        """
        self.gold_truth_query_file = gold_truth_query_file
        self.run_query_file = run_query_file

        self.dom_gold_truth = dom.parse("%s/%s" % (gold_truth_dir, self.gold_truth_query_file))
        self.dom_run = dom.parse("%s/%s" % (run_dir, self.run_query_file))

        self.items_to_categories = self.get_items_to_entities(self.dom_gold_truth)
        self.items_to_clusters = self.get_items_to_entities(self.dom_run)

        self.gold_truth_doc_ids = self.get_doc_ids(self.dom_gold_truth)
        self.discarded_doc_ids = self.get_discarded_doc_ids(self.dom_gold_truth)

        # print to stderr any mismatches, and make sure that clustering holds only doc_ids from the ground truth.
        self.last_inserted_singleton_id = 0 # if we have to add singletons to the clustering, we need unique ids.
        self.doc_mismatches()

        # compute B-cubed precision and recall per doc
        self.doc_id_2_eval = self.compute_bcubed()

        # compute B-cubed precision and recall
        self.bep, self.ber, self.f05, self.f02 = self.compute_eval()

    def get_items_to_entities(self, dom):
        """Returns a dictionary with:
        key: doc_id
        value: set of entities this doc_id appears in
        """
        i_2_e = {}
        entities = dom.getElementsByTagName("entity")
        for e in entities:
            e_id = e.getAttribute("id")
            items = e.getElementsByTagName("doc")
            for i in items:
                i_id = i.getAttribute("rank")
                if not i_id in i_2_e:
                    i_2_e[i_id] = set([e_id])
                else:
                    i_2_e[i_id].add(e_id)
        return i_2_e

    def compute_bcubed(self):
        """
        Computes B-cubed precision and recall.
        """
        doc_id_2_eval = dict([[i, (0.0, 0.0)] for i in self.gold_truth_doc_ids])
        for i in self.gold_truth_doc_ids:
            summed_mult_prec = 0.0
            summed_mult_rec = 0.0
            items_sharing_clusters_with_i = 0
            items_sharing_categories_with_i = 0
            for j in self.gold_truth_doc_ids:
                clusters_containing_i_and_j = len(self.items_to_clusters[i] & self.items_to_clusters[j])
                categories_containing_i_and_j = len(self.items_to_categories[i] & self.items_to_categories[j])
                if clusters_containing_i_and_j:
                    items_sharing_clusters_with_i += 1
                    summed_mult_prec += min(clusters_containing_i_and_j, categories_containing_i_and_j) / float(clusters_containing_i_and_j)
                if categories_containing_i_and_j:
                    items_sharing_categories_with_i += 1
                    summed_mult_rec += min(clusters_containing_i_and_j, categories_containing_i_and_j) / float(categories_containing_i_and_j)
            mult_prec = summed_mult_prec / items_sharing_clusters_with_i
            mult_rec = summed_mult_rec / items_sharing_categories_with_i
            doc_id_2_eval[i] = (mult_prec, mult_rec)
        return doc_id_2_eval

    def compute_eval(self):
        bep, ber = zip(*self.doc_id_2_eval.values())
        avg_bep = np.mean(np.array(bep))
        avg_ber = np.mean(np.array(ber))
        f05 = F(0.5, avg_bep, avg_ber)
        f02 = F(0.2, avg_bep, avg_ber)
        return (avg_bep, avg_ber, f05, f02)

    def get_doc_ids(self, dom):
        """
        finds doc ids in clustering, but only when they are in <entity> tags,
        to avoid finding discarded documents in the gold truth file
        """
        doc_ids = set([])
        entity_es = dom.getElementsByTagName("entity")
        for e in entity_es:
            doc_es = e.getElementsByTagName("doc")
            for d in doc_es:
                doc_id = d.getAttribute("rank")
                if not doc_id in doc_ids:
                    doc_ids.add(doc_id)
        return doc_ids

    def get_discarded_doc_ids(self, dom):
        """
        finds doc ids in clustering, but only when they are in <discarded> tags
        """
        doc_ids = set([])
        discarded_es = dom.getElementsByTagName("discarded")
        for e in discarded_es:
            doc_es = e.getElementsByTagName("doc")
            for d in doc_es:
                doc_id = d.getAttribute("rank")
                if not doc_id in doc_ids:
                    doc_ids.add(doc_id)
        return doc_ids

    def add_singleton_clusters_to_clustering(self, missing_set):
        for i in missing_set:
            self.items_to_clusters[i] = set(["singleton_%s" % self.last_inserted_singleton_id])
            self.last_inserted_singleton_id += 1

    def delete_superfluous_clustered(self, superfluous_set):
        for i in superfluous_set:
            del self.items_to_clusters[i]
            self.run_doc_ids.remove(i)

    def doc_mismatches(self):
        """
        Checks doms of both gold_truth and run, and yells if there are any mismatches in docids.
        Sets self.run_doc_ids
        """
        self.run_doc_ids = self.get_doc_ids(self.dom_run)

        # print the doc_ids that are in the gold_truth but not in the run.
        print >> sys.stderr, "Checking if docids are missing in clustering..."
        in_gold_truth_but_not_in_run = (self.gold_truth_doc_ids | self.discarded_doc_ids) - self.run_doc_ids
        if in_gold_truth_but_not_in_run:
            print >> sys.stderr, """
            Warning: following ids in gold truth file: %s but not in run file %s:
            %s
            Will add singleton clusters with missing docids to clustering before evaluating
            """ % (self.gold_truth_query_file, self.run_query_file, in_gold_truth_but_not_in_run)
            self.add_singleton_clusters_to_clustering(in_gold_truth_but_not_in_run)

        # print the doc_ids that are not in the annotated query (clustered + discarded elements) but are in the run.
        print >> sys.stderr, "Checking superfluous docids in run..."
        in_run_but_not_in_corpus = self.run_doc_ids - (self.gold_truth_doc_ids | self.discarded_doc_ids)
        if in_run_but_not_in_corpus:
            print >> sys.stderr, """
            Warning: following ids in run file %s but not in the annotated query %s:
            %s
            Will delete superfluous docids from clustering before evaluating
            """ % (self.run_query_file, self.gold_truth_query_file, in_run_but_not_in_corpus)
            self.delete_superfluous_clustered(in_run_but_not_in_corpus)

        # print the doc_ids that are not in the gold_truth (excluding discarded elements) but are in the run.
        print >> sys.stderr, "Checking superfluous docids in run..."
        in_run_but_not_in_gold_truth = self.run_doc_ids - self.gold_truth_doc_ids
        if in_run_but_not_in_gold_truth:
            print >> sys.stderr, """
            Notice: following ids in run file %s but discarded in gold truth file %s:
            %s
            Will delete discarded docids from clustering before evaluating
            """ % (self.run_query_file, self.gold_truth_query_file, in_run_but_not_in_gold_truth)
            self.delete_superfluous_clustered(in_run_but_not_in_gold_truth)

    def print_detailed_results(self, path):
        """
        Writes detailed query results to a file.
        """
        f = open(path, 'w')
        print >> f, "doc_id\tBEP\tBER"
        for key, (bep, ber) in self.doc_id_2_eval.iteritems():
            print >> f, "%s\t%s\t%s" % (key, bep, ber)
        f.close()

##### end class QueryEval
