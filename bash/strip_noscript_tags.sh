#!/usr/local/bin/bash

# Assumes the script resides alongside the other preprocessing scripts.
# First argument a crawls directory (in the case of weps).
# It can also be a symbolic links directory, in the case of wieowie data.
# Second argument an empty results directory.

print_usage() {
    cat 1>&2 <<EOF
Usage: 
$1 weps_2_crawls_dir empty_dir
EOF
}

crawls_dir=$1
result_dir=$2
script_dir="$(dirname "$0")"

if [ "$#" -ne "2" ] || [ ! -d "$1" ] || [ ! -d "$2" ] || [ "$(ls -A "$2")" ]
then
    print_usage $0
    exit 2
fi

echo "$0: Crawls dir is ""$crawls_dir"
echo "$0: Result dir is ""$result_dir"
echo "$0: Script dir is ""$script_dir"

for query_dir in $(ls $crawls_dir)
do
    mkdir "$result_dir/$query_dir"
    for doc in $(ls "$crawls_dir/$query_dir")
    do
        python "$script_dir/../py/drop_noscript_tags_weps.py" < "$crawls_dir/$query_dir/$doc" > "$result_dir/$query_dir/$doc"
    done
done
