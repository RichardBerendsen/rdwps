#!/usr/bin/python

import re
import sys

def naive_tokenize(doc):
    """
    Returns a tokenized version of doc.
    doc is a string, a string is returned, only all tokens are separated by a space.
    """
    tokenized_doc = ""
    for line in doc.strip().split('\n'):
        #re_punct = re.compile(r'([,.\\()!:"\'[{}+?><_#|=]]|\.\.\.)', re.UNICODE)
        # note that the above line is identical save that the last ] is not escaped.
        # this did not work in our Python version, which is strange.
        re_punct = re.compile(r'((\.\.\.)|[,.\\()!:"\'[{}+?><_"|=\]])', re.UNICODE)
        re_space = re.compile(r'\s+', re.UNICODE)
        line = re.sub(re_punct, r' \1 ', line)
        line = re.sub(re_space, r' ', line)
        tokenized_doc += "%s\n" % line.strip()
    return tokenized_doc
