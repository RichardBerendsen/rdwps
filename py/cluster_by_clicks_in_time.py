"""Cluster documents that were clicked in the same burst.

Usage:

%s result_dir 

Will write resulting clusterings to result_dir (which should be empty)
These can then later be used as a partial starting clustering.

Options:

    -d  --dry-run 
        Do not store results in the database or on disk
"""

import db
import sys
import os
import get_queries
import idiom
import cluster_helper
import util
import hac
import store_run
import numpy as np
from optparse import OptionParser


def get_search_volume(conn, query):
   sql = "call trend_uid(%s, %s)"
   return db.select(sql, (query['FirstName'], query['LastName']), conn)

# search_volume is a resultlist coming from the stored proc. trend_uid
def get_bursts(conf, search_volume):
    # get a logical vector for the search volume: is it part of a burst yes or
    # no? Yes if it is higher than conf['stddevs_above_mean'] * stddev + mean,
    # we don't use a moving average first.  In addition to that, a burst day
    # has to have at least conf['burst_threshold'] users that day.
    y = np.array([float(x['mycount']) for x in search_volume])
    mean = np.mean(y)
    stddev = np.std(y, ddof=1)
    y_logical = [y_i > mean + conf['stddevs_above_mean'] * stddev and y_i >= conf['burst_threshold'] for y_i in y]

    # now start finding burst_intervals
    bursts = [] # a list of tuples (begin_day, end_day)
    days = [x['mydate'] for x in search_volume]
    tmp_burst_days = []
    for day, is_burst in zip(days, y_logical):
        if is_burst:
            tmp_burst_days += [day]
        else:
            if tmp_burst_days:
                bursts += [(tmp_burst_days[0], tmp_burst_days[-1])]
                tmp_burst_days = []
    return bursts

# burst is a tuple (begin_day, last_day) we get get the docs that were the
# 'last click' in a search during this burst.  the idea is that the last
# clicked documents have a high chance of being relevant (like in the cascade
# model). And then the idea of looking at clicks in a burst is that people are
# likely to be searching for the same person with this name.
def docs_clicked_last(query, burst, conn):

    # we have to find the last click for each search query.
    # this is a within-group (SearchID) aggregrate (find last click) problem, see
    # http://www.artfulsoftware.com/infotree/mysqlquerytree.php
    # for a nice treatment: navigate to Aggregates->Within-group aggregates
    # Note that there are 13 searches where two clicks were made in the same second.
    # Only in two of these cases did these clicks land on different urls, so we'll just
    # add them all.
    # 
    # The beginning time of a search is used to bin the search in days. This is the time
    # of the first search. So a click that is on the next day may still fall on the previous
    # day for our purposes if the search was made on the previous day. 
    # This adds just a bit of complexity to the query and is the reason I went for the
    # solution where we first group the results in a subquery. And this subquery is then
    # moved to the from clause for performance reasons.

    # We also have to have matched
    # the last click to a crawled search result that was in the corpus for it
    # to be useful (urlid != 0) 
    # But we have to exclude these urls only after establishing they were indeed the last,
    # otherwise we would include urls that were not the last click.

    # For now we care only for the distinct urlids that were clicked

    # XXX: hrmh by not selecting the urls that were not matched we discard later interactions:
    # the clicks that we select are not always really the last clicks.
    sql = """\
select urlid, count(*), group_concat(distinct type separator ':') as types from selected_queries_clicks as c1 join \
(select c.SearchID as SearchID, max(c.Time) as time_of_last_click \
from Searches as s, selected_queries_clicks as c \
where s.LastName = %s and s.FirstName = %s and \
s.SearchID = c.SearchID and s.Time >= %s and s.Time <= %s \
group by c.SearchID) as c2 \
on c1.SearchID = c2.SearchID and c1.Time = c2.time_of_last_click \
where c1.urlid is not null and c1.urlid != 0 group by urlid"""
    params = (query['LastName'], query['FirstName'], burst[0], burst[1])
    return db.select(sql, params, conn)

def _store_run(conn, conf, name, out_dir):
    description = '\n'.join(["%s : %s" % (key, value) for key, value in conf.iteritems()])
    params = (name, description)
    return cluster_helper.store_run_and_get_run_id(name, description, out_dir, conn=conn)

def main(args):

    progname = args[0]
    parser = OptionParser()
    parser.add_option("-d", "--dry-run", dest="dry_run", action="store_true", default=False)
    parser.add_option("-f", "--query_id_from", dest="query_id_from", action="store", type="int", default=3)
    parser.add_option("-t", "--query_id_to", dest="query_id_to", action="store", type="int", default=122)
    parser.add_option("-q", "--query_result_set", dest="query_result_set", action="store", type="choice", choices=["all","socialmedia","web"], default="all")
    options, args = parser.parse_args()

    if len(args) != 1:
        print >> sys.stderr, __doc__ % progname
        return 2

    out_dir = args[0].rstrip('/')
    out_dir_ls = idiom.ls_dir_or_exit(out_dir)
    if out_dir_ls:
        print >> sys.stderr, "Error: output directory %s not empty" % (out_dir,)
        return 2


    conf = {
            'burst_threshold' : 10,
            'stddevs_above_mean' : 2.0}
    if options.query_result_set == "all":
        conf["platforms"] = set(["hyves", "facebook", "linkedin", "twitter", "myspace", "google", "yahoo", "bing"])
    elif options.query_result_set == "socialmedia":
        conf["platforms"] = set(["hyves", "facebook", "linkedin", "twitter", "myspace"])
    elif options.query_result_set == "web":
        conf["platforms"] = set(["google", "yahoo", "bing"])

    conn = db.connect()

    # store the run in the database, even if it wouldn't find any clusters.
    if not options.dry_run:
        run_id = _store_run(conn, conf, "cluster_docs_clicked_in_same_burst", out_dir)

    queries = get_queries.get_queries(options.query_id_from, options.query_id_to)

    for query in queries:
        print >> sys.stderr, "Doing query: %s %s" % (query['FirstName'], query['LastName'])
        query_dir = util.get_dirname_from_query(query)
        search_volume = get_search_volume(conn, query)

        bursts = get_bursts(conf, search_volume)
        print >> sys.stderr, "bursts: %s" % bursts

        sim_matrix_sparse = set([])
                # (doc_id_x, doc_id_y) with doc_id_x < doc_id_y
                # sim[(x, y)] = # x, y have been clicked in the same burst.

        for burst in bursts:
            docs = [doc for doc in docs_clicked_last(query, burst, conn) if conf['platforms'] & set(doc['types'].split(':'))]
            print >> sys.stderr, "burst: (%s, %s)" % burst
            print >> sys.stderr, "docs clicked last: %s" % (docs,)

            # by just using the unique urlids we are discarding their counts,
            # but we could later easily add a minimum number of times of document
            # has to be 'last' clicked in the burst in order to be included in
            # the cluster.
            cluster = sorted([doc['urlid'] for doc in docs])
            print >> sys.stderr, "cluster = %s" % cluster
            for i in range(0, len(cluster)):
                for j in range(i + 1, len(cluster)):
                    sim_matrix_sparse.add((cluster[i], cluster[j]))

        labels, sim_matrix_full = cluster_helper.expand_sim_set(sim_matrix_sparse)

        print >> sys.stderr, "sim matrix sparse: %s" % sim_matrix_sparse
        print >> sys.stderr, "sim matrix full: %s" % sim_matrix_full

        # clustering with minimal similarity threshold of 1, so basically it
        # will amount to a full merge of all connected components in my binary
        # similarity matrix here.
        if sim_matrix_full:
            clustering, clustering_with_labels = \
                    hac.cluster_single_link_maximum(sim_matrix_full, labels, 1)

            print >> sys.stderr, "clustering with labels: %s" % (clustering_with_labels,)

            # at this point, store the clustering in the database. Note two things:
            # 1: we store only clusterings for a few docs in the db. Therefore,
            # the write_run.py script outputs singleton clusters for all docs
            # it can't find but that are 'in_weps_corpus'.
            # 2: we may have clustered documents that are not 'in_weps_corpus',
            # so we have no ground truth for these documents, e.g. because the
            # name was not found in some documents. Storing these clusterings
            # nonetheless would mean the write run script has to filter them
            # out, which it does. However, it also means that algorithms that
            # work from this partial clustering will have to filter them out!
            # So, e.g. cluster_with_text_from_partial.py will have to filter them out!

            if not options.dry_run:
                store_run.store_clustering_in_db(clustering_with_labels, run_id, query['idx'])
                cluster_helper.write_clustering_to_disk(clustering_with_labels, out_dir, query_dir)

            # now also store the partial clustering somewhere on the hard disk, for use with the repeated_hac.py program.

if __name__ == "__main__":
    sys.exit(main(sys.argv))
