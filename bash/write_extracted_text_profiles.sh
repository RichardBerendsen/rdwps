#!/bin/bash

# Assumes the script resides alongside the other preprocessing scripts.
# First argument a weps crawls directory.
# Second argument an empty results directory.
# 

print_usage() {
    cat 1>&2 <<EOF
Usage: 
$1 social_features_dir empty_dir
EOF
}

if [ "$#" -ne "2" ] || [ ! -d "$1" ] || [ ! -d "$2" ] || [ "$(ls -A "$2")" ]
then
    print_usage $0
    exit 2
fi

social_features_dir=$(echo "$1" | sed 's+/*$++')
result_dir=$(echo "$2" | sed 's+/*$++')
script_dir="$(dirname "$0")"

for query_dir in $(ls $social_features_dir)
do
    for doc_dir in $(ls $social_features_dir/$query_dir)
    do
        if [ -e $social_features_dir/$query_dir/$doc_dir/features ]
        then
            if [ ! -e $result_dir/$query_dir ]
            then
                mkdir $result_dir/$query_dir
            fi
            python $script_dir/../py/extract_text_from_extracted_features.py < $social_features_dir/$query_dir/$doc_dir/features > $result_dir/$query_dir/$doc_dir
            if [ ! -s $result_dir/$query_dir/$doc_dir ] # NOT (file exists AND has size greater than zero)
            then
                rm $result_dir/$query_dir/$doc_dir
            fi
        fi
    done
done

