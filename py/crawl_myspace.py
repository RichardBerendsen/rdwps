#!/usr/bin/python

from BeautifulSoup import BeautifulSoup
import urllib # for quote function
import urllib2 # for urlopen
import urlparse # to split the url, and urlencode only path part.

import get_queries
import sys
import time
#import oauth2 as oauth
#import simplejson as json
from pprint import pprint
from util import normalize
from get_queries import get_queries
from store_urls import store_urls
import random
import time

def _query_url(firstname, lastname):
	# This URL was created from the URL that wieowie.nl offers
	# when you click on 'more MySpace results'
	firstname = firstname.replace('"', '').strip()
	lastname = lastname.replace('"', '').strip()
	words = firstname.split() + lastname.split()
	query_string = '+'.join(words)
	url = "http://www.myspace.com/search/people?" \
			+ "q=" + query_string \
			+ "&loc=Netherlands"
	return url

def _get_page(url):
   return urllib2.urlopen(url).read()

# keep search results in case we would want to get the snippets later.  to
# retrieve these, we would only need the query_id and the rank of each search
# result.
def _store_myspace_search_result_html(page, query_id):
    try:
        f = open('../../exp/myspace_search_results_per_query/%s.html' % (query_id,), 'w')
    except IOError, e:
        sys.stderr.write('Error: Exception caught \
while storing myspace search results html page\n')
        sys.stderr.write(str(e) + '\n')
        sys.exit(2)
    else:
        f.write(page)
        f.close()

def _parse_search_results(page):
    res = []
    soup = BeautifulSoup(page)
    #f = open("page", "w")
    #f.write(soup.prettify())
    #f.close()

    # The url is in the href of an <a> element matching those criteria.
    # Strangely it is the last part of the href right after two consecutive stars.

    #ts = soup.findAll(lambda t: t.name=="a" and t.parent.name=="li" and t.parent.name=="ul" and t["class"]=="fn url beacon" and t.has_key("href"))
    ts = soup.findAll(lambda t: t.name=="a" and t.has_key("class") and t.has_key("href"))
    ts = [t for t in ts if t["class"] == "fn url beacon" ]
    return [ "www.myspace.com" + t['href'] for t in ts ]

def _fetch_urls(firstname, lastname, query_id):
    query_url = _query_url(firstname, lastname)
    print query_url
    page = _get_page(query_url)
    _store_myspace_search_result_html(page, query_id)
    urls = _parse_search_results(page)
    return urls

def _main():
    print "Starting myspace crawling..."
    print "############################"
    print "############################"
    queries = get_queries()
    for query in queries:
        firstname = query['FirstName']
        lastname = query['LastName']
        urls = _fetch_urls(firstname, lastname, query['idx'])
        n_urls=[normalize(u) for u in urls]

        store_urls(firstname, lastname, "myspace", urls[:50], n_urls[:50])
        time.sleep(117 + random.randrange(1,6,1))


if __name__ == "__main__":
    _main()
