"""
Run a HAC experiment

Usage: %s sim_matrix_dir out_dir

where out_dir is an empty directory where you would like to store the clusterings.
"""

import sys
import os
import serializer
import hac
import util
import db
import idiom
import cluster_helper
import store_run
import store_run_weps
from optparse import OptionParser

def get_similarity_threshold(conf, query):
    if query['linkedin_count'] > conf['min_linkedin_count']:
        return conf['similarity_threshold_high']
    return conf['similarity_threshold']

def cluster_with_text(conf, matrices_dir, query_dir, query):
    sim_matrix, labels = serializer.deserialize_similarity_matrix(\
            "%s/%s/similarity_matrix" % (matrices_dir, query_dir))
    return hac.cluster(sim_matrix, labels, conf['linkage_func'], \
            get_similarity_threshold(conf, query))

def store_run_and_get_run_id(conn, options, conf, matrices_dir, out_dir, which_weps):
    matrices_conf = idiom.read_file_or_empty_string("%s/run.conf" % matrices_dir)
    run_conf = """Text based clustering from similarity matrices.

    Settings for obtaining the matrices:
    %s

    Settings for the text based clustering:
    %s
    """ % (matrices_conf, '\n'.join(["%s : %s" % (key, value) for key, value in conf.iteritems()]))
    print >> sys.stderr, "Run description:\n%s" % (run_conf,)
    if not options.dry_run:
        return cluster_helper.store_run_and_get_run_id("cluster_with_text_weps_%s" % which_weps, run_conf, out_dir, which_weps, conn)
    else:
        return 1


def main(args):

    progname = args[0]
    parser = OptionParser(usage = __doc__ % progname)
    parser.add_option("-d", "--dry-run", dest="dry_run", action="store_true", default=False,
            help = "do not store stuff in the db or on disk")
    parser.add_option("-f", "--query_id_from", dest="query_id_from", action="store", type="int", default=1)
    parser.add_option("-t", "--query_id_to", dest="query_id_to", action="store", type="int", default=100000)
    parser.add_option("-w", "--which-weps", dest="which_weps", action="store", type="int", default="0", help="""
    if this is a weps run, specify which one. If this is not a weps run, don't specify, or set it to 0. 
    1: weps-1-test
    2: weps-2
    3: weps-1-train
    """)
    parser.add_option("-s", "--similarity-threshold", dest="similarity_threshold", action="store", type="float", default=0.1)
    parser.add_option("-q", "--similarity-threshold-high", dest="similarity_threshold_high", action="store", type="float", default=0.36)
    parser.add_option("-l", "--min-linkedin-count", dest="min_linkedin_count", action="store", type="int", default=500)
    parser.add_option("-m", "--linkage-method", dest="linkage_method", action="store", type="string", default="single_link")
    options, args = parser.parse_args()

    if len(args) != 2:
        print >> sys.stderr, parser.print_help()
        return 2

    matrices_dir = args[0].rstrip('/')
    out_dir = args[1].rstrip('/')
    db_storer = store_run
    if options.which_weps:
        db_storer = store_run_weps

    linkage_method_to_linkage_func = {
            'single_link' : hac.cluster_sim_single_link_max
            }

    conf = {
            'linkage_method' : options.linkage_method,
            'linkage_func' : linkage_method_to_linkage_func[options.linkage_method],
            'similarity_threshold': options.similarity_threshold,
            'similarity_threshold_high': options.similarity_threshold_high,
            'min_linkedin_count': options.min_linkedin_count
            }


    # we look in the matrices dir. For each query there, 
    # we check that the query is in the database, this is necessary to write
    # the results of this experiment to the database

    query_dirs_matrices = idiom.ls_dir_or_exit(matrices_dir)
    query_dirs_matrices.discard('run.conf')
    out_dir_ls = idiom.ls_dir_or_exit(out_dir)
    if out_dir_ls:
        print >> sys.stderr, "Error: out_dir: %s is not empty" % out_dir
        return 2

    conn = db.connect()

    run_id = store_run_and_get_run_id(conn, options, conf, matrices_dir, out_dir, options.which_weps)

    for query_dir in query_dirs_matrices:
        # check if it is in the db.
        if options.which_weps:
            query = util.get_query_from_dirname_weps(conn, query_dir)
        else:
            query = util.get_query_from_dirname(conn, query_dir)
        if not query:
            print >> sys.stderr, "Warning: Could not find query %s in the database" % query_dir
            continue
        elif query['idx'] < options.query_id_from or query['idx'] > options.query_id_to:
            continue
        clustering, labelled_clustering = cluster_with_text(conf, \
                matrices_dir, query_dir, query)
        print >> sys.stderr, "Writing clustering to disk, creating dir %s/%s" % (out_dir, query_dir)
        if not options.dry_run:
            db_storer.store_clustering_in_db(labelled_clustering, run_id, query['idx'])
            cluster_helper.write_clustering_to_disk(labelled_clustering, out_dir, query_dir)

    conn.close()

if __name__ == '__main__':
    sys.exit(main(sys.argv))
