"""
Writes gold truth

Usage:

%s dir_gold_truth

Where dir_gold_truth is the directory in which the gold truth files will be written
"""
import get_queries
import sys
import db
from optparse import OptionParser

def _query_xml_filename(firstname, lastname):
    firstname = firstname.replace('"', '').strip()
    lastname = lastname.replace('"', '').strip()
    words = firstname.split() + lastname.split()
    query_string = '_'.join(words)
    filename = query_string + ".clust.xml"
    return filename

def _get_entities(conn, query, annotator):
    cursor = conn.cursor()
    cursor.execute("select * from cluster where query_id = %s and discard = 'no' and author = %s order by id asc", (query['idx'], annotator))
    res = cursor.fetchall()
    cursor.close()
    return res

def _get_normalized_url_ids(conn, options, query_id, cluster):
    sql = {
            "SERs" : "select ids.id from (select id from search_results where query_id = %s and in_weps_corpus = 'yes' and type in ('google', 'yahoo', 'bing') group by id) as ids where ids.id in (select normalized_url_id from clustering where cluster_id = %s)",
            "Profiles" : "select ids.id from (select id from search_results where query_id = %s and in_weps_corpus = 'yes' and type in ('hyves', 'facebook', 'linkedin', 'twitter', 'myspace') group by id) as ids where ids.id in (select normalized_url_id from clustering where cluster_id =%s)",
            "both" : "select ids.id from (select id from search_results where query_id = %s and in_weps_corpus = 'yes' group by id) as ids where ids.id in (select normalized_url_id from clustering where cluster_id = %s)"}
    params = (query_id, cluster['id'])
    return db.select(sql[options.dataset], params, conn)

def _get_xml_doc(doc):
    xml = "\t\t<doc rank=\"%s\"/>\n" % doc['id']
    return xml

def _get_xml_entity(conn, options, query_id, cluster):
    docs = [_get_xml_doc(doc) for doc in _get_normalized_url_ids(conn, options, query_id, cluster)]
    xml = ""
    if docs != []:
        xml += "\t<entity id =\"%s\">\n" % cluster['id']
        xml += "".join(docs)
        xml += "\t</entity>\n"
    return xml

def _get_xml_entities(conn, options, query, annotator):
    entities = [_get_xml_entity(conn, options, query['idx'], i) for i in _get_entities(conn, query, annotator)]
    xml = "".join(entities)
    return xml

def _get_discarded(conn, options, query, annotator):
    sql = "select id from cluster where query_id = %s and discard = 'yes' and author = %s"
    params = (query['idx'], annotator)
    res = db.select(sql, params, conn)
    ids = set([])
    for cluster in res:
        for row in _get_normalized_url_ids(conn, options, query['idx'], cluster):
            ids.add(row['id'])
    return [id for id in ids]

def _get_xml_discarded(conn, options, query, annotator):
    docs = [_get_xml_doc({'id':id}) for id in _get_discarded(conn, options, query, annotator)]
    xml = ""
    if docs != []:
        xml = "\t<discarded>\n"
        xml += "".join(docs)
        xml += "\t</discarded>\n"
    return xml

def _write_gold_truth(conn, options, query, annotator, dir):
    xml_entities = _get_xml_entities(conn, options, query, annotator)
    xml_discarded = _get_xml_discarded(conn, options, query, annotator)
    if xml_entities != "" or xml_discarded != "":
        xml = "<clustering>\n" + xml_entities + xml_discarded + "</clustering>\n"

        basename = _query_xml_filename(query['FirstName'], query['LastName'])
        f = open("%s/%s" % (dir, basename), 'w')
        f.write(xml)
        f.close()

def get_annotator(conn, query):
    annotated_by = ''
    cursor = conn.cursor()
    sql = "select count(*) as n from cluster where query_id = %s and author = %s"
    richard = db.select_unique(sql, (query['idx'], 'richard'))
    if richard['n']:
        annotated_by = 'richard'
    else:
        thomas = db.select_unique(sql, (query['idx'], 'great_annotator'))
        if thomas['n']:
            annotated_by = 'great_annotator'
        else:
            bogomil = db.select_unique(sql, (query['idx'], 'bogomil'))
            if bogomil['n']:
                annotated_by = 'bogomil'
    cursor.close()
    return annotated_by


def main(argv):
    parser = OptionParser(usage=__doc__ % argv[0])
    parser.add_option("-f", "--query_id_from", dest="query_id_from", action="store", type="int", default=3)
    parser.add_option("-t", "--query_id_to", dest="query_id_to", action="store", type="int", default=45)
    parser.add_option("-s", "--dataset", dest="dataset", action="store", type="choice", choices=["SERs", "Profiles", "both"], default="both")
    options, args = parser.parse_args()
    if len(args) != 1:
        print >> sys.stderr, "Please specify a directory to write to\n"
        parser.print_help()
        return 2

    dir = args[0].rstrip('/')
    queries = get_queries.get_queries(options.query_id_from, options.query_id_to)

    conn = db.connect()
    for q in queries:
        annotator = get_annotator(conn, q)
        if annotator != "":
            _write_gold_truth(conn, options, q, annotator, dir)

    conn.close()

if __name__ == "__main__":
    sys.exit(main(sys.argv))
