# used by crawl_SEARCHENGINEX.py
# ipmort it from there
import MySQLdb

def _store_url(conn, first_name, last_name, platform, rank, url, normalized_url):
	cursor = conn.cursor()
	cursor.execute("call store_url(%s, %s, %s, %s, %s, %s)", (first_name, last_name, platform, rank, url, normalized_url))
	cursor.close()

def _store_urls(conn, first_name, last_name, platform, urls, normalized_urls):
	for rank, (url, normalized_url) in enumerate(zip(urls, normalized_urls)):
 		try:
		  _store_url(conn, first_name, last_name, platform, rank + 1, url, normalized_url)
		except Exception, e:
		  print "Exception storing url in db."
		  print e

def _connect():
	return MySQLdb.connect (host = "qassir",
						   user = "bogomil",
						   passwd = "wieowie",
						   db = "people")

def store_urls(first_name, last_name, platform, urls, normalized_urls):
    conn = _connect()
    _store_urls(conn, first_name, last_name, platform, urls, normalized_urls)
    conn.close()
