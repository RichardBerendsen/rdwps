#!/usr/bin/python
"""
Output:
username
profile_picture_url
list of frends separated by #
"""
import sys
import os
import re
from BeautifulSoup import BeautifulSoup
import markup_util

def _get_vcard(soup):
  name_div = soup.findAll(lambda x: x.name == "a" and x.parent.name == "div" and x.parent.has_key('class') and x.parent['class'] == "vcard userInfo")
  return name_div[0]

def extract_name_from_mini_prof(mini_prof):
  mini_prof = mini_prof.replace("&quot;", "'")
  #print mini_prof
  name = re.search("['\"]title['\"]:['\"]([^'\"]+)['\"]", mini_prof).group(1)
  return name

def get_profilename(soup):
  vcard = _get_vcard(soup)
  #mini_prof = vcard['data-miniprofile']
  mini_prof = soup.findAll(lambda x: x.name == "a" and x.has_key('data-miniprofile'))[0]['data-miniprofile']
  return extract_name_from_mini_prof(mini_prof)

def get_picture(soup):
  vcard = _get_vcard(soup)
  img = vcard.findAll(lambda x: x.name == "img")
  return img[0]['src']

def get_friends(soup):
  elems = soup.findAll(lambda x: x.name == "div" and x.has_key('class') and x['class'] == "social")
  #print len(elems)
  friends = []
  for e in elems:
    mini_prof = e.findAll(lambda x: x.name == "a" and x.has_key('data-miniprofile'))[0]['data-miniprofile']
    name = extract_name_from_mini_prof(mini_prof)
    if not name == "Tom Anderson": # founder of myspace - everyone's friend
      friends.append(name)
  return friends, ["" for f in friends]

def get_text(soup):
  return ""
