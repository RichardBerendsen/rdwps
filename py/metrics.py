
"""
  File containing different metrics.
"""

"""
Assumes equal length vectors.
"""

import math

def dot_product(x,y):
  return sum([x[i]*y[i]*1.0 for i in range(0, len(x))])

def dot_product_hashes(x,y):
  return sum([x[k]*y[k]*1.0 for k in x.keys()])

def norm(x):
  return math.sqrt(sum([t*t*1.0 for t in x]))

def norm_hash(x):
  return math.sqrt(sum([x[k]*x[k]*1.0 for k in x.keys()]))

def cosine(x, y):
  return dot_product(x,y)*1.0/(norm(x)*norm(y))

# x and y are hashes of the form {term:weight}
def cosine_hashes(x, y):
  normx=norm_hash(x)
  normy=norm_hash(y)
  if normx==0 or normy==0:
    return 0
  return dot_product_hashes(x,y)*1.0/(norm_hash(x)*norm_hash(y))

#print dot_product([1,2],[2,1])
#print norm([1,2])
#print cosine([1,0],[0.75,0.43301])
