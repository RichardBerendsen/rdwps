#!/usr/local/bin/bash

# read URLs from stdin

while read line;
do
    query_id=$(echo $line | cut -f 1 -d ';')
    query_url=$(echo $line | cut -f 2 -d ';')
    w3m -dump_source -no-cookie $query_url > "../../exp/google_search_results_per_query/$query_id.html"
    sleep 180;
done
