#!/usr/bin/python

# the differences with get_urls:
# - this one retrieves only urls in which the name occurs (using search_results.name_found_in_doc_2)
# - this one retrieves only urls of mimetype starting with 'text/html'
# - selects only queries that have been crawled before, obtaining above statistics (query_id <= 45)
# - select only queries of social media profiles.
import MySQLdb
import MySQLdb.cursors
import urllib
from pprint import pprint
from urlparse import urlparse
import re
import random
import sys

def _connect():
	return MySQLdb.connect (host = "qassir",
						   user = "bogomil",
						   passwd = "wieowie",
						   db = "people",
                           cursorclass = MySQLdb.cursors.DictCursor)
def _get_queries(conn, query_id_from, query_id_to):
    cursor = conn.cursor()
    cursor.execute("select * from selected_queries where idx >= %s and idx <= %s", (query_id_from, query_id_to))
    queries = cursor.fetchall()
    cursor.close()
    return queries

def _get_search_results(query, conn):
    cursor = conn.cursor()
    cursor.execute("select s.* from search_results as s, normalized_url as n \
where s.query_id = %s and s.id = n.id and n.mimetype like \
'text/html%%' and s.name_found_in_doc_2 = 'yes' group by s.id \
having \
sum(if(s.type in ('hyves', 'facebook', 'linkedin', 'twitter', 'myspace') or \
s.url like '%%hyves.nl%%' or \
s.url like '%%facebook.com%%' or \
s.url like '%%linkedin.com%%' or \
s.url like '%%twitter.com%%' or \
s.url like '%%myspace.com%%', 1, 0)) > 0 order by s.query_id, s.id",
(query['idx'],))
    srch_results = cursor.fetchall()
    cursor.close()
    return srch_results

def _get_top_level_domain(url):
    parsed = urlparse(url)
    domain = ""
    if (url[0:4] == "http"):
      domain = parsed[1]
    elif (url[0:4] == "www."):
      domain = parsed[2]
    else:
      raise Exception("unexpected url format")
    levels = re.split("\.", domain)
    return ".".join(levels[-2:])

def _get_all_bins(search_results, queries):
   all_bins = {}
   for q in queries:
     key = q['FirstName'] + "_" + q['LastName']
     all_bins[key] = []

   for s in search_results:
     key = s['firstname'] + "_" + s['lastname']
     top_domain = _get_top_level_domain(s['url'])
     if top_domain not in all_bins[key]:
       all_bins[key].append(top_domain)
   return all_bins

def _get_bins(search_results):
   bins = {}
   for s in search_results:
      key = _get_top_level_domain(s['url'])
      if key in bins.keys():
        bins[key].append(s['auto_incr_id'])
      else:
        bins[key] = [s['auto_incr_id']]
   return bins

def _get_next_nonempty_bin(bins, last_bin):
    keys = sorted(bins.keys())
    for i in range(last_bin+1, len(keys)):
      if len(bins[keys[i]]) != 0:
        return i

    for i in range(0, last_bin+1):
      if len(bins[keys[i]]) != 0:
        return i
    return -1


def _get_next(bins, last_key):
    keys = sorted(bins.keys())
    next_nonempty = _get_next_nonempty_bin(bins, last_key)
    if next_nonempty == -1:
      return None, -1
    res = bins[keys[next_nonempty]][0]
    bins[keys[next_nonempty]] = bins[keys[next_nonempty]][1:]
    return res, next_nonempty

def _get_all(bins):
    waits = {}
    srt = sorted(bins.keys())
    for k in sorted(bins.keys()):
      waits[srt.index(k)] = -1
    res = []
    keys = sorted(bins.keys())
    last_key = -1
    while(True):
      next_res, key_used = _get_next(bins, last_key)
      if (next_res == None):
        return res
      else:
        if waits[key_used] == -1:
            waittime = 0
        elif waits[key_used] < 60:
            waittime = 50 - waits[key_used] + random.randrange(0,20)
            waittime = 0 if waittime < 0 else waittime
        else:
            waittime = 0
        for k in waits.keys():
            waits[k] += waittime
        waits[key_used] = 0
        res.append((waittime, next_res))

      last_key = key_used


if __name__ == "__main__":

    query_id_from = 3
    query_id_to = 45
    if len(sys.argv) != 3 and len(sys.argv) != 1:
        print >> sys.stderr, "Usage: %s [from to]" % sys.argv[0]
        print >> sys.stderr, "where from and to with from <= to is a query id range"
        sys.exit(2)
    if len(sys.argv) == 3:
        query_id_from = sys.argv[1]
        query_id_to = sys.argv[2]


    conn = _connect()
    queries = _get_queries(conn, query_id_from, query_id_to)

    for q in queries:
      fname = q['FirstName']
      lname = q['LastName']
      chopped_lname = lname.replace("\"", "")
      chopped_lname = chopped_lname.replace(" ", "_")
      search_results = _get_search_results(q, conn)
      bins = _get_bins(search_results)
      all_urls = _get_all(bins)

      for u in all_urls:
        waittme, url = u
        row = [s for s in search_results if s['auto_incr_id'] == url][0]
        encoded_url = urllib.quote(row['url'], '')
        print str(q['idx']) + "_" + fname + "_" + chopped_lname + ";" + str(row['id']) + ";" + str(waittme) + ";" + encoded_url




    #all_bins = _get_all_bins(search_results, queries)



    #urls = [r['url'] for r in search_results if r['url'][0:4] != "http"]

    #pprint(bins.keys()[0])
    #pprint(sorted(bins[bins.keys()[0]]))
    conn.close()

