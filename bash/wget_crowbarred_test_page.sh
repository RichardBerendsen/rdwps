#!/usr/local/bin/bash

# the idea of this script is that wget will download all prerequisites of a file that was stored offline.

#online_url="http://www.facebook.com/tha.kimkong"
#offline_url="http://zookst1.science.uva.nl:8089/static/kimkong.html"

#online_url="http://kimmetjeberg.hyves.nl/"
#offline_url="http://zookst1.science.uva.nl:8089/static/kimmetjeberg_hyves.html"

online_url="http://nl.linkedin.com/pub/ebel-nikolaas-kremer/17/302/a9b"
offline_url="http://zookst1.science.uva.nl:8089/static/ebel-nikolaas-kremer_linkedin.html"

# But to not raise suspicion of imposting the page, we will first wget the online version of the page without anything else.
wget \
--append-output "wget_log_online_pages" \
--user-agent="Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.2.14) Gecko/20110218 Firefox/3.6.14" \
--delete-after \
$online_url

# now let's let wget rock and roll the page requisites, converting the links.
wget \
--append-output "wget_log_offline_pages" \
--user-agent="Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.2.14) Gecko/20110218 Firefox/3.6.14" \
--convert-links \
--page-requisites \
--backup-converted \
--span-hosts \
--html-extension \
--timestamping \
--execute robots=off \
$offline_url

# and this is the explanation of all these options
#wget \
#--append-output "wget_log" \ # error messages, status, anything.
#--user-agent="Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.2.14) Gecko/20110218 Firefox/3.6.14" \ # or risk being refused at facebook and other sites
#--convert-links \ # for local storing
#--page-requisites \ # for storing images and other 'internal links' in leaf nodes
#--backup-converted \ # if you rewrite with --html-extension, you back it up with the original extension, to let wget know when it downloaded a page already.
#--span-hosts \ # allow images, stylesheets and so on from other hosts to be retrieved.
#--html-extension \ # if page mimetype is html, then this changes .asp or .php to .html. 
#--timestamping # sets the modified date to the server modified date.
#--execute robots=off is necessary for really retrieving all images locally.
