#!/usr/bin/python
"""
Output:
username
profile_picture_url
list of frends separated by #
"""
import sys
import os
import re
from BeautifulSoup import BeautifulSoup
import markup_util

def get_picture(soup):
  img = soup.findAll(lambda x: x.name == "img" and x.has_key("id") and x['id'] == "profile_pic")
  if len(img) > 0:
    return img[0]['src']
  else:
    return ""

def get_profilename(soup):
  return ""
def get_text(soup):
  elems = soup.findAll(lambda x:\
   (x.name == "div" and x.has_key("class") and x['class'] == "mediaPortrait")\
   or\
   (x.name == "td" and x.has_key("class") and x['class'] == "data"))
  text =""
  for e in elems:
    text += " ".join(e.findAll(text=True)) + " "
  return text

def get_friends(soup):
  elems = soup.findAll(lambda x: x.name == "div" and x.has_key("class") and x['class'] == "profileFriendsText")
  names = []
  types = []
  for e in elems:
    name = " ".join(e.findAll(text=True))
    names.append(name)
    types.append("")
  return names, types

