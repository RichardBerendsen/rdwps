#!/usr/bin/python

import os
import re
import subprocess

specials = {}
specials[" "] = "%20"
specials["!"] = "%21"
specials["\""] = "%22"
specials["#"] = "%23"
specials["$"] = "%24"
specials["%"] = "%25"
specials["&"] = "%26"
specials["'"] = "%27"
specials["("] = "%28"
specials[")"] = "%29"
specials["*"] = "%2A"
specials["+"] = "%2B"
specials[","] = "%2C"
specials["-"] = "%2D"
specials["."] = "%2E"
specials["/"] = "%2F"
specials[":"] = "%3A"
specials[";"] = "%3B"
specials["<"] = "%3C"
specials["="] = "%3D"
specials[">"] = "%3E"
specials["?"] = "%3F"
specials["@"] = "%40"
specials["["] = "%5B"
specials["\\"] = "%5C"
specials["]"] = "%5D"
specials["^"] = "%5E"
specials["_"] = "%5F"
specials["`"] = "%60"
specials["{"] = "%7B"
specials["|"] = "%7C"
specials["}"] = "%7D"
specials["~"] = "%7E"
for i in range(0, 10):
  exec("specials[\"\\x0" + str(i) +"\"] = \"%0" + str(i) + "\"")
  exec("specials[\"\\x1" + str(i) +"\"] = \"%1" + str(i) + "\"")
#specials["\x08"] = "%08"  # backspace
#specials["\x09"] = "%09"  # tab
specials["\x0A"] = "%0A"  # linefeed
specials["\x0B"] = "%0B"
specials["\x0C"] = "%0C"
specials["\x0D"] = "%0D"  # carriage return
specials["\x0E"] = "%0E"
specials["\x0F"] = "%0F"
specials["\x1A"] = "%1A"  # linefeed
specials["\x1B"] = "%1B"
specials["\x1C"] = "%1C"
specials["\x1D"] = "%1D"  # carriage return
specials["\x1E"] = "%1E"
specials["\x1F"] = "%1F"

hexdigits=["0","1","2","3","4","5","6","7","8","9",'A', 'a','B', 'b','C', 'c','D', 'd','E', 'e','F', 'f']

def _iter(s, n):
  res = s
  for i in range(0 , n):
     res = normalize(res)
  return res

def _normalize_syntactic(s):
  special_chars = specials.keys()
  l = len(s)
  res = ""
  i = 0
  while i < l :
    # leave escaped characters but uppercase the digits a-f
    if s[i] == "%" and i+2 < l and s[i+1] in hexdigits and s[i+2] in hexdigits:
      res += s[i] + s[i+1].upper() + s[i+2].upper()
      i = i + 3
      continue
    elif s[i] in special_chars:
      res += specials[s[i]]
      i += 1
      continue
    else:
      res += s[i]
      i += 1
  return res

def _normalize_semantic(s):
    # glue together several consecutive /'s
    s = re.sub("(%2F)+", "%2F", s)
    # remove trailing / or #
    if (s[-3:] == "%2F" or s[-3:] == "%23"):
      s = s[:-3]
    return s

def normalize(s):
    s = _normalize_syntactic(s)
    s = _normalize_semantic(s)
    return s

def _string_array_to_array(s):
    exec("x="+s)
    return x

# returns a flattened version of the list l
def linearize(l):
  x = str(l)
  x = x.replace('[','')
  x = x.replace(']','')
  x = x.replace(',','')
  toks = re.split("\s+", x)
  return [int(t) for t in toks if len(t) > 0]


# Calls the dutch_stemmer or english_stemmer
# for a single word.
# Used when we want to define term windows around names,
# because the names could be stemmed already.

def stem(stemmer, word):
  if stemmer == "":
    return word
  #process = subprocess.Popen('echo "' + word + '" | ../bash/' + lang + '_stemmer', shell=True,stdout=subprocess.PIPE)
  process = subprocess.Popen('echo "' + word + '" | ' + stemmer , shell=True,stdout=subprocess.PIPE)
  return process.communicate()[0][:-1]

def linearize_list_with_ints_as_strings(l):
  x = str(l)
  x = x.replace('[','')
  x = x.replace(']','')
  x = x.replace('\'','')
  x = x.replace('\"','')
  x = x.replace(',','')
  toks = re.split("\s+", x)
  return [int(t) for t in toks if len(t) > 0]

# doesn't work
def ___insert(arr, value, insert_pointer):
   print arr
   expr="arr"
   for i in range(0, len(insert_pointer)):
     expr += "[" + str(insert_pointer[i]) + "]"
   par = expr[:expr.rfind("[")]
   to_exec = "if len(" + par + ") > 0:\n  " + expr + ".append(" + value + ")" + "\nelse:\n  " + par + "=[" + value + "]"
   print to_exec
   exec(to_exec)

def string_array_extract_first_level(s):
  res = []
  content = ""
  level = 0
  for i in range(0, len(s)):
    if s[i] == "[":
      if level > 0:
        pass
        #content += "["
      level += 1
    elif s[i] == "]":
      level -= 1
      if level == 1:
        if content[0] == ",":
          content = content[1:]
        #print content
        exec("temp=["+content+"]")
        res.append(temp)
        #res.append(content)
        content = ""
      else:
        pass
        #content += "]"
    elif s[i] == ",":
      content += ", "
    else:
      content += s[i]
  res_str = str(res)
  #print res
  #print res_str
  exec("result="+res_str)
  return result

# doesn't work
def ___string_array_to_array(s):
    # first make it an array of numbers
    s.replace('\'','')
    s.replace('\"','')
    s.replace(' ','')
    insert_pointer = []
    result=[[]]
    value=""
    for i in range(0, len(s)):
      if s[i] == "[":
        insert_pointer.append(0)
        insert(result, "#", insert_pointer)
      elif s[i] == "]":
        if s[i-1] not in ["]", ","]:
          insert(result, value, insert_pointer)
        insert_pointer=insert_pointer[:-1]
        value = ""
      elif s[i] == ",":
        if s[i-1] not in ["]", ","]:
          insert(result, value, insert_pointer)
        value = ""
      else:
        value += s[i]
    return result

def get_query_from_dirname(conn, dirname):
    """
    Example: 1_George_W_Bush -> George "W Bush"

    works only for our wieowie queries, since the weps queries have no leading number.
    """
    toks = re.split("_", dirname)
    firstname = toks[1]
    if len(toks) > 3:
        lastname = "\""+" ".join(toks[2:])+"\""
    elif len(toks) == 3:
        lastname = toks[2]
    else:
        lastname = ""
    cursor = conn.cursor()
    cursor.execute("select * from selected_queries where FirstName=%s and LastName=%s", (firstname, lastname))
    res = cursor.fetchone()
    cursor.close()
    return res

def get_query_from_dirname_weps(conn, dirname):
    """
    Example: George_Bush -> George Bush

    Works only for weps queries, since our queries have a leading idx.
    """
    toks = re.split("_", dirname)
    firstname=toks[0]
    lastname=toks[1]
    cursor = conn.cursor()
    cursor.execute("select * from selected_queries_weps where FirstName=%s and LastName=%s", (firstname, lastname))
    res = cursor.fetchone()
    cursor.close()
    return res

# query is a db record (select * from selected_queries)
def get_dirname_from_query(query):
    firstname = query['FirstName'].replace('"', '').strip()
    lastname = query['LastName'].replace('"', '').strip()
    words = [str(query['idx'])] + firstname.split() + lastname.split()
    return '_'.join(words)

# query is a db record (select * from selected_queries)
def get_dirname_from_query_weps(query):
    firstname = query['FirstName'].replace('"', '').strip()
    lastname = query['LastName'].replace('"', '').strip()
    words = firstname.split() + lastname.split()
    return '_'.join(words)

def apply_rel_path(base_path, rel_path):
  #print base_path
  #print rel_path
  rel_path_dirname = os.path.dirname(rel_path)
  rel_path_basename = os.path.basename(rel_path)
  cur_dir = os.getcwd()
  os.chdir(base_path)
  os.chdir(rel_path_dirname)
  res = os.getcwd() + "/" + rel_path_basename
  os.chdir(cur_dir)
  return res

if __name__=="__main__":
   #print string_array_to_array("[[[1]],2,3]")
   #print string_array_extract_first_level("[[[231,[[[2432]]]],[4232]],[22],[23]]")[0][1]
   print stem("english", "going")
