#!/usr/bin/python

# First argument file with the matrix.
# Second argument directory where the cluster should appear.

import db
import MySQLdb
import MySQLdb.cursors
import re
import hac
import create_matrices
from datetime import datetime
import util
import serializer


def store_run_in_db(base_dir, feature_vector_method, occurrence_limit, metric, stopword_file, similarity_threshold, term_window, logged_tfs, experiment_name, which_weps=""):
  """
  Stores a new run with the specified parameters. Returns the id of the inserted run.
  """
  stopwords = serializer.get_stopwords(stopword_file)
  description="";
  now = datetime.now()
  description += "\nTime: " + now.__str__() + "\n"
  description += "Base dir is: " + base_dir + "\n"
  description += "Feature vector method: " + feature_vector_method + "\n"
  description += "Metric: " + metric + "\n"
  description += "Logged tfs: " + str(logged_tfs) + "\n"
  description += "Similarity threshold: " + str(similarity_threshold) + "\n"
  description += "Occurrence limit: " + str(occurrence_limit) + "\n"
  description += "Term window: " + str(term_window) + "\n"
  description += "Stopwords: "
  for s in stopwords:
    description +=  "  " + s
  conn=db.connect()
  cursor = conn.cursor()
  cursor.execute("insert into run (name, description) values (%s, %s)", (experiment_name, description))
  cursor.execute("select last_insert_id() as id from run")
  res = cursor.fetchone()
  id = res['id']
  return id

def get_query_id(dirname):
    conn = db.connect()
    query = util.get_query_from_dirname(conn, dirname)
    conn.close()
    return query['idx']


def store_clustering_in_db(labelled_clustering, run_id, query_id):
  first_level_clusters = util.string_array_extract_first_level(labelled_clustering)
  conn = db.connect()
  cursor = conn.cursor()
  cluster_count=0
  for c in first_level_clusters:
    #print c
    cluster_count += 1
    cluster_label = "run_"+str(run_id) + " qry_"+str(query_id)+" cluster_" + str(cluster_count)
    cursor.execute("insert into run_cluster (label, run_id, query_id) values (%s, %s, %s)", (cluster_label, str(run_id), str(query_id)))
    cursor.execute("select id from run_cluster where run_id=%s and query_id=%s and label=%s", (str(run_id), str(query_id), cluster_label))
    cluster_id = cursor.fetchone()['id']
    #print "TTTTTT"
    #print c
    #print "QQQQQQ"
    #cluster = hac.linearize_list_with_ints_as_strings(c)
    #print "AAAAA"
    #print cluster
    #print "BBBBB"
    for urlid in c:
      cursor.execute("insert into run_clustering (normalized_url_id, cluster_id) values (%s, %s)", (str(urlid),str(cluster_id) ))
