#!/usr/bin/python

from BeautifulSoup import BeautifulSoup
import get_queries
import MySQLdb
import MySQLdb.cursors
import util
import re
import connect


def _get_clickouts(conn):
    cursor = conn.cursor()
    cursor.execute("select ClickID as ClickID, c.type as type, c.URL as URL from selected_queries_clicks as c;")
    clickouts = cursor.fetchall()
    cursor.close()
    return clickouts

def _google_de_cache(conn, url):
    if re.search(r'search\?q=cache', url) is None:
        return url
    else:
        cursor = conn.cursor()
        cursor.execute("select translated_URL from google_cached_urls where URL = %s and translated_URL is not null", (url,))
        res = cursor.fetchone() # translated_url may be an empty string.
        cursor.close()
        if res is None: # empty result set
            translated_url = ''
        else:
            translated_url = res['translated_URL']
        return translated_url

def _update_click_with_urlid(conn, click_id, urlid):
    cursor = conn.cursor()
    cursor.execute("update selected_queries_clicks set urlid = %s where ClickID = %s", (urlid, click_id))
    cursor.close()

def _match_clickout(conn, clickout, normalized_url):
    # check if normalized url has been stored. If not, urlid will remain 0.
    urlid = 0
    cursor = conn.cursor()
    cursor.execute("select * from normalized_url where normalized_url = %s", (normalized_url,))
    res = cursor.fetchone()
    cursor.close()
    if res is not None:
        # the url was matched. 
        urlid = res['id']
    # now update click
    _update_click_with_urlid(conn, clickout['ClickID'], urlid)

def _match_clickout_urls(conn):
    # get clickouts
    clickouts = _get_clickouts(conn)
    for clickout in clickouts:
        if clickout['type'] == 'google':
            clickout['URL'] = _google_de_cache(conn, clickout['URL'])
        if clickout['URL'] == '':
            continue
        normalized_url = util.normalize(clickout['URL'].strip())
        _match_clickout(conn, clickout, normalized_url)

def _main():
    conn = connect.connect()
    _match_clickout_urls(conn)
    conn.close()


if __name__ == "__main__":
    _main()
