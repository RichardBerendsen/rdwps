#!/usr/bin/python
"""
Extracts and tokenizes links from an html page.
Outputs a list of tokens.
The tokens include the top level domain and the elements of the path. If the last element of the path ends in .ext the .ext is ignored.
The query is ignored.
Usage:
  python program input_file
"""

import sys
import os
import re
from BeautifulSoup import BeautifulSoup
from util import normalize

infile = sys.argv[1]
#sys.stderr.write("Now processing: " + infile + "\n")
try:
  f = open(infile, "r")
except Exception, e:
  sys.stderr.write("Problem opening " + infile + "\n")
text = f.read()
#sys.stderr.write("Starting: " + infile + "\n")
try:
  soup = BeautifulSoup(text)
except Exception, e:
  try:
    text = unicode(text, errors='ignore')
    soup = BeautifulSoup(text)
  except Exception, e:
    sys.stderr.write("Problem in " + infile + "\n")
    sys.stderr.write(e.message)
  pass

anchors = soup.findAll(lambda t:t.has_key("href"))

print len(anchors)
for a in anchors:
  try:
    print normalize(a["href"])
  except Exception, e:
    pass
f.close()
