import connect


def _get_search_results_starting_with_www(conn):
    cursor = conn.cursor()
    cursor.execute("select * from search_results where url like 'www%'")
    res = cursor.fetchall()
    cursor.close()
    return res

def _update_url(conn, search_result):
    cursor = conn.cursor()
    cursor.execute("update search_results set url = %s where auto_incr_id = %s", ('http://' + search_result['url'], search_result['auto_incr_id']))
    cursor.close()

conn = connect.connect()
for search_result in _get_search_results_starting_with_www(conn):
    _update_url(conn, search_result)
conn.close()

