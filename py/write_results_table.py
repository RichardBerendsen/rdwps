"""
create table for results output by wepsEvaluation

Usage: %s results_dir which_evaluator

where which_evaluator is either
 1 : the weps1 evaluator (wepsEvaluation.jar)
 2 : the weps2 evaluator (wepsScorer-weps2.jar)

They give the same results, good for us.
"""

import os
import sys
import idiom

def main(args):
    if len(args) != 3:
        print >> sys.stderr, __doc__ % args[0]
        return 2

    results_dir = args[1]
    which_evaluator = int(args[2])
    if which_evaluator != 1 and which_evaluator != 2:
        print >> sys.stderr, __doc__ % args[0]
        return 2

    files = idiom.ls_dir_or_exit(results_dir)

    results_files = [file for file in files if file.endswith(".eval")]
    avg_scores = [] # list of tuples (run_name, BEP, BER, F)
    for results_file in results_files:
        f = open("%s/%s" % (results_dir, results_file))
        if which_evaluator == 1:
            avg_score = f.readlines()[-1].strip().split()[1:4]
        else:
            parts = f.readlines()[-1].strip().split()
            avg_score = parts[1:3] + [parts[5]]
        avg_scores.append(tuple([results_file] + avg_score))
        f.close()

    delim = '\t'
    print delim.join(["name", "BEP", "BER", "FMeasure_0.5_BEP-BER"])
    for avg_score in avg_scores:
        print delim.join(avg_score)

if __name__ == "__main__":
    sys.exit(main(sys.argv))
